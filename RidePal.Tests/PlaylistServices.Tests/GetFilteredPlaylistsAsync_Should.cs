﻿using MapsterMapper;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RidePal.Data.Context;
using RidePal.Data.Models;
using RidePal.Services;
using RidePal.Services.Contracts;
using RidePal.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RidePal.Tests.PlaylistServices.Tests
{
    [TestClass]
    public class GetFilteredPlaylistsAsync_Should
    {
        private static List<Playlist> playlistsList;
        private static List<Album> albums;
        private static List<Artist> artists;
        private static List<Genre> genres;
        private static List<Track> tracks;
        private static List<User> users;

        static GetFilteredPlaylistsAsync_Should()
        {
            albums = TestSetup.GetAlbums().ToList();
            artists = TestSetup.GetArtists().ToList();
            genres = TestSetup.GetGenres().ToList();
            tracks = TestSetup.GetTracks().ToList();
            users = TestSetup.GetUsers().ToList();

            var playlistTracks1 = new List<Track> { tracks[0], tracks[5], tracks[3] };
            var playlistTracks2 = new List<Track> { tracks[1], tracks[6], tracks[4] };
            var playlistTracks3 = new List<Track> { tracks[2], tracks[7], tracks[5] };

            playlistsList = new List<Playlist>
                {
                    new Playlist
                    {
                        Name = "Playlist 1",
                        UserID = 1,
                        Tracks = playlistTracks1,
                        Genres = genres.Take(2).ToList(),
                        TracksCount = playlistTracks1.Count,
                        TotalPlaytime = playlistTracks1.Sum(track => track.Duration),
                        Rank = playlistTracks1.Average(track => track.Rank)
                    },
                    new Playlist
                    {
                        Name = "Fave songs",
                        UserID = 2,
                        Tracks = playlistTracks2,
                        Genres = genres.Take(3).ToList(),
                        TracksCount = playlistTracks2.Count,
                        TotalPlaytime = playlistTracks2.Sum(track => track.Duration),
                        Rank = playlistTracks2.Average(track => track.Rank)
                    },
                    new Playlist
                    {
                        Name = "Playlist for the soul",
                        UserID = 3,
                        Tracks = playlistTracks3,
                        Genres = genres.Take(3).ToList(),
                        TracksCount = playlistTracks3.Count,
                        TotalPlaytime = playlistTracks3.Sum(track => track.Duration),
                        Rank = playlistTracks3.Average(track => track.Rank)
                    }
                };
        }

        [TestMethod]
        public async Task ReturnNull_When_NoValidParametersPassed()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnNull_When_NoValidParametersPassed));

            var bingMapsServicesMock = new Mock<IBingMapsServicesAsync>();
            var randomNumGenereatorMock = new Mock<Random>();
            var loggerMock = new Mock<ILogger<PlaylistServicesAsync>>();
            var pictureServicesMock = new Mock<IPictureServicesAsync>();

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new PlaylistServicesAsync(arrangeCtx, bingMapsServicesMock.Object, pictureServicesMock.Object, new Mapper(), randomNumGenereatorMock.Object, loggerMock.Object);

                //Act
                var result = await sut.GetFilteredPlaylistsAsync("", "", "");

                //Assert
                Assert.IsNull(result);
            }
        }

        [TestMethod]
        public async Task ReturnNull_When_GenreIsInvalid()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnNull_When_GenreIsInvalid));

            var bingMapsServicesMock = new Mock<IBingMapsServicesAsync>();
            var randomNumGenereatorMock = new Mock<Random>();
            var loggerMock = new Mock<ILogger<PlaylistServicesAsync>>();
            var pictureServicesMock = new Mock<IPictureServicesAsync>();

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new PlaylistServicesAsync(arrangeCtx, bingMapsServicesMock.Object, pictureServicesMock.Object, new Mapper(), randomNumGenereatorMock.Object, loggerMock.Object);

                //Act
                var result = await sut.GetFilteredPlaylistsAsync(null, "12", null);

                //Assert
                Assert.IsNull(result);
            }
        }

        [TestMethod]
        public async Task ReturnPlaylists_When_ValidNamePassed()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnPlaylists_When_ValidNamePassed));
            TestSetup.TestInitialize();

            using (var arrangeCtx = new RidePalContext(options))
            {
                arrangeCtx.Albums.AddRange(albums);
                arrangeCtx.Artists.AddRange(artists);
                arrangeCtx.Genres.AddRange(genres);
                arrangeCtx.Tracks.AddRange(tracks);
                arrangeCtx.Users.AddRange(users);

                arrangeCtx.Playlists.AddRange(playlistsList);

                arrangeCtx.SaveChanges();
            }

            var bingMapsServicesMock = new Mock<IBingMapsServicesAsync>();
            var randomNumGenereatorMock = new Mock<Random>();
            var loggerMock = new Mock<ILogger<PlaylistServicesAsync>>();
            var pictureServicesMock = new Mock<IPictureServicesAsync>();

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new PlaylistServicesAsync(arrangeCtx, bingMapsServicesMock.Object, pictureServicesMock.Object, new Mapper(), randomNumGenereatorMock.Object, loggerMock.Object);

                //Act
                var result = await sut.GetFilteredPlaylistsAsync("Play", null, null);
                var resultList = result.ToList();

                //Assert
                Assert.IsNotNull(result);
                Assert.IsInstanceOfType(resultList[0], typeof(PlaylistDTO));
                Assert.AreEqual(2, result.Count());
            }
        }

        [TestMethod]
        public async Task ReturnEmptyList_When_NoNameMatches()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnEmptyList_When_NoNameMatches));
            TestSetup.TestInitialize();

            using (var arrangeCtx = new RidePalContext(options))
            {
                arrangeCtx.Albums.AddRange(albums);
                arrangeCtx.Artists.AddRange(artists);
                arrangeCtx.Genres.AddRange(genres);
                arrangeCtx.Tracks.AddRange(tracks);
                arrangeCtx.Users.AddRange(users);

                arrangeCtx.Playlists.AddRange(playlistsList);

                arrangeCtx.SaveChanges();
            }

            var bingMapsServicesMock = new Mock<IBingMapsServicesAsync>();
            var randomNumGenereatorMock = new Mock<Random>();
            var loggerMock = new Mock<ILogger<PlaylistServicesAsync>>();
            var pictureServicesMock = new Mock<IPictureServicesAsync>();

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new PlaylistServicesAsync(arrangeCtx, bingMapsServicesMock.Object, pictureServicesMock.Object, new Mapper(), randomNumGenereatorMock.Object, loggerMock.Object);

                //Act
                var result = await sut.GetFilteredPlaylistsAsync("music", null, null);
                var resultList = result.ToList();

                //Assert
                Assert.IsNotNull(result);
                Assert.AreEqual(0, result.Count());
            }
        }

        [TestMethod]
        public async Task ReturnPlaylists_When_ValidName_Genre_Passed()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnPlaylists_When_ValidName_Genre_Passed));
            TestSetup.TestInitialize();

            using (var arrangeCtx = new RidePalContext(options))
            {
                arrangeCtx.Albums.AddRange(albums);
                arrangeCtx.Artists.AddRange(artists);
                arrangeCtx.Genres.AddRange(genres);
                arrangeCtx.Tracks.AddRange(tracks);
                arrangeCtx.Users.AddRange(users);

                arrangeCtx.Playlists.AddRange(playlistsList);

                arrangeCtx.SaveChanges();
            }

            var bingMapsServicesMock = new Mock<IBingMapsServicesAsync>();
            var randomNumGenereatorMock = new Mock<Random>();
            var loggerMock = new Mock<ILogger<PlaylistServicesAsync>>();
            var pictureServicesMock = new Mock<IPictureServicesAsync>();

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new PlaylistServicesAsync(arrangeCtx, bingMapsServicesMock.Object, pictureServicesMock.Object, new Mapper(), randomNumGenereatorMock.Object, loggerMock.Object);

                //Act
                var result = await sut.GetFilteredPlaylistsAsync("Play", "1", null);
                var resultList = result.ToList();

                //Assert
                Assert.IsNotNull(result);
                Assert.IsInstanceOfType(resultList[0], typeof(PlaylistDTO));
                Assert.AreEqual(2, result.Count());
            }
        }

        [TestMethod]
        public async Task ReturnEmptyList_When_NoMatchWithName_Genre()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnEmptyList_When_NoMatchWithName_Genre));
            TestSetup.TestInitialize();

            using (var arrangeCtx = new RidePalContext(options))
            {
                arrangeCtx.Albums.AddRange(albums);
                arrangeCtx.Artists.AddRange(artists);
                arrangeCtx.Genres.AddRange(genres);
                arrangeCtx.Tracks.AddRange(tracks);
                arrangeCtx.Users.AddRange(users);

                arrangeCtx.Playlists.AddRange(playlistsList);

                arrangeCtx.SaveChanges();
            }

            var bingMapsServicesMock = new Mock<IBingMapsServicesAsync>();
            var randomNumGenereatorMock = new Mock<Random>();
            var loggerMock = new Mock<ILogger<PlaylistServicesAsync>>();
            var pictureServicesMock = new Mock<IPictureServicesAsync>();

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new PlaylistServicesAsync(arrangeCtx, bingMapsServicesMock.Object, pictureServicesMock.Object, new Mapper(), randomNumGenereatorMock.Object, loggerMock.Object);

                //Act
                var result = await sut.GetFilteredPlaylistsAsync("Play", "4", null);
                var resultList = result.ToList();

                //Assert
                Assert.IsNotNull(result);
                Assert.AreEqual(0, result.Count());
            }
        }

        [TestMethod]
        public async Task ReturnPlaylists_When_ValidName_Genre_Duration_Passed()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnPlaylists_When_ValidName_Genre_Duration_Passed));
            TestSetup.TestInitialize();

            using (var arrangeCtx = new RidePalContext(options))
            {
                arrangeCtx.Albums.AddRange(albums);
                arrangeCtx.Artists.AddRange(artists);
                arrangeCtx.Genres.AddRange(genres);
                arrangeCtx.Tracks.AddRange(tracks);
                arrangeCtx.Users.AddRange(users);

                arrangeCtx.Playlists.AddRange(playlistsList);

                arrangeCtx.SaveChanges();
            }

            var bingMapsServicesMock = new Mock<IBingMapsServicesAsync>();
            var randomNumGenereatorMock = new Mock<Random>();
            var loggerMock = new Mock<ILogger<PlaylistServicesAsync>>();
            var pictureServicesMock = new Mock<IPictureServicesAsync>();

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new PlaylistServicesAsync(arrangeCtx, bingMapsServicesMock.Object, pictureServicesMock.Object, new Mapper(), randomNumGenereatorMock.Object, loggerMock.Object);

                //Act
                var result = await sut.GetFilteredPlaylistsAsync("Play", "1", "700");
                var resultList = result.ToList();

                //Assert
                Assert.IsNotNull(result);
                Assert.IsInstanceOfType(resultList[0], typeof(PlaylistDTO));
                Assert.AreEqual(2, result.Count());
            }
        }

        [TestMethod]
        public async Task ReturnEmptyList_When_NoMatchWithName_Genre_Duration()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnEmptyList_When_NoMatchWithName_Genre_Duration));
            TestSetup.TestInitialize();

            using (var arrangeCtx = new RidePalContext(options))
            {
                arrangeCtx.Albums.AddRange(albums);
                arrangeCtx.Artists.AddRange(artists);
                arrangeCtx.Genres.AddRange(genres);
                arrangeCtx.Tracks.AddRange(tracks);
                arrangeCtx.Users.AddRange(users);

                arrangeCtx.Playlists.AddRange(playlistsList);

                arrangeCtx.SaveChanges();
            }

            var bingMapsServicesMock = new Mock<IBingMapsServicesAsync>();
            var randomNumGenereatorMock = new Mock<Random>();
            var loggerMock = new Mock<ILogger<PlaylistServicesAsync>>();
            var pictureServicesMock = new Mock<IPictureServicesAsync>();

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new PlaylistServicesAsync(arrangeCtx, bingMapsServicesMock.Object, pictureServicesMock.Object, new Mapper(), randomNumGenereatorMock.Object, loggerMock.Object);

                //Act
                var result = await sut.GetFilteredPlaylistsAsync("Play", "1", "8");
                var resultList = result.ToList();

                //Assert
                Assert.IsNotNull(result);
                Assert.AreEqual(0, result.Count());
            }
        }

        [TestMethod]
        public async Task ReturnPlaylists_When_ValidGenrePassed()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnPlaylists_When_ValidGenrePassed));
            TestSetup.TestInitialize();

            using (var arrangeCtx = new RidePalContext(options))
            {
                arrangeCtx.Albums.AddRange(albums);
                arrangeCtx.Artists.AddRange(artists);
                arrangeCtx.Genres.AddRange(genres);
                arrangeCtx.Tracks.AddRange(tracks);
                arrangeCtx.Users.AddRange(users);

                arrangeCtx.Playlists.AddRange(playlistsList);

                arrangeCtx.SaveChanges();
            }

            var bingMapsServicesMock = new Mock<IBingMapsServicesAsync>();
            var randomNumGenereatorMock = new Mock<Random>();
            var loggerMock = new Mock<ILogger<PlaylistServicesAsync>>();
            var pictureServicesMock = new Mock<IPictureServicesAsync>();

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new PlaylistServicesAsync(arrangeCtx, bingMapsServicesMock.Object, pictureServicesMock.Object, new Mapper(), randomNumGenereatorMock.Object, loggerMock.Object);

                //Act
                var result = await sut.GetFilteredPlaylistsAsync(null, "1", null);
                var resultList = result.ToList();

                //Assert
                Assert.IsNotNull(result);
                Assert.IsInstanceOfType(resultList[0], typeof(PlaylistDTO));
                Assert.AreEqual(3, result.Count());
            }
        }

        [TestMethod]
        public async Task ReturnEmptyList_When_NoMatchWithGenre()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnEmptyList_When_NoMatchWithGenre));
            TestSetup.TestInitialize();

            using (var arrangeCtx = new RidePalContext(options))
            {
                arrangeCtx.Albums.AddRange(albums);
                arrangeCtx.Artists.AddRange(artists);
                arrangeCtx.Genres.AddRange(genres);
                arrangeCtx.Tracks.AddRange(tracks);
                arrangeCtx.Users.AddRange(users);

                arrangeCtx.Playlists.AddRange(playlistsList);

                arrangeCtx.SaveChanges();
            }

            var bingMapsServicesMock = new Mock<IBingMapsServicesAsync>();
            var randomNumGenereatorMock = new Mock<Random>();
            var loggerMock = new Mock<ILogger<PlaylistServicesAsync>>();
            var pictureServicesMock = new Mock<IPictureServicesAsync>();

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new PlaylistServicesAsync(arrangeCtx, bingMapsServicesMock.Object, pictureServicesMock.Object, new Mapper(), randomNumGenereatorMock.Object, loggerMock.Object);

                //Act
                var result = await sut.GetFilteredPlaylistsAsync(null, "6", null);
                var resultList = result.ToList();

                //Assert
                Assert.IsNotNull(result);
                Assert.AreEqual(0, result.Count());
            }
        }

        [TestMethod]
        public async Task ReturnPlaylists_When_ValidGenre_Duration_Passed()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnPlaylists_When_ValidGenre_Duration_Passed));
            TestSetup.TestInitialize();

            using (var arrangeCtx = new RidePalContext(options))
            {
                arrangeCtx.Albums.AddRange(albums);
                arrangeCtx.Artists.AddRange(artists);
                arrangeCtx.Genres.AddRange(genres);
                arrangeCtx.Tracks.AddRange(tracks);
                arrangeCtx.Users.AddRange(users);

                arrangeCtx.Playlists.AddRange(playlistsList);

                arrangeCtx.SaveChanges();
            }

            var bingMapsServicesMock = new Mock<IBingMapsServicesAsync>();
            var randomNumGenereatorMock = new Mock<Random>();
            var loggerMock = new Mock<ILogger<PlaylistServicesAsync>>();
            var pictureServicesMock = new Mock<IPictureServicesAsync>();

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new PlaylistServicesAsync(arrangeCtx, bingMapsServicesMock.Object, pictureServicesMock.Object, new Mapper(), randomNumGenereatorMock.Object, loggerMock.Object);

                //Act
                var result = await sut.GetFilteredPlaylistsAsync(null, "1", "700");
                var resultList = result.ToList();

                //Assert
                Assert.IsNotNull(result);
                Assert.IsInstanceOfType(resultList[0], typeof(PlaylistDTO));
                Assert.AreEqual(3, result.Count());
            }
        }

        [TestMethod]
        public async Task ReturnEmptyList_When_NoMatchWithGenre_Duration()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnEmptyList_When_NoMatchWithGenre_Duration));
            TestSetup.TestInitialize();

            using (var arrangeCtx = new RidePalContext(options))
            {
                arrangeCtx.Albums.AddRange(albums);
                arrangeCtx.Artists.AddRange(artists);
                arrangeCtx.Genres.AddRange(genres);
                arrangeCtx.Tracks.AddRange(tracks);
                arrangeCtx.Users.AddRange(users);

                arrangeCtx.Playlists.AddRange(playlistsList);

                arrangeCtx.SaveChanges();
            }

            var bingMapsServicesMock = new Mock<IBingMapsServicesAsync>();
            var randomNumGenereatorMock = new Mock<Random>();
            var loggerMock = new Mock<ILogger<PlaylistServicesAsync>>();
            var pictureServicesMock = new Mock<IPictureServicesAsync>();

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new PlaylistServicesAsync(arrangeCtx, bingMapsServicesMock.Object, pictureServicesMock.Object, new Mapper(), randomNumGenereatorMock.Object, loggerMock.Object);

                //Act
                var result = await sut.GetFilteredPlaylistsAsync(null, "1", "8");
                var resultList = result.ToList();

                //Assert
                Assert.IsNotNull(result);
                Assert.AreEqual(0, result.Count());
            }
        }

        [TestMethod]
        public async Task ReturnPlaylists_When_ValidDuration_Passed()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnPlaylists_When_ValidDuration_Passed));
            TestSetup.TestInitialize();

            using (var arrangeCtx = new RidePalContext(options))
            {
                arrangeCtx.Albums.AddRange(albums);
                arrangeCtx.Artists.AddRange(artists);
                arrangeCtx.Genres.AddRange(genres);
                arrangeCtx.Tracks.AddRange(tracks);
                arrangeCtx.Users.AddRange(users);

                arrangeCtx.Playlists.AddRange(playlistsList);

                arrangeCtx.SaveChanges();
            }

            var bingMapsServicesMock = new Mock<IBingMapsServicesAsync>();
            var randomNumGenereatorMock = new Mock<Random>();
            var loggerMock = new Mock<ILogger<PlaylistServicesAsync>>();
            var pictureServicesMock = new Mock<IPictureServicesAsync>();

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new PlaylistServicesAsync(arrangeCtx, bingMapsServicesMock.Object, pictureServicesMock.Object, new Mapper(), randomNumGenereatorMock.Object, loggerMock.Object);

                //Act
                var result = await sut.GetFilteredPlaylistsAsync(null, null, "700");
                var resultList = result.ToList();

                //Assert
                Assert.IsNotNull(result);
                Assert.IsInstanceOfType(resultList[0], typeof(PlaylistDTO));
                Assert.AreEqual(3, result.Count());
            }
        }

        [TestMethod]
        public async Task ReturnEmptyList_When_NoMatchWithDuration()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnEmptyList_When_NoMatchWithDuration));
            TestSetup.TestInitialize();

            using (var arrangeCtx = new RidePalContext(options))
            {
                arrangeCtx.Albums.AddRange(albums);
                arrangeCtx.Artists.AddRange(artists);
                arrangeCtx.Genres.AddRange(genres);
                arrangeCtx.Tracks.AddRange(tracks);
                arrangeCtx.Users.AddRange(users);

                arrangeCtx.Playlists.AddRange(playlistsList);

                arrangeCtx.SaveChanges();
            }

            var bingMapsServicesMock = new Mock<IBingMapsServicesAsync>();
            var randomNumGenereatorMock = new Mock<Random>();
            var loggerMock = new Mock<ILogger<PlaylistServicesAsync>>();
            var pictureServicesMock = new Mock<IPictureServicesAsync>();

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new PlaylistServicesAsync(arrangeCtx, bingMapsServicesMock.Object, pictureServicesMock.Object, new Mapper(), randomNumGenereatorMock.Object, loggerMock.Object);

                //Act
                var result = await sut.GetFilteredPlaylistsAsync(null, null, "8");
                var resultList = result.ToList();

                //Assert
                Assert.IsNotNull(result);
                Assert.AreEqual(0, result.Count());
            }
        }

        [TestMethod]
        public async Task ReturnPlaylists_When_ValidName_Duration_Passed()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnPlaylists_When_ValidName_Duration_Passed));
            TestSetup.TestInitialize();

            using (var arrangeCtx = new RidePalContext(options))
            {
                arrangeCtx.Albums.AddRange(albums);
                arrangeCtx.Artists.AddRange(artists);
                arrangeCtx.Genres.AddRange(genres);
                arrangeCtx.Tracks.AddRange(tracks);
                arrangeCtx.Users.AddRange(users);

                arrangeCtx.Playlists.AddRange(playlistsList);

                arrangeCtx.SaveChanges();
            }

            var bingMapsServicesMock = new Mock<IBingMapsServicesAsync>();
            var randomNumGenereatorMock = new Mock<Random>();
            var loggerMock = new Mock<ILogger<PlaylistServicesAsync>>();
            var pictureServicesMock = new Mock<IPictureServicesAsync>();

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new PlaylistServicesAsync(arrangeCtx, bingMapsServicesMock.Object, pictureServicesMock.Object, new Mapper(), randomNumGenereatorMock.Object, loggerMock.Object);

                //Act
                var result = await sut.GetFilteredPlaylistsAsync("Play", null, "12");
                var resultList = result.ToList();

                //Assert
                Assert.IsNotNull(result);
                Assert.IsInstanceOfType(resultList[0], typeof(PlaylistDTO));
                Assert.AreEqual(2, result.Count());
            }
        }

        [TestMethod]
        public async Task ReturnEmptyList_When_NoMatchWithName_Duration()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnEmptyList_When_NoMatchWithName_Duration));
            TestSetup.TestInitialize();

            using (var arrangeCtx = new RidePalContext(options))
            {
                arrangeCtx.Albums.AddRange(albums);
                arrangeCtx.Artists.AddRange(artists);
                arrangeCtx.Genres.AddRange(genres);
                arrangeCtx.Tracks.AddRange(tracks);
                arrangeCtx.Users.AddRange(users);

                arrangeCtx.Playlists.AddRange(playlistsList);

                arrangeCtx.SaveChanges();
            }

            var bingMapsServicesMock = new Mock<IBingMapsServicesAsync>();
            var randomNumGenereatorMock = new Mock<Random>();
            var loggerMock = new Mock<ILogger<PlaylistServicesAsync>>();
            var pictureServicesMock = new Mock<IPictureServicesAsync>();

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new PlaylistServicesAsync(arrangeCtx, bingMapsServicesMock.Object, pictureServicesMock.Object, new Mapper(), randomNumGenereatorMock.Object, loggerMock.Object);

                //Act
                var result = await sut.GetFilteredPlaylistsAsync("Play", null, "8");
                var resultList = result.ToList();

                //Assert
                Assert.IsNotNull(result);
                Assert.AreEqual(0, result.Count());
            }
        }
    }
}
