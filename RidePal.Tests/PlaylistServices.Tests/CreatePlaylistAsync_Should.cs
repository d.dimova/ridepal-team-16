﻿using MapsterMapper;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RidePal.Data.Context;
using RidePal.Data.Models;
using RidePal.Services;
using RidePal.Services.Contracts;
using RidePal.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RidePal.Tests.PlaylistServices.Tests
{
    [TestClass]
    public class CreatePlaylistAsync_Should
    {
        [TestMethod]
        public async Task ReturnNull_When_PlaylistWithSameNameExists()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnNull_When_PlaylistWithSameNameExists));

            var bingMapsServicesMock = new Mock<IBingMapsServicesAsync>();
            var randomNumGenereatorMock = new Mock<Random>();
            var loggerMock = new Mock<ILogger<PlaylistServicesAsync>>();
            var pictureServicesMock = new Mock<IPictureServicesAsync>();

            var genres = TestSetup.GetGenres();
            var artists = TestSetup.GetArtists();
            var albums = TestSetup.GetAlbums();
            var users = TestSetup.GetUsers();
            var tracks = TestSetup.GetTracks();

            using (var arrangeCtx = new RidePalContext(options))
            {
                arrangeCtx.Albums.AddRange(albums);
                arrangeCtx.Artists.AddRange(artists);
                arrangeCtx.Genres.AddRange(genres);
                arrangeCtx.Tracks.AddRange(tracks);
                arrangeCtx.Users.AddRange(users);

                arrangeCtx.Playlists.Add(new Playlist
                {
                    Name = "Playlist 1",
                    UserID = 1,
                    Genres = genres.Take(2).ToList(),
                    TracksCount = 2,
                    TotalPlaytime = 30,
                    Rank = 150
                });

                arrangeCtx.SaveChanges();
            }

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new PlaylistServicesAsync(arrangeCtx, bingMapsServicesMock.Object, pictureServicesMock.Object, new Mapper(), randomNumGenereatorMock.Object, loggerMock.Object);

                //Act
                var result = await sut.CreatePlaylistAsync(new CreatePlaylistDTO
                {
                    OriginAddress = "Sofia, BG",
                    DestinationAddress = "Pernik, BG",
                    Name = "Playlist 1",
                    GenrePercentage = new Dictionary<int, int>()
                    {
                        { 1, 100 }
                    },
                    UseTopTracks = false,
                    RepeatSameArtist = false
                }, 1);

                //Assert
                Assert.IsNull(result);
                loggerMock.VerifyLog(m => m.LogError("PlaylistServicesAsync: Playlist with the given name already exists."), Times.Once);
            }
        }

        [TestMethod]
        public async Task ReturnNull_When_DurationIsInvalid()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnNull_When_DurationIsInvalid));

            var bingMapsServicesMock = new Mock<IBingMapsServicesAsync>();
            bingMapsServicesMock.Setup(m => m.GetTravelDurationAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(-1);

            var randomNumGenereatorMock = new Mock<Random>();
            var loggerMock = new Mock<ILogger<PlaylistServicesAsync>>();
            var pictureServicesMock = new Mock<IPictureServicesAsync>();

            var genres = TestSetup.GetGenres();
            var artists = TestSetup.GetArtists();
            var albums = TestSetup.GetAlbums();
            var users = TestSetup.GetUsers();
            var tracks = TestSetup.GetTracks();

            using (var arrangeCtx = new RidePalContext(options))
            {
                arrangeCtx.Albums.AddRange(albums);
                arrangeCtx.Artists.AddRange(artists);
                arrangeCtx.Genres.AddRange(genres);
                arrangeCtx.Tracks.AddRange(tracks);
                arrangeCtx.Users.AddRange(users);

                arrangeCtx.SaveChanges();
            }

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new PlaylistServicesAsync(arrangeCtx, bingMapsServicesMock.Object, pictureServicesMock.Object, new Mapper(), randomNumGenereatorMock.Object, loggerMock.Object);

                //Act
                var result = await sut.CreatePlaylistAsync(new CreatePlaylistDTO
                {
                    OriginAddress = "Sofia, BG",
                    DestinationAddress = "Pernik, BG",
                    Name = "Playlist 1",
                    GenrePercentage = new Dictionary<int, int>()
                    {
                        { 1, 100 }
                    },
                    UseTopTracks = false,
                    RepeatSameArtist = false
                }, 1);

                //Assert
                Assert.IsNull(result);
                loggerMock.VerifyLog(m => m.LogError("PlaylistServicesAsync: Failed to create playlist. Either userID or duration is invalid."), Times.Once);
            }
        }

        [TestMethod]
        public async Task ReturnNull_When_UserIDIsInvalid()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnNull_When_UserIDIsInvalid));

            var bingMapsServicesMock = new Mock<IBingMapsServicesAsync>();
            bingMapsServicesMock.Setup(m => m.GetTravelDurationAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(300);

            var randomNumGenereatorMock = new Mock<Random>();
            var loggerMock = new Mock<ILogger<PlaylistServicesAsync>>();
            var pictureServicesMock = new Mock<IPictureServicesAsync>();

            var genres = TestSetup.GetGenres();
            var artists = TestSetup.GetArtists();
            var albums = TestSetup.GetAlbums();
            var users = TestSetup.GetUsers();
            var tracks = TestSetup.GetTracks();

            using (var arrangeCtx = new RidePalContext(options))
            {
                arrangeCtx.Albums.AddRange(albums);
                arrangeCtx.Artists.AddRange(artists);
                arrangeCtx.Genres.AddRange(genres);
                arrangeCtx.Tracks.AddRange(tracks);
                arrangeCtx.Users.AddRange(users);

                arrangeCtx.SaveChanges();
            }

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new PlaylistServicesAsync(arrangeCtx, bingMapsServicesMock.Object, pictureServicesMock.Object, new Mapper(), randomNumGenereatorMock.Object, loggerMock.Object);

                //Act
                var result = await sut.CreatePlaylistAsync(new CreatePlaylistDTO
                {
                    OriginAddress = "Sofia, BG",
                    DestinationAddress = "Pernik, BG",
                    Name = "Playlist 1",
                    GenrePercentage = new Dictionary<int, int>()
                    {
                        { 1, 100 }
                    },
                    UseTopTracks = false,
                    RepeatSameArtist = false
                }, 0);

                //Assert
                Assert.IsNull(result);
                loggerMock.VerifyLog(m => m.LogError("PlaylistServicesAsync: Failed to create playlist. Either userID or duration is invalid."), Times.Once);
            }
        }

        [TestMethod]
        public async Task ReturnNull_When_GenreIDIsInvalid()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnNull_When_GenreIDIsInvalid));

            var bingMapsServicesMock = new Mock<IBingMapsServicesAsync>();
            bingMapsServicesMock.Setup(m => m.GetTravelDurationAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(300);

            var randomNumGenereatorMock = new Mock<Random>();
            var loggerMock = new Mock<ILogger<PlaylistServicesAsync>>();
            var pictureServicesMock = new Mock<IPictureServicesAsync>();

            var genres = TestSetup.GetGenres();
            var artists = TestSetup.GetArtists();
            var albums = TestSetup.GetAlbums();
            var users = TestSetup.GetUsers();
            var tracks = TestSetup.GetTracks();

            using (var arrangeCtx = new RidePalContext(options))
            {
                arrangeCtx.Albums.AddRange(albums);
                arrangeCtx.Artists.AddRange(artists);
                arrangeCtx.Genres.AddRange(genres);
                arrangeCtx.Tracks.AddRange(tracks);
                arrangeCtx.Users.AddRange(users);

                arrangeCtx.SaveChanges();
            }

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new PlaylistServicesAsync(arrangeCtx, bingMapsServicesMock.Object, pictureServicesMock.Object, new Mapper(), randomNumGenereatorMock.Object, loggerMock.Object);

                //Act
                var result = await sut.CreatePlaylistAsync(new CreatePlaylistDTO
                {
                    OriginAddress = "Sofia, BG",
                    DestinationAddress = "Pernik, BG",
                    Name = "Playlist 1",
                    GenrePercentage = new Dictionary<int, int>()
                    {
                        { 10, 100 }
                    },
                    UseTopTracks = false,
                    RepeatSameArtist = false
                }, 1);

                //Assert
                Assert.IsNull(result);
                loggerMock.VerifyLog(m => m.LogError("PlaylistServicesAsync: Failed to insert tracks from genre. Either the genre is invalid or no tracks are found."), Times.Once);
            }
        }

        [TestMethod]
        public async Task ReturnNull_When_GenreHasNoTracks()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnNull_When_GenreHasNoTracks));

            var bingMapsServicesMock = new Mock<IBingMapsServicesAsync>();
            bingMapsServicesMock.Setup(m => m.GetTravelDurationAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(300);

            var randomNumGenereatorMock = new Mock<Random>();
            var loggerMock = new Mock<ILogger<PlaylistServicesAsync>>();
            var pictureServicesMock = new Mock<IPictureServicesAsync>();

            var genres = TestSetup.GetGenres();
            var artists = TestSetup.GetArtists();
            var albums = TestSetup.GetAlbums();
            var users = TestSetup.GetUsers();
            var tracks = TestSetup.GetTracks();

            using (var arrangeCtx = new RidePalContext(options))
            {
                arrangeCtx.Albums.AddRange(albums);
                arrangeCtx.Artists.AddRange(artists);
                arrangeCtx.Genres.AddRange(genres);
                arrangeCtx.Tracks.AddRange(tracks);
                arrangeCtx.Users.AddRange(users);

                arrangeCtx.SaveChanges();
            }

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new PlaylistServicesAsync(arrangeCtx, bingMapsServicesMock.Object, pictureServicesMock.Object, new Mapper(), randomNumGenereatorMock.Object, loggerMock.Object);

                //Act
                var result = await sut.CreatePlaylistAsync(new CreatePlaylistDTO
                {
                    OriginAddress = "Sofia, BG",
                    DestinationAddress = "Pernik, BG",
                    Name = "Playlist 1",
                    GenrePercentage = new Dictionary<int, int>()
                    {
                        { 7, 100 }
                    },
                    UseTopTracks = false,
                    RepeatSameArtist = false
                }, 1);

                //Assert
                Assert.IsNull(result);
                loggerMock.VerifyLog(m => m.LogError("PlaylistServicesAsync: Failed to insert tracks from genre. Either the genre is invalid or no tracks are found."), Times.Once);
            }
        }

        [TestMethod]
        public async Task ReturnNull_When_AdjustmentIncreaseIsImpossible_NoSuitableTracks()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnNull_When_AdjustmentIncreaseIsImpossible_NoSuitableTracks));

            var bingMapsServicesMock = new Mock<IBingMapsServicesAsync>();
            bingMapsServicesMock.Setup(m => m.GetTravelDurationAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(550);

            var randomNumGenereatorMock = new Mock<Random>();
            var loggerMock = new Mock<ILogger<PlaylistServicesAsync>>();
            var pictureServicesMock = new Mock<IPictureServicesAsync>();

            var genres = TestSetup.GetGenres();
            var artists = TestSetup.GetArtists();
            var albums = TestSetup.GetAlbums();
            var users = TestSetup.GetUsers();
            var tracks = TestSetup.GetTracks();

            using (var arrangeCtx = new RidePalContext(options))
            {
                arrangeCtx.Albums.AddRange(albums);
                arrangeCtx.Artists.AddRange(artists);
                arrangeCtx.Genres.AddRange(genres);
                arrangeCtx.Tracks.AddRange(tracks);
                arrangeCtx.Users.AddRange(users);

                arrangeCtx.SaveChanges();
            }

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new PlaylistServicesAsync(arrangeCtx, bingMapsServicesMock.Object, pictureServicesMock.Object, new Mapper(), randomNumGenereatorMock.Object, loggerMock.Object);

                //Act
                var result = await sut.CreatePlaylistAsync(new CreatePlaylistDTO
                {
                    OriginAddress = "Sofia, BG",
                    DestinationAddress = "Pernik, BG",
                    Name = "Playlist 1",
                    GenrePercentage = new Dictionary<int, int>()
                    {
                        { 1, 100 }
                    },
                    UseTopTracks = false,
                    RepeatSameArtist = false
                }, 1);

                //Assert
                Assert.IsNull(result);
                loggerMock.VerifyLog(m => m.LogError("PlaylistServicesAsync: No suitable tracks found."), Times.AtMost(2));
                loggerMock.VerifyLog(m => m.LogError("PlaylistServicesAsync: Failed to increase the duration. No more tracks are available from the given genre."), Times.Once);
            }
        }

        [TestMethod]
        public async Task ReturnNull_When_AdjustmentIncreaseIsImpossible_NoMoreTracksFromGenre()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnNull_When_AdjustmentIncreaseIsImpossible_NoMoreTracksFromGenre));

            var bingMapsServicesMock = new Mock<IBingMapsServicesAsync>();
            bingMapsServicesMock.Setup(m => m.GetTravelDurationAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(1500);

            var randomNumGenereatorMock = new Mock<Random>();
            var loggerMock = new Mock<ILogger<PlaylistServicesAsync>>();
            var pictureServicesMock = new Mock<IPictureServicesAsync>();

            var genres = TestSetup.GetGenres();
            var artists = TestSetup.GetArtists();
            var albums = TestSetup.GetAlbums();
            var users = TestSetup.GetUsers();
            var tracks = TestSetup.GetTracks();

            using (var arrangeCtx = new RidePalContext(options))
            {
                arrangeCtx.Albums.AddRange(albums);
                arrangeCtx.Artists.AddRange(artists);
                arrangeCtx.Genres.AddRange(genres);
                arrangeCtx.Tracks.AddRange(tracks);
                arrangeCtx.Users.AddRange(users);

                arrangeCtx.SaveChanges();
            }

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new PlaylistServicesAsync(arrangeCtx, bingMapsServicesMock.Object, pictureServicesMock.Object, new Mapper(), randomNumGenereatorMock.Object, loggerMock.Object);

                //Act
                var result = await sut.CreatePlaylistAsync(new CreatePlaylistDTO
                {
                    OriginAddress = "Sofia, BG",
                    DestinationAddress = "Pernik, BG",
                    Name = "Playlist 1",
                    GenrePercentage = new Dictionary<int, int>()
                    {
                        { 1, 100 }
                    },
                    UseTopTracks = false,
                    RepeatSameArtist = true
                }, 1);

                //Assert
                Assert.IsNull(result);
                loggerMock.VerifyLog(m => m.LogError("PlaylistServicesAsync: No suitable tracks found."), Times.AtMost(2));
                loggerMock.VerifyLog(m => m.LogError("PlaylistServicesAsync: Failed to adjust the duration. Either no such genre exists or no more tracks are available from the given genre."), Times.Once);
            }
        }

        [TestMethod]
        public async Task ReturnPlaylist_When_SameArtistIsTrue()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnPlaylist_When_SameArtistIsTrue));

            var bingMapsServicesMock = new Mock<IBingMapsServicesAsync>();
            bingMapsServicesMock.Setup(m => m.GetTravelDurationAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(700);

            var randomNumGenereatorMock = new Mock<Random>();
            var loggerMock = new Mock<ILogger<PlaylistServicesAsync>>();
            var pictureServicesMock = new Mock<IPictureServicesAsync>();
            pictureServicesMock.Setup(m => m.GetRandomPicAsync()).ReturnsAsync("http://playlist.pic.com/playlist_1.png");

            var genres = TestSetup.GetGenres();
            var artists = TestSetup.GetArtists();
            var albums = TestSetup.GetAlbums();
            var users = TestSetup.GetUsers();
            var tracks = TestSetup.GetTracks();

            using (var arrangeCtx = new RidePalContext(options))
            {
                arrangeCtx.Albums.AddRange(albums);
                arrangeCtx.Artists.AddRange(artists);
                arrangeCtx.Genres.AddRange(genres);
                arrangeCtx.Tracks.AddRange(tracks);
                arrangeCtx.Users.AddRange(users);

                arrangeCtx.SaveChanges();
            }

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new PlaylistServicesAsync(arrangeCtx, bingMapsServicesMock.Object, pictureServicesMock.Object, new Mapper(), randomNumGenereatorMock.Object, loggerMock.Object);

                //Act
                var result = await sut.CreatePlaylistAsync(new CreatePlaylistDTO
                {
                    OriginAddress = "Sofia, BG",
                    DestinationAddress = "Pernik, BG",
                    Name = "Playlist 1",
                    GenrePercentage = new Dictionary<int, int>()
                    {
                        { 1, 100 }
                    },
                    UseTopTracks = false,
                    RepeatSameArtist = true
                }, 1);

                //Assert
                Assert.IsNotNull(result);
                Assert.IsInstanceOfType(result, typeof(SinglePlaylistDTO));
                Assert.AreEqual(1, result.ID);
                Assert.AreEqual("Playlist 1", result.Name);
            }
        }

        [TestMethod]
        public async Task ReturnPlaylist_When_TopTrackstIsTrue()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnPlaylist_When_TopTrackstIsTrue));

            var bingMapsServicesMock = new Mock<IBingMapsServicesAsync>();
            bingMapsServicesMock.Setup(m => m.GetTravelDurationAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(500);

            var randomNumGenereatorMock = new Mock<Random>();
            var loggerMock = new Mock<ILogger<PlaylistServicesAsync>>();
            var pictureServicesMock = new Mock<IPictureServicesAsync>();
            pictureServicesMock.Setup(m => m.GetRandomPicAsync()).ReturnsAsync("http://playlist.pic.com/playlist_1.png");

            var genres = TestSetup.GetGenres();
            var artists = TestSetup.GetArtists();
            var albums = TestSetup.GetAlbums();
            var users = TestSetup.GetUsers();
            var tracks = TestSetup.GetTracks();

            using (var arrangeCtx = new RidePalContext(options))
            {
                arrangeCtx.Albums.AddRange(albums);
                arrangeCtx.Artists.AddRange(artists);
                arrangeCtx.Genres.AddRange(genres);
                arrangeCtx.Tracks.AddRange(tracks);
                arrangeCtx.Users.AddRange(users);

                arrangeCtx.SaveChanges();
            }

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new PlaylistServicesAsync(arrangeCtx, bingMapsServicesMock.Object, pictureServicesMock.Object, new Mapper(), randomNumGenereatorMock.Object, loggerMock.Object);

                //Act
                var result = await sut.CreatePlaylistAsync(new CreatePlaylistDTO
                {
                    OriginAddress = "Sofia, BG",
                    DestinationAddress = "Pernik, BG",
                    Name = "Playlist 1",
                    GenrePercentage = new Dictionary<int, int>()
                    {
                        { 1, 100 }
                    },
                    UseTopTracks = true,
                    RepeatSameArtist = false
                }, 1);

                //Assert
                Assert.IsNotNull(result);
                Assert.IsInstanceOfType(result, typeof(SinglePlaylistDTO));
                Assert.AreEqual(1, result.ID);
                Assert.AreEqual("Playlist 1", result.Name);
            }
        }

        [TestMethod]
        public async Task ReturnPlaylist_When_BothParamsAreTrue()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnPlaylist_When_BothParamsAreTrue));

            var bingMapsServicesMock = new Mock<IBingMapsServicesAsync>();
            bingMapsServicesMock.Setup(m => m.GetTravelDurationAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(700);

            var randomNumGenereatorMock = new Mock<Random>();
            var loggerMock = new Mock<ILogger<PlaylistServicesAsync>>();
            var pictureServicesMock = new Mock<IPictureServicesAsync>();
            pictureServicesMock.Setup(m => m.GetRandomPicAsync()).ReturnsAsync("http://playlist.pic.com/playlist_1.png");

            var genres = TestSetup.GetGenres();
            var artists = TestSetup.GetArtists();
            var albums = TestSetup.GetAlbums();
            var users = TestSetup.GetUsers();
            var tracks = TestSetup.GetTracks();

            using (var arrangeCtx = new RidePalContext(options))
            {
                arrangeCtx.Albums.AddRange(albums);
                arrangeCtx.Artists.AddRange(artists);
                arrangeCtx.Genres.AddRange(genres);
                arrangeCtx.Tracks.AddRange(tracks);
                arrangeCtx.Users.AddRange(users);

                arrangeCtx.SaveChanges();
            }

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new PlaylistServicesAsync(arrangeCtx, bingMapsServicesMock.Object, pictureServicesMock.Object, new Mapper(), randomNumGenereatorMock.Object, loggerMock.Object);

                //Act
                var result = await sut.CreatePlaylistAsync(new CreatePlaylistDTO
                {
                    OriginAddress = "Sofia, BG",
                    DestinationAddress = "Pernik, BG",
                    Name = "Playlist 1",
                    GenrePercentage = new Dictionary<int, int>()
                    {
                        { 1, 100 }
                    },
                    UseTopTracks = true,
                    RepeatSameArtist = true
                }, 1);

                //Assert
                Assert.IsNotNull(result);
                Assert.IsInstanceOfType(result, typeof(SinglePlaylistDTO));
                Assert.AreEqual(1, result.ID);
                Assert.AreEqual("Playlist 1", result.Name);
            }
        }
    }
}
