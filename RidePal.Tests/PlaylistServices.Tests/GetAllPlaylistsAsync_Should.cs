﻿using MapsterMapper;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RidePal.Data.Context;
using RidePal.Data.Models;
using RidePal.Services;
using RidePal.Services.Contracts;
using RidePal.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RidePal.Tests.PlaylistServices.Tests
{
    [TestClass]
    public class GetAllPlaylistsAsync_Should
    {
        [TestMethod]
        public async Task ReturnEmptyCollection_When_NoRecordsInDatabase()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnEmptyCollection_When_NoRecordsInDatabase));

            var bingMapsServicesMock = new Mock<IBingMapsServicesAsync>();
            var randomNumGenereatorMock = new Mock<Random>();
            var loggerMock = new Mock<ILogger<PlaylistServicesAsync>>();
            var pictureServicesMock = new Mock<IPictureServicesAsync>();

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new PlaylistServicesAsync(arrangeCtx, bingMapsServicesMock.Object, pictureServicesMock.Object, new Mapper(), randomNumGenereatorMock.Object, loggerMock.Object);

                //Act
                var result = await sut.GetAllPlaylistsAsync();

                //Assert
                Assert.IsTrue(result.Count() == 0);
            }
        }

        [TestMethod]
        public async Task ReturnValidPlaylistCollection_When_PlaylistsExist()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnValidPlaylistCollection_When_PlaylistsExist));

            TestSetup.TestInitialize();

            var albums = TestSetup.GetAlbums();
            var artists = TestSetup.GetArtists();
            var genres = TestSetup.GetGenres();
            var tracks = TestSetup.GetTracks();
            var users = TestSetup.GetUsers();

            var playlistTracks = new List<Track> { tracks[0], tracks[5], tracks[3] };

            var playlistDTO = new PlaylistDTO
            {
                Name = "Playlist 1",
                UserID = 1,
                UserName = "Georgi",
                Genres = new string[] { "Pop", "R&B" },
                TracksCount = playlistTracks.Count,
                TotalPlaytime = playlistTracks.Sum(track => track.Duration),
                Rank = playlistTracks.Average(track => track.Rank)
            };

            using (var arrangeCtx = new RidePalContext(options))
            {
                arrangeCtx.Albums.AddRange(albums);
                arrangeCtx.Artists.AddRange(artists);
                arrangeCtx.Genres.AddRange(genres);
                arrangeCtx.Tracks.AddRange(tracks);
                arrangeCtx.Users.AddRange(users);

                arrangeCtx.Playlists.Add(new Playlist
                {
                    Name = "Playlist 1",
                    UserID = 1,
                    Tracks = playlistTracks,
                    Genres = genres.Take(2).ToList(),
                    TracksCount = playlistTracks.Count,
                    TotalPlaytime = playlistTracks.Sum(track => track.Duration),
                    Rank = playlistTracks.Average(track => track.Rank)
                });

                arrangeCtx.SaveChanges();
            }

            var bingMapsServicesMock = new Mock<IBingMapsServicesAsync>();
            var randomNumGenereatorMock = new Mock<Random>();
            var loggerMock = new Mock<ILogger<PlaylistServicesAsync>>();
            var pictureServicesMock = new Mock<IPictureServicesAsync>();

            using (var assertCtx = new RidePalContext(options))
            {
                var sut = new PlaylistServicesAsync(assertCtx, bingMapsServicesMock.Object, pictureServicesMock.Object , new Mapper(), randomNumGenereatorMock.Object, loggerMock.Object);

                //Act
                var result = await sut.GetAllPlaylistsAsync();

                //Assert
                Assert.IsTrue(result.Count() == 1);
                Assert.IsInstanceOfType(result.ToList()[0], typeof(PlaylistDTO));
            }
        }
    }
}
