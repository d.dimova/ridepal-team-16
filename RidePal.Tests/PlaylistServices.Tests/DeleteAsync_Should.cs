﻿using MapsterMapper;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RidePal.Data.Context;
using RidePal.Data.Models;
using RidePal.Services;
using RidePal.Services.Contracts;
using RidePal.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RidePal.Tests.PlaylistServices.Tests
{
    [TestClass]
    public class DeleteAsync_Should
    {
        [TestMethod]
        public async Task ReturnFalse_WhenNoPlaylistToDeleteFound()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnFalse_WhenNoPlaylistToDeleteFound));

            var bingMapsServicesMock = new Mock<IBingMapsServicesAsync>();
            var randomNumGenereatorMock = new Mock<Random>();
            var loggerMock = new Mock<ILogger<PlaylistServicesAsync>>();
            var pictureServicesMock = new Mock<IPictureServicesAsync>();

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new PlaylistServicesAsync(arrangeCtx, bingMapsServicesMock.Object, pictureServicesMock.Object, new Mapper(), randomNumGenereatorMock.Object, loggerMock.Object);

                //Act
                var result = await sut.DeleteAsync(1);

                //Assert
                Assert.IsFalse(result);
            }
        }

        [TestMethod]
        public async Task ReturnTrue_WhenPlaylistIsDeleted()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnTrue_WhenPlaylistIsDeleted));

            var albums = TestSetup.GetAlbums();
            var artists = TestSetup.GetArtists();
            var genres = TestSetup.GetGenres();
            var tracks = TestSetup.GetTracks();
            var users = TestSetup.GetUsers();

            var playlistTracks = new List<Track> { tracks[0], tracks[5], tracks[3] };

            using (var arrangeCtx = new RidePalContext(options))
            {
                arrangeCtx.Albums.AddRange(albums);
                arrangeCtx.Artists.AddRange(artists);
                arrangeCtx.Genres.AddRange(genres);
                arrangeCtx.Tracks.AddRange(tracks);
                arrangeCtx.Users.AddRange(users);

                arrangeCtx.Playlists.Add(new Playlist
                {
                    Name = "Playlist 1",
                    UserID = 1,
                    Tracks = playlistTracks,
                    Genres = genres.Take(2).ToList(),
                    TracksCount = playlistTracks.Count,
                    TotalPlaytime = playlistTracks.Sum(track => track.Duration),
                    Rank = playlistTracks.Average(track => track.Rank)
                });

                arrangeCtx.SaveChanges();
            }

            var bingMapsServicesMock = new Mock<IBingMapsServicesAsync>();
            var randomNumGenereatorMock = new Mock<Random>();
            var loggerMock = new Mock<ILogger<PlaylistServicesAsync>>();
            var pictureServicesMock = new Mock<IPictureServicesAsync>();

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new PlaylistServicesAsync(arrangeCtx, bingMapsServicesMock.Object, pictureServicesMock.Object, new Mapper(), randomNumGenereatorMock.Object, loggerMock.Object);

                //Act
                var result = await sut.DeleteAsync(1);

                var deleteResult = await arrangeCtx.Playlists.FindAsync(1);

                //Assert
                Assert.IsTrue(result);
                Assert.IsTrue(deleteResult.IsDeleted);
                Assert.IsNotNull(deleteResult.DeletedOn);
            }
        }
    }
}
