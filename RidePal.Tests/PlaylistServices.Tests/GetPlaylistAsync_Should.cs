﻿using MapsterMapper;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RidePal.Data.Context;
using RidePal.Data.Models;
using RidePal.Services;
using RidePal.Services.Contracts;
using RidePal.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RidePal.Tests.PlaylistServices.Tests
{
    [TestClass]
    public class GetPlaylistAsync_Should
    {
        [TestMethod]
        public async Task ReturnNull_When_NoRecordsInDatabase()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnNull_When_NoRecordsInDatabase));

            var bingMapsServicesMock = new Mock<IBingMapsServicesAsync>();
            var randomNumGenereatorMock = new Mock<Random>();
            var loggerMock = new Mock<ILogger<PlaylistServicesAsync>>();
            var pictureServicesMock = new Mock<IPictureServicesAsync>();

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new PlaylistServicesAsync(arrangeCtx, bingMapsServicesMock.Object, pictureServicesMock.Object, new Mapper(), randomNumGenereatorMock.Object, loggerMock.Object);

                //Act
                var result = await sut.GetPlaylistAsync(1);

                //Assert
                Assert.IsNull(result);
            }
        }

        [TestMethod]
        public async Task ReturnValidPlaylist_When_PlaylistExist()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnValidPlaylist_When_PlaylistExist));

            TestSetup.TestInitialize();

            var albums = TestSetup.GetAlbums();
            var artists = TestSetup.GetArtists();
            var genres = TestSetup.GetGenres();
            var tracks = TestSetup.GetTracks();
            var users = TestSetup.GetUsers();

            var playlistTracks = new List<Track> { tracks[0], tracks[5], tracks[3] };

            var playlistDTO = new SinglePlaylistDTO
            {
                Name = "Playlist 1",
                UserID = 1,
                UserName = "Georgi",
                Genres = new string[] { "Pop", "R&B" },
                TracksCount = playlistTracks.Count,
                TotalPlaytime = playlistTracks.Sum(track => track.Duration),
                Rank = playlistTracks.Average(track => track.Rank),
                Tracks = playlistTracks.Select(record => new TrackDTO
                {
                    ID = record.ID,
                    Title = record.Title,
                    Duration = record.Duration,
                    Rank = record.Rank,
                    ReleaseDate = record.ReleaseDate,
                    Preview = record.Preview,
                    TrackLink = record.TrackLink,
                    AlbumID = record.AlbumID,
                    ArtistID = record.ArtistID,
                    GenreID = record.GenreID
                }).ToList()
            };

            using (var arrangeCtx = new RidePalContext(options))
            {
                arrangeCtx.Albums.AddRange(albums);
                arrangeCtx.Artists.AddRange(artists);
                arrangeCtx.Genres.AddRange(genres);
                arrangeCtx.Tracks.AddRange(tracks);
                arrangeCtx.Users.AddRange(users);

                arrangeCtx.Playlists.Add(new Playlist
                {
                    Name = "Playlist 1",
                    UserID = 1,
                    Tracks = playlistTracks,
                    Genres = genres.Take(2).ToList(),
                    TracksCount = playlistTracks.Count,
                    TotalPlaytime = playlistTracks.Sum(track => track.Duration),
                    Rank = playlistTracks.Average(track => track.Rank)
                });

                arrangeCtx.SaveChanges();
            }

            var bingMapsServicesMock = new Mock<IBingMapsServicesAsync>();
            var randomNumGenereatorMock = new Mock<Random>();
            var loggerMock = new Mock<ILogger<PlaylistServicesAsync>>();
            var pictureServicesMock = new Mock<IPictureServicesAsync>();

            using (var assertCtx = new RidePalContext(options))
            {
                var sut = new PlaylistServicesAsync(assertCtx, bingMapsServicesMock.Object, pictureServicesMock.Object, new Mapper(), randomNumGenereatorMock.Object, loggerMock.Object);

                //Act
                var result = await sut.GetPlaylistAsync(1);

                //Assert
                Assert.IsNotNull(result);
                Assert.IsInstanceOfType(result, typeof(SinglePlaylistDTO));
                Assert.AreEqual(playlistTracks.Count, result.Tracks.Count);
            }
        }
    }
}
