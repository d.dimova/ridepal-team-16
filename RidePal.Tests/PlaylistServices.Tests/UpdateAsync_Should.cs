﻿using MapsterMapper;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RidePal.Data.Context;
using RidePal.Data.Models;
using RidePal.Services;
using RidePal.Services.Contracts;
using RidePal.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RidePal.Tests.PlaylistServices.Tests
{
    [TestClass]
    public class UpdateAsync_Should
    {
        [TestMethod]
        public async Task ReturnFalse_WhenNoPlaylistToUpdateFound()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnFalse_WhenNoPlaylistToUpdateFound));

            var bingMapsServicesMock = new Mock<IBingMapsServicesAsync>();
            var randomNumGenereatorMock = new Mock<Random>();
            var loggerMock = new Mock<ILogger<PlaylistServicesAsync>>();
            var pictureServicesMock = new Mock<IPictureServicesAsync>();

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new PlaylistServicesAsync(arrangeCtx, bingMapsServicesMock.Object, pictureServicesMock.Object, new Mapper(), randomNumGenereatorMock.Object, loggerMock.Object);

                //Act
                var result = await sut.UpdateAsync(1, new UpdatePlaylistDTO());

                //Assert
                Assert.IsFalse(result);
            }
        }

        [TestMethod]
        public async Task ReturnTrue_WhenPlaylistNameIsUpdated()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnTrue_WhenPlaylistNameIsUpdated));

            var albums = TestSetup.GetAlbums();
            var artists = TestSetup.GetArtists();
            var genres = TestSetup.GetGenres();
            var tracks = TestSetup.GetTracks();
            var users = TestSetup.GetUsers();

            var playlistTracks = new List<Track> { tracks[0], tracks[5], tracks[3] };

            var playlistDTO = new PlaylistDTO
            {
                Name = "New awesome playlist",
                UserID = 1,
                UserName = "Georgi",
                Genres = new string[] { "Pop", "R&B" },
                TracksCount = playlistTracks.Count,
                TotalPlaytime = playlistTracks.Sum(track => track.Duration),
                Rank = playlistTracks.Average(track => track.Rank)
            };

            using (var arrangeCtx = new RidePalContext(options))
            {
                arrangeCtx.Albums.AddRange(albums);
                arrangeCtx.Artists.AddRange(artists);
                arrangeCtx.Genres.AddRange(genres);
                arrangeCtx.Tracks.AddRange(tracks);
                arrangeCtx.Users.AddRange(users);

                arrangeCtx.Playlists.Add(new Playlist
                {
                    Name = "Playlist 1",
                    UserID = 1,
                    Tracks = playlistTracks,
                    Genres = genres.Take(2).ToList(),
                    TracksCount = playlistTracks.Count,
                    TotalPlaytime = playlistTracks.Sum(track => track.Duration),
                    Rank = playlistTracks.Average(track => track.Rank)
                });

                arrangeCtx.SaveChanges();
            }

            var updateDTO = new UpdatePlaylistDTO { Name = "New awesome playlist", PictureUrl = "" };

            var bingMapsServicesMock = new Mock<IBingMapsServicesAsync>();
            var randomNumGenereatorMock = new Mock<Random>();
            var loggerMock = new Mock<ILogger<PlaylistServicesAsync>>();
            var pictureServicesMock = new Mock<IPictureServicesAsync>();

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new PlaylistServicesAsync(arrangeCtx, bingMapsServicesMock.Object, pictureServicesMock.Object, new Mapper(), randomNumGenereatorMock.Object, loggerMock.Object);

                //Act
                var result = await sut.UpdateAsync(1, updateDTO);

                var updatedResult = await arrangeCtx.Playlists.FindAsync(1);

                //Assert
                Assert.IsTrue(result);
                Assert.AreEqual(updateDTO.Name, updatedResult.Name);
                Assert.IsTrue(updatedResult.PictureUrl != "");
            }
        }

        [TestMethod]
        public async Task ReturnTrue_WhenPlaylistNameAndPictureUrlIsUpdated()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnTrue_WhenPlaylistNameAndPictureUrlIsUpdated));

            var albums = TestSetup.GetAlbums();
            var artists = TestSetup.GetArtists();
            var genres = TestSetup.GetGenres();
            var tracks = TestSetup.GetTracks();
            var users = TestSetup.GetUsers();

            var playlistTracks = new List<Track> { tracks[0], tracks[5], tracks[3] };

            var playlistDTO = new PlaylistDTO
            {
                Name = "New awesome playlist",
                UserID = 1,
                UserName = "Georgi",
                Genres = new string[] { "Pop", "R&B" },
                TracksCount = playlistTracks.Count,
                TotalPlaytime = playlistTracks.Sum(track => track.Duration),
                Rank = playlistTracks.Average(track => track.Rank),
                PictureUrl = "/assets/images/_playlist_1.png"
            };

            using (var arrangeCtx = new RidePalContext(options))
            {
                arrangeCtx.Albums.AddRange(albums);
                arrangeCtx.Artists.AddRange(artists);
                arrangeCtx.Genres.AddRange(genres);
                arrangeCtx.Tracks.AddRange(tracks);
                arrangeCtx.Users.AddRange(users);

                arrangeCtx.Playlists.Add(new Playlist
                {
                    Name = "Playlist 1",
                    UserID = 1,
                    Tracks = playlistTracks,
                    Genres = genres.Take(2).ToList(),
                    TracksCount = playlistTracks.Count,
                    TotalPlaytime = playlistTracks.Sum(track => track.Duration),
                    Rank = playlistTracks.Average(track => track.Rank)
                });

                arrangeCtx.SaveChanges();
            }

            var updateDTO = new UpdatePlaylistDTO { Name = "New awesome playlist", PictureUrl = "/assets/images/_playlist_1.png" };

            var bingMapsServicesMock = new Mock<IBingMapsServicesAsync>();
            var randomNumGenereatorMock = new Mock<Random>();
            var loggerMock = new Mock<ILogger<PlaylistServicesAsync>>();
            var pictureServicesMock = new Mock<IPictureServicesAsync>();

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new PlaylistServicesAsync(arrangeCtx, bingMapsServicesMock.Object, pictureServicesMock.Object, new Mapper(), randomNumGenereatorMock.Object, loggerMock.Object);

                //Act
                var result = await sut.UpdateAsync(1, updateDTO);

                var updatedResult = await arrangeCtx.Playlists.FindAsync(1);

                //Assert
                Assert.IsTrue(result);
                Assert.AreEqual(updateDTO.Name, updatedResult.Name);
                Assert.AreEqual(updateDTO.PictureUrl, updatedResult.PictureUrl);
            }
        }
    }
}
