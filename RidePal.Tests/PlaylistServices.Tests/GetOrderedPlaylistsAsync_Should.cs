﻿using MapsterMapper;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RidePal.Data.Context;
using RidePal.Data.Models;
using RidePal.Services;
using RidePal.Services.Contracts;
using RidePal.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RidePal.Tests.PlaylistServices.Tests
{
    [TestClass]
    public class GetOrderedPlaylistsAsync_Should
    {
        [TestMethod]
        public async Task ReturnEmptyCollection_When_NoRecordsInDatabase_Ordered()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnEmptyCollection_When_NoRecordsInDatabase_Ordered));

            var bingMapsServicesMock = new Mock<IBingMapsServicesAsync>();
            var randomNumGenereatorMock = new Mock<Random>();
            var loggerMock = new Mock<ILogger<PlaylistServicesAsync>>();
            var pictureServicesMock = new Mock<IPictureServicesAsync>();

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new PlaylistServicesAsync(arrangeCtx, bingMapsServicesMock.Object, pictureServicesMock.Object, new Mapper(), randomNumGenereatorMock.Object, loggerMock.Object);

                //Act
                var result = await sut.GetOrderedPlaylistsAsync("asc");

                //Assert
                Assert.IsTrue(result.Count() == 0);
            }
        }

        [TestMethod]
        public async Task ReturnValidOrderedAscPlaylistCollection_When_PlaylistsExist()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnValidOrderedAscPlaylistCollection_When_PlaylistsExist));

            TestSetup.TestInitialize();

            var albums = TestSetup.GetAlbums();
            var artists = TestSetup.GetArtists();
            var genres = TestSetup.GetGenres();
            var tracks = TestSetup.GetTracks();
            var users = TestSetup.GetUsers();

            var playlistTracks1 = new List<Track> { tracks[0], tracks[5], tracks[3] }; //AvgRank: 352 481, 129 463, 214 704 = 232 216
            var playlistTracks2 = new List<Track> { tracks[1], tracks[6], tracks[4] }; //AvgRank: 976 542, 379 429, 624 752 = 660 241
            var playlistTracks3 = new List<Track> { tracks[2], tracks[7], tracks[5] }; //AvgRank: 923 645, 243 579, 129 463 = 432 229

            using (var arrangeCtx = new RidePalContext(options))
            {
                arrangeCtx.Albums.AddRange(albums);
                arrangeCtx.Artists.AddRange(artists);
                arrangeCtx.Genres.AddRange(genres);
                arrangeCtx.Tracks.AddRange(tracks);
                arrangeCtx.Users.AddRange(users);

                arrangeCtx.Playlists.AddRange(new Playlist[]
                {
                    new Playlist
                    {
                        Name = "Playlist 1",
                        UserID = 1,
                        Tracks = playlistTracks1,
                        Genres = genres.Take(2).ToList(),
                        TracksCount = playlistTracks1.Count,
                        TotalPlaytime = playlistTracks1.Sum(track => track.Duration),
                        Rank = playlistTracks1.Average(track => track.Rank)
                    },
                    new Playlist
                    {
                        Name = "Playlist 2",
                        UserID = 2,
                        Tracks = playlistTracks2,
                        Genres = genres.Take(3).ToList(),
                        TracksCount = playlistTracks2.Count,
                        TotalPlaytime = playlistTracks2.Sum(track => track.Duration),
                        Rank = playlistTracks2.Average(track => track.Rank)
                    },
                    new Playlist
                    {
                        Name = "Playlist 3",
                        UserID = 3,
                        Tracks = playlistTracks3,
                        Genres = genres.Take(3).ToList(),
                        TracksCount = playlistTracks3.Count,
                        TotalPlaytime = playlistTracks3.Sum(track => track.Duration),
                        Rank = playlistTracks3.Average(track => track.Rank)
                    },
                });

                arrangeCtx.SaveChanges();
            }

            var bingMapsServicesMock = new Mock<IBingMapsServicesAsync>();
            var randomNumGenereatorMock = new Mock<Random>();
            var loggerMock = new Mock<ILogger<PlaylistServicesAsync>>();
            var pictureServicesMock = new Mock<IPictureServicesAsync>();

            using (var assertCtx = new RidePalContext(options))
            {
                var sut = new PlaylistServicesAsync(assertCtx, bingMapsServicesMock.Object, pictureServicesMock.Object, new Mapper(), randomNumGenereatorMock.Object, loggerMock.Object);

                //Act
                var result = await sut.GetOrderedPlaylistsAsync("asc");
                var listResult = result.ToList();

                //Assert
                Assert.IsTrue(result.Count() == 3);
                Assert.AreEqual(1, listResult[0].ID);
                Assert.AreEqual(3, listResult[1].ID);
                Assert.AreEqual(2, listResult[2].ID);
            }
        }

        [TestMethod]
        public async Task ReturnValidOrderedDescPlaylistCollection_When_PlaylistsExist()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnValidOrderedDescPlaylistCollection_When_PlaylistsExist));

            TestSetup.TestInitialize();

            var albums = TestSetup.GetAlbums();
            var artists = TestSetup.GetArtists();
            var genres = TestSetup.GetGenres();
            var tracks = TestSetup.GetTracks();
            var users = TestSetup.GetUsers();

            var playlistTracks1 = new List<Track> { tracks[0], tracks[5], tracks[3] }; //AvgRank: 352 481, 129 463, 214 704 = 232 216
            var playlistTracks2 = new List<Track> { tracks[1], tracks[6], tracks[4] }; //AvgRank: 976 542, 379 429, 624 752 = 660 241
            var playlistTracks3 = new List<Track> { tracks[2], tracks[7], tracks[5] }; //AvgRank: 923 645, 243 579, 129 463 = 432 229

            using (var arrangeCtx = new RidePalContext(options))
            {
                arrangeCtx.Albums.AddRange(albums);
                arrangeCtx.Artists.AddRange(artists);
                arrangeCtx.Genres.AddRange(genres);
                arrangeCtx.Tracks.AddRange(tracks);
                arrangeCtx.Users.AddRange(users);

                arrangeCtx.Playlists.AddRange(new Playlist[]
                {
                    new Playlist
                    {
                        Name = "Playlist 1",
                        UserID = 1,
                        Tracks = playlistTracks1,
                        Genres = genres.Take(2).ToList(),
                        TracksCount = playlistTracks1.Count,
                        TotalPlaytime = playlistTracks1.Sum(track => track.Duration),
                        Rank = playlistTracks1.Average(track => track.Rank)
                    },
                    new Playlist
                    {
                        Name = "Playlist 2",
                        UserID = 2,
                        Tracks = playlistTracks2,
                        Genres = genres.Take(3).ToList(),
                        TracksCount = playlistTracks2.Count,
                        TotalPlaytime = playlistTracks2.Sum(track => track.Duration),
                        Rank = playlistTracks2.Average(track => track.Rank)
                    },
                    new Playlist
                    {
                        Name = "Playlist 3",
                        UserID = 3,
                        Tracks = playlistTracks3,
                        Genres = genres.Take(3).ToList(),
                        TracksCount = playlistTracks3.Count,
                        TotalPlaytime = playlistTracks3.Sum(track => track.Duration),
                        Rank = playlistTracks3.Average(track => track.Rank)
                    },
                });

                arrangeCtx.SaveChanges();
            }

            var bingMapsServicesMock = new Mock<IBingMapsServicesAsync>();
            var randomNumGenereatorMock = new Mock<Random>();
            var loggerMock = new Mock<ILogger<PlaylistServicesAsync>>();
            var pictureServicesMock = new Mock<IPictureServicesAsync>();

            using (var assertCtx = new RidePalContext(options))
            {
                var sut = new PlaylistServicesAsync(assertCtx, bingMapsServicesMock.Object, pictureServicesMock.Object, new Mapper(), randomNumGenereatorMock.Object, loggerMock.Object);

                //Act
                var result = await sut.GetOrderedPlaylistsAsync("desc");
                var listResult = result.ToList();

                //Assert
                Assert.IsTrue(result.Count() == 3);
                Assert.AreEqual(2, listResult[0].ID);
                Assert.AreEqual(3, listResult[1].ID);
                Assert.AreEqual(1, listResult[2].ID);
            }
        }
    }
}
