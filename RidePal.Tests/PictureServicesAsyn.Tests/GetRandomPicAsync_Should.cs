﻿using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;
using RidePal.Services;
using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace RidePal.Tests.PictureServicesAsyn.Tests
{
    [TestClass]
    public class GetRandomPicAsync_Should
    {
        [TestMethod]
        public async Task ThrowArgumentNullException_WhenRequestCodeIsNotSuccess()
        {
            // Arrange
            var mockFactory = new Mock<IHttpClientFactory>();

            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            handlerMock
               .Protected()
               .Setup<Task<HttpResponseMessage>>(
                  "SendAsync",
                  ItExpr.IsAny<HttpRequestMessage>(),
                  ItExpr.IsAny<CancellationToken>()
               )
               .ReturnsAsync(new HttpResponseMessage()
               {
                   StatusCode = HttpStatusCode.BadRequest,
                   Content = new StringContent("")
               })
               .Verifiable();

            var httpClient = new HttpClient(handlerMock.Object);
            mockFactory.Setup(method => method.CreateClient(It.IsAny<string>())).Returns(httpClient);

            var loggerMock = new Mock<ILogger<PictureServicesAsync>>();

            //Act
            var sut = new PictureServicesAsync(mockFactory.Object, loggerMock.Object);

            //Assert
            await Assert.ThrowsExceptionAsync<ArgumentException>(async () => await sut.GetRandomPicAsync());
        }

        [TestMethod]
        public async Task ReturnsStringUrl_WhenStatusCodeIsSucces()
        {
            // Arrange
            var mockFactory = new Mock<IHttpClientFactory>();

            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            handlerMock
               .Protected()
               .Setup<Task<HttpResponseMessage>>(
                  "SendAsync",
                  ItExpr.IsAny<HttpRequestMessage>(),
                  ItExpr.IsAny<CancellationToken>()
               )
               .ReturnsAsync(new HttpResponseMessage()
               {
                   StatusCode = HttpStatusCode.OK,
                   Content = new StringContent("{\"id\":\"UDCSlDDcJVU\",\"created_at\":\"2020-10-21T04:58:01-04:00\",\"updated_at\":\"2020-11-18T08:25:56-05:00\",\"promoted_at\":\"2020-10-21T15:33:01-04:00\",\"width\":4928,\"height\":3280,\"color\":\"#DEA95D\",\"blur_hash\":\"L96*q7tTIUR+yGkYj=s:9Faexuof\",\"description\":null,\"alt_description\":\"brown rock formation near body of water during night time\",\"urls\":{\"raw\":\"https://images.unsplash.com/photo-1603270650322-f7aa3749f2fb?ixlib=rb-1.2.1\\u0026ixid=eyJhcHBfaWQiOjE4NDU2N30\",\"full\":\"https://images.unsplash.com/photo-1603270650322-f7aa3749f2fb?ixlib=rb-1.2.1\\u0026q=85\\u0026fm=jpg\\u0026crop=entropy\\u0026cs=srgb\\u0026ixid=eyJhcHBfaWQiOjE4NDU2N30\",\"regular\":\"https://images.unsplash.com/photo-1603270650322-f7aa3749f2fb?ixlib=rb-1.2.1\\u0026q=80\\u0026fm=jpg\\u0026crop=entropy\\u0026cs=tinysrgb\\u0026w=1080\\u0026fit=max\\u0026ixid=eyJhcHBfaWQiOjE4NDU2N30\",\"small\":\"https://images.unsplash.com/photo-1603270650322-f7aa3749f2fb?ixlib=rb-1.2.1\\u0026q=80\\u0026fm=jpg\\u0026crop=entropy\\u0026cs=tinysrgb\\u0026w=400\\u0026fit=max\u0026ixid=eyJhcHBfaWQiOjE4NDU2N30\",\"thumb\":\"https://images.unsplash.com/photo-1603270650322-f7aa3749f2fb?ixlib=rb-1.2.1\\u0026q=80\\u0026fm=jpg\\u0026crop=entropy\\u0026cs=tinysrgb\\u0026w=200\\u0026fit=max\\u0026ixid=eyJhcHBfaWQiOjE4NDU2N30\"},\"links\":{\"self\":\"https://api.unsplash.com/photos/UDCSlDDcJVU\",\"html\":\"https://unsplash.com/photos/UDCSlDDcJVU\",\"download\":\"https://unsplash.com/photos/UDCSlDDcJVU/download\",\"download_location\":\"https://api.unsplash.com/photos/UDCSlDDcJVU/download\"},\"categories\":[],\"likes\":55,\"liked_by_user\":false,\"current_user_collections\":[],\"sponsorship\":null,\"user\":{\"id\":\"Ed2Drkid1Pg\",\"updated_at\":\"2020-11-20T02:05:13-05:00\",\"username\":\"liuxx\",\"name\":\"xx liu\",\"first_name\":\"xx\",\"last_name\":\"liu\",\"twitter_username\":null,\"portfolio_url\":null,\"bio\":null,\"location\":\"FuJian.China\",\"links\":{\"self\":\"https://api.unsplash.com/users/liuxx\",\"html\":\"https://unsplash.com/@liuxx\",\"photos\":\"https://api.unsplash.com/users/liuxx/photos\",\"likes\":\"https://api.unsplash.com/users/liuxx/likes\",\"portfolio\":\"https://api.unsplash.com/users/liuxx/portfolio\",\"following\":\"https://api.unsplash.com/users/liuxx/following\",\"followers\":\"https://api.unsplash.com/users/liuxx/followers\"},\"profile_image\":{\"small\":\"https://images.unsplash.com/profile-1583904875401-2f701fe62683image?ixlib=rb-1.2.1\\u0026q=80\\u0026fm=jpg\\u0026crop=faces\\u0026cs=tinysrgb\\u0026fit=crop\\u0026h=32\\u0026w=32\",\"medium\":\"https://images.unsplash.com/profile-1583904875401-2f701fe62683image?ixlib=rb-1.2.1\\u0026q=80\\u0026fm=jpg\\u0026crop=faces\\u0026cs=tinysrgb\\u0026fit=crop\\u0026h=64\\u0026w=64\",\"large\":\"https://images.unsplash.com/profile-1583904875401-2f701fe62683image?ixlib=rb-1.2.1\\u0026q=80\\u0026fm=jpg\\u0026crop=faces\\u0026cs=tinysrgb\\u0026fit=crop\\u0026h=128\\u0026w=128\"},\"instagram_username\":null,\"total_collections\":30,\"total_likes\":267,\"total_photos\":23,\"accepted_tos\":true},\"exif\":{\"make\":\"NIKON CORPORATION\",\"model\":\"NIKON Df\",\"exposure_time\":\"15\",\"aperture\":\"1.8\",\"focal_length\":\"20.0\",\"iso\":3200},\"location\":{\"title\":null,\"name\":null,\"city\":null,\"country\":null,\"position\":{\"latitude\":null,\"longitude\":null}},\"views\":356198,\"downloads\":2115}")
               })
               .Verifiable();

            var httpClient = new HttpClient(handlerMock.Object);
            mockFactory.Setup(method => method.CreateClient(It.IsAny<string>())).Returns(httpClient);

            var loggerMock = new Mock<ILogger<PictureServicesAsync>>();

            //Act
            var sut = new PictureServicesAsync(mockFactory.Object, loggerMock.Object);
            var result = await sut.GetRandomPicAsync();

            //Assert
            Assert.IsTrue(result == "https://images.unsplash.com/photo-1603270650322-f7aa3749f2fb?ixlib=rb-1.2.1&q=85&fm=jpg&crop=entropy&cs=srgb&ixid=eyJhcHBfaWQiOjE4NDU2N30");
        }
    }
}
