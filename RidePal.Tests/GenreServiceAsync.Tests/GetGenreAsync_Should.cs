﻿using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RidePal.Data.Context;
using RidePal.Services;
using RidePal.Services.DTOs;
using System.Threading.Tasks;

namespace RidePal.Tests.GenreServiceAsync.Tests
{
    [TestClass]
    public class GetGenreAsync_Should
    {
        [TestMethod]
        public async Task ReturnValidGenre_When_MethodCalled()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnValidGenre_When_MethodCalled));
            var genresToInsert = TestSetup.GetGenres();
            var loggerMock = new Mock<ILogger<GenreServicesAsync>>();

            using (var arrangeCtx = new RidePalContext(options))
            {
                await arrangeCtx.AddRangeAsync(genresToInsert);

                await arrangeCtx.SaveChangesAsync();
            }

            using (var actCtx = new RidePalContext(options))
            {
                var sut = new GenreServicesAsync(actCtx, loggerMock.Object);

                //Act
                var result = await sut.GetGenreAsync(1);
                
                //Assert
                Assert.IsInstanceOfType(result, typeof(SingleGenreDTO));
                Assert.AreEqual(genresToInsert[0].ID, result.ID);
                Assert.AreEqual(genresToInsert[0].Name, result.Name);
            }
        }
    }
}
