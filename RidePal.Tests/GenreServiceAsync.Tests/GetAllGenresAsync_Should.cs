﻿using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RidePal.Data.Context;
using RidePal.Services;
using RidePal.Services.DTOs;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RidePal.Tests.GenreServiceAsync.Tests
{
    [TestClass]
    public class GetAllGenresAsync_Should
    {
        [TestMethod]
        public async Task ReturnEmptyCollection_When_NoRecordsInDatabase()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnEmptyCollection_When_NoRecordsInDatabase));
            var loggerMock = new Mock<ILogger<GenreServicesAsync>>();

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new GenreServicesAsync(arrangeCtx, loggerMock.Object);

                //Act
                var result = await sut.GetAllGenresAsync();

                //Assert
                Assert.IsTrue(result.Count() == 0);
                Assert.IsInstanceOfType(result, typeof(IEnumerable<GenreDTO>));
            }
        }

        [TestMethod]
        public async Task ReturnValidGenreCollection()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnValidGenreCollection));
            var genresToInsert = TestSetup.GetGenres();
            var loggerMock = new Mock<ILogger<GenreServicesAsync>>();

            using (var arrangeCtx = new RidePalContext(options))
            {
                await arrangeCtx.AddRangeAsync(genresToInsert);

                await arrangeCtx.SaveChangesAsync();
            }

            using (var actCtx = new RidePalContext(options))
            {
                var sut = new GenreServicesAsync(actCtx, loggerMock.Object);

                //Act
                var result =await sut.GetAllGenresAsync();
                var resultList = result.ToList();

                //Assert
                Assert.IsTrue(resultList.Count() == 2);
                Assert.IsInstanceOfType(resultList, typeof(IEnumerable<GenreDTO>));
                Assert.AreEqual(genresToInsert[7].ID, resultList[0].ID);
                Assert.AreEqual(genresToInsert[7].Name, resultList[0].Name);
                Assert.AreEqual(genresToInsert[8].ID, resultList[1].ID);
                Assert.AreEqual(genresToInsert[8].Name, resultList[1].Name);
            }
        }

    }
}
