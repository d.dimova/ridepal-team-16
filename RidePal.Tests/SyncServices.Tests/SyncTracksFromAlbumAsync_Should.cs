﻿using MapsterMapper;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;
using RidePal.Data.Context;
using RidePal.Data.Models;
using RidePal.Services;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace RidePal.Tests.SyncServices.Tests
{
    [TestClass]
    public class SyncTracksFromAlbumAsync_Should
    {
        [TestMethod]
        public async Task InsertTracksFromAlbumsIntoDb()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(InsertTracksFromAlbumsIntoDb));

            var loggerMock = new Mock<ILogger<SyncServicesAsync>>();

            var mockFactory = new Mock<IHttpClientFactory>();
            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            handlerMock
               .Protected()
               .Setup<Task<HttpResponseMessage>>(
                  "SendAsync",
                  ItExpr.IsAny<HttpRequestMessage>(),
                  ItExpr.IsAny<CancellationToken>()
               )
               .ReturnsAsync(new HttpResponseMessage()
               {
                   StatusCode = HttpStatusCode.NoContent,
                   Content = new StringContent(
                     "{\"data\":[{\"id\":384027711,\"readable\":true,\"title\":\"Simon Boccanegra: Prelude (Remastered)\",\"title_short\":\"Simon Boccanegra: Prelude (Remastered)\",\"title_version\":\"\",\"isrc\":\"USSM11702973\",\"link\":\"https:\\/\\/www.deezer.com\\/track\\/384027711\",\"duration\":169,\"track_position\":1,\"disk_number\":1,\"rank\":69471,\"explicit_lyrics\":false,\"explicit_content_lyrics\":0,\"explicit_content_cover\":2,\"preview\":\"https:\\/\\/cdns-preview-1.dzcdn.net\\/stream\\/c-14d43e6ab68127113948bb7b691faa26-4.mp3\",\"md5_image\":\"9677c2b2940bedb55ffce10ae771afcf\",\"artist\":{\"id\":77655,\"name\":\"Claudio Abbado\",\"tracklist\":\"https:\\/\\/api.deezer.com\\/artist\\/77655\\/top?limit=50\",\"type\":\"artist\"},\"type\":\"track\"}],\"total\":8,\"next\":\"https:\\/\\/api.deezer.com\\/album\\/44706951\\/tracks?limit=1&index=1\"}"
                   )
               })
               .Verifiable();

            var httpClient = new HttpClient(handlerMock.Object);
            mockFactory.Setup(method => method.CreateClient(It.IsAny<string>())).Returns(httpClient);

            using (var arrangeCtx = new RidePalContext(options))
            {
                var genreToInsert = new Genre { ID = 98, Name = "Classical", PictureURL = "" };
                var artistToInsert = new Artist { ID = 13896, Name = "Luciano Pavarotti" };
                var albumToInsert = new Album { ID = 44706951, Title = "Pavarotti Sings Rare Verdi Arias (Remastered)", TrackListUrl = "https:/api.deezer.com/album/44706951/tracks", GenreID = 98, ArtistID = 13896 };

                await arrangeCtx.AddAsync(genreToInsert);
                await arrangeCtx.AddAsync(artistToInsert);
                await arrangeCtx.AddAsync(albumToInsert);

                await arrangeCtx.SaveChangesAsync();
            }

            using (var actCtx = new RidePalContext(options))
            {
                var sut = new SyncServicesAsync(actCtx, new Mapper(), mockFactory.Object, loggerMock.Object);

                await sut.SyncTracksFromAlbumsAsync(1, 1);

                //Act
                var result = actCtx.Tracks.ToList();

                //Assert
                Assert.IsTrue(result.Count() != 0);
                Assert.IsTrue(result[0].ID == 384027711);
                Assert.IsTrue(result[0].Title == "Simon Boccanegra: Prelude (Remastered)");
                Assert.IsTrue(result[0].Duration == 169);
            }
        }

        [TestMethod]
        public async Task UpdateAndSyncTrack_IfAlreadyInDb()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(UpdateAndSyncTrack_IfAlreadyInDb));

            var loggerMock = new Mock<ILogger<SyncServicesAsync>>();

            var mockFactory = new Mock<IHttpClientFactory>();
            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            handlerMock
               .Protected()
               .Setup<Task<HttpResponseMessage>>(
                  "SendAsync",
                  ItExpr.IsAny<HttpRequestMessage>(),
                  ItExpr.IsAny<CancellationToken>()
               )
               .ReturnsAsync(new HttpResponseMessage()
               {
                   StatusCode = HttpStatusCode.NoContent,
                   Content = new StringContent(
                     "{\"data\":[{\"id\":384027711,\"readable\":true,\"title\":\"Simon Boccanegra: Prelude (Remastered)\",\"title_short\":\"Simon Boccanegra: Prelude (Remastered)\",\"title_version\":\"\",\"isrc\":\"USSM11702973\",\"link\":\"https:\\/\\/www.deezer.com\\/track\\/384027711\",\"duration\":169,\"track_position\":1,\"disk_number\":1,\"rank\":69471,\"explicit_lyrics\":false,\"explicit_content_lyrics\":0,\"explicit_content_cover\":2,\"preview\":\"https:\\/\\/cdns-preview-1.dzcdn.net\\/stream\\/c-14d43e6ab68127113948bb7b691faa26-4.mp3\",\"md5_image\":\"9677c2b2940bedb55ffce10ae771afcf\",\"artist\":{\"id\":77655,\"name\":\"Claudio Abbado\",\"tracklist\":\"https:\\/\\/api.deezer.com\\/artist\\/77655\\/top?limit=50\",\"type\":\"artist\"},\"type\":\"track\"}],\"total\":8,\"next\":\"https:\\/\\/api.deezer.com\\/album\\/44706951\\/tracks?limit=1&index=1\"}"
                   )
               })
               .Verifiable();

            var httpClient = new HttpClient(handlerMock.Object);
            mockFactory.Setup(method => method.CreateClient(It.IsAny<string>())).Returns(httpClient);

            using (var arrangeCtx = new RidePalContext(options))
            {
                var genreToInsert = new Genre { ID = 98, Name = "Classical", PictureURL = "" };
                var artistToInsert = new Artist { ID = 13896, Name = "Luciano Pavarotti" };
                var albumToInsert = new Album { ID = 44706951, Title = "Pavarotti Sings Rare Verdi Arias (Remastered)", TrackListUrl = "https:/api.deezer.com/album/44706951/tracks", GenreID = 98, ArtistID = 13896 };
                var trackToUpdateInsert = new Track { ID = 384027711, Title = "Should Sync", Duration = 0, Rank = 0, ArtistID = 13896, AlbumID = 44706951, GenreID = 98 };

                await arrangeCtx.AddAsync(genreToInsert);
                await arrangeCtx.AddAsync(artistToInsert);
                await arrangeCtx.AddAsync(albumToInsert);
                await arrangeCtx.AddAsync(trackToUpdateInsert);

                await arrangeCtx.SaveChangesAsync();
            }

            using (var actCtx = new RidePalContext(options))
            {
                var sut = new SyncServicesAsync(actCtx, new Mapper(), mockFactory.Object, loggerMock.Object);

                await sut.SyncTracksFromAlbumsAsync(1, 1);

                //Act
                var result = actCtx.Tracks.ToList();

                //Assert
                Assert.IsTrue(result[0].Title != "Should Sync");
                Assert.IsTrue(result[0].Rank != 0);
            }
        }
    }
}
