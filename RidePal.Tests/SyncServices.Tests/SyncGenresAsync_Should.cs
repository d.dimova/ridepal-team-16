﻿using MapsterMapper;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;
using RidePal.Data.Context;
using RidePal.Data.Models;
using RidePal.Services;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace RidePal.Tests.SyncServices.Tests
{
    [TestClass]
    public class SyncGenresAsync_Should
    {
        [TestMethod]
        public async Task ShouldAddGenreToDb_IFNotAlreadyIN()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ShouldAddGenreToDb_IFNotAlreadyIN));

            var loggerMock = new Mock<ILogger<SyncServicesAsync>>();

            var mockFactory = new Mock<IHttpClientFactory>();
            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            handlerMock
               .Protected()
               .Setup<Task<HttpResponseMessage>>(
                  "SendAsync",
                  ItExpr.IsAny<HttpRequestMessage>(),
                  ItExpr.IsAny<CancellationToken>()
               )
               .ReturnsAsync(new HttpResponseMessage()
               {
                   StatusCode = HttpStatusCode.NoContent,
                   Content = new StringContent("{\"data\":[{\"id\":0,\"name\":\"All\",\"picture\":\"https:\\/\\/api.deezer.com\\/genre\\/0\\/image\",\"picture_small\":\"https:\\/\\/e-cdns-images.dzcdn.net\\/images\\/misc\\/\\/56x56-000000-80-0-0.jpg\",\"picture_medium\":\"https:\\/\\/e-cdns-images.dzcdn.net\\/images\\/misc\\/\\/250x250-000000-80-0-0.jpg\",\"picture_big\":\"https:\\/\\/e-cdns-images.dzcdn.net\\/images\\/misc\\/\\/500x500-000000-80-0-0.jpg\",\"picture_xl\":\"https:\\/\\/e-cdns-images.dzcdn.net\\/images\\/misc\\/\\/1000x1000-000000-80-0-0.jpg\",\"type\":\"genre\"}]}")
               })
               .Verifiable();

            var httpClient = new HttpClient(handlerMock.Object);
            mockFactory.Setup(method => method.CreateClient(It.IsAny<string>())).Returns(httpClient);

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new SyncServicesAsync(arrangeCtx, new Mapper(), mockFactory.Object, loggerMock.Object);

                await sut.SyncGenresAsync();

                //Act
                var result = arrangeCtx.Genres.ToList();

                //Assert
                Assert.IsTrue(result.Count() != 0);
                Assert.IsTrue(result[0].Name == "All");
            };
        }

        [TestMethod]
        public async Task ShouldUpdateGenre_IfInDb()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ShouldUpdateGenre_IfInDb));

            var loggerMock = new Mock<ILogger<SyncServicesAsync>>();

            var mockFactory = new Mock<IHttpClientFactory>();
            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            handlerMock
               .Protected()
               .Setup<Task<HttpResponseMessage>>(
                  "SendAsync",
                  ItExpr.IsAny<HttpRequestMessage>(),
                  ItExpr.IsAny<CancellationToken>()
               )
               .ReturnsAsync(new HttpResponseMessage()
               {
                   StatusCode = HttpStatusCode.NoContent,
                   Content = new StringContent("{\"data\":[{\"id\":0,\"name\":\"All\",\"picture\":\"https:\\/\\/api.deezer.com\\/genre\\/0\\/image\",\"picture_small\":\"https:\\/\\/e-cdns-images.dzcdn.net\\/images\\/misc\\/\\/56x56-000000-80-0-0.jpg\",\"picture_medium\":\"https:\\/\\/e-cdns-images.dzcdn.net\\/images\\/misc\\/\\/250x250-000000-80-0-0.jpg\",\"picture_big\":\"https:\\/\\/e-cdns-images.dzcdn.net\\/images\\/misc\\/\\/500x500-000000-80-0-0.jpg\",\"picture_xl\":\"https:\\/\\/e-cdns-images.dzcdn.net\\/images\\/misc\\/\\/1000x1000-000000-80-0-0.jpg\",\"type\":\"genre\"}]}")
               })
               .Verifiable();

            var httpClient = new HttpClient(handlerMock.Object);
            mockFactory.Setup(method => method.CreateClient(It.IsAny<string>())).Returns(httpClient);

            using (var arrangeCtx = new RidePalContext(options))
            {
                var genreToBeUpdated = new Genre { ID = 0, Name = "test", PictureURL = "" };

                await arrangeCtx.AddAsync(genreToBeUpdated);

                await arrangeCtx.SaveChangesAsync();
            }

            using (var actCtx = new RidePalContext(options))
            {
                var sut = new SyncServicesAsync(actCtx, new Mapper(), mockFactory.Object, loggerMock.Object);

                await sut.SyncGenresAsync();

                //Act
                var result = actCtx.Genres.ToList();

                //Assert
                Assert.IsTrue(result[0].Name == "All");
            };
        }
    }


}
