﻿using MapsterMapper;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;
using RidePal.Data.Context;
using RidePal.Data.Models;
using RidePal.Services;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
namespace RidePal.Tests.SyncServices.Tests
{
    [TestClass]
    public class SyncTopArtistAsync_Should
    {

        [TestMethod]
        public async Task InsertArtistInDb()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(InsertArtistInDb));

            var loggerMock = new Mock<ILogger<SyncServicesAsync>>();

            var mockFactory = new Mock<IHttpClientFactory>();
            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            handlerMock
               .Protected()
               .Setup<Task<HttpResponseMessage>>(
                  "SendAsync",
                  ItExpr.IsAny<HttpRequestMessage>(),
                  ItExpr.IsAny<CancellationToken>()
               )
               .ReturnsAsync(new HttpResponseMessage()
               {
                   StatusCode = HttpStatusCode.NoContent,
                   Content = new StringContent("{\"data\":[{\"id\":13896,\"name\":\"Luciano Pavarotti\",\"picture\":\"https:\\/\\/api.deezer.com\\/artist\\/13896\\/image\",\"picture_small\":\"https:\\/\\/cdns-images.dzcdn.net\\/images\\/artist\\/65592b30d258c43a770ef811c62fccb9\\/56x56-000000-80-0-0.jpg\",\"picture_medium\":\"https:\\/\\/cdns-images.dzcdn.net\\/images\\/artist\\/65592b30d258c43a770ef811c62fccb9\\/250x250-000000-80-0-0.jpg\",\"picture_big\":\"https:\\/\\/cdns-images.dzcdn.net\\/images\\/artist\\/65592b30d258c43a770ef811c62fccb9\\/500x500-000000-80-0-0.jpg\",\"picture_xl\":\"https:\\/\\/cdns-images.dzcdn.net\\/images\\/artist\\/65592b30d258c43a770ef811c62fccb9\\/1000x1000-000000-80-0-0.jpg\",\"radio\":true,\"tracklist\":\"https:\\/\\/api.deezer.com\\/artist\\/13896\\/top?limit=50\",\"type\":\"artist\"}]}")
               })
               .Verifiable();

            var httpClient = new HttpClient(handlerMock.Object);
            mockFactory.Setup(method => method.CreateClient(It.IsAny<string>())).Returns(httpClient);

            using (var arrangeCtx = new RidePalContext(options))
            {
                var genreToInsert = new Genre { ID = 98, Name = "Classical", PictureURL = "" };

                await arrangeCtx.AddAsync(genreToInsert);

                await arrangeCtx.SaveChangesAsync();
            }

            using (var actCtx = new RidePalContext(options))
            {
                var sut = new SyncServicesAsync(actCtx, new Mapper(), mockFactory.Object, loggerMock.Object);

                await sut.SyncTopAristsAsync(0);

                //Act
                var result = actCtx.Artists.ToList();

                //Assert
                Assert.IsTrue(result.Count() != 0);
                Assert.IsTrue(result[0].Name == "Luciano Pavarotti");
                Assert.IsTrue(result[0].ID == 13896);
            };
        }

        [TestMethod]
        public async Task UpdateArtist_IfFoundInDd()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(UpdateArtist_IfFoundInDd));

            var loggerMock = new Mock<ILogger<SyncServicesAsync>>();

            var mockFactory = new Mock<IHttpClientFactory>();
            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            handlerMock
               .Protected()
               .Setup<Task<HttpResponseMessage>>(
                  "SendAsync",
                  ItExpr.IsAny<HttpRequestMessage>(),
                  ItExpr.IsAny<CancellationToken>()
               )
               .ReturnsAsync(new HttpResponseMessage()
               {
                   StatusCode = HttpStatusCode.NoContent,
                   Content = new StringContent("{\"data\":[{\"id\":13896,\"name\":\"Luciano Pavarotti\",\"picture\":\"https:\\/\\/api.deezer.com\\/artist\\/13896\\/image\",\"picture_small\":\"https:\\/\\/cdns-images.dzcdn.net\\/images\\/artist\\/65592b30d258c43a770ef811c62fccb9\\/56x56-000000-80-0-0.jpg\",\"picture_medium\":\"https:\\/\\/cdns-images.dzcdn.net\\/images\\/artist\\/65592b30d258c43a770ef811c62fccb9\\/250x250-000000-80-0-0.jpg\",\"picture_big\":\"https:\\/\\/cdns-images.dzcdn.net\\/images\\/artist\\/65592b30d258c43a770ef811c62fccb9\\/500x500-000000-80-0-0.jpg\",\"picture_xl\":\"https:\\/\\/cdns-images.dzcdn.net\\/images\\/artist\\/65592b30d258c43a770ef811c62fccb9\\/1000x1000-000000-80-0-0.jpg\",\"radio\":true,\"tracklist\":\"https:\\/\\/api.deezer.com\\/artist\\/13896\\/top?limit=50\",\"type\":\"artist\"}]}")
               })
               .Verifiable();

            var httpClient = new HttpClient(handlerMock.Object);
            mockFactory.Setup(method => method.CreateClient(It.IsAny<string>())).Returns(httpClient);

            using (var arrangeCtx = new RidePalContext(options))
            {
                var genreToInsert = new Genre { ID = 98, Name = "Classical", PictureURL = "" };
                var artistToUpdate = new Artist { ID = 13896, Name = "Eminem", PictureURL = "testUrl" };

                await arrangeCtx.AddAsync(genreToInsert);
                await arrangeCtx.AddAsync(artistToUpdate);

                await arrangeCtx.SaveChangesAsync();
            }

            using (var actCtx = new RidePalContext(options))
            {
                var sut = new SyncServicesAsync(actCtx, new Mapper(), mockFactory.Object, loggerMock.Object);

                await sut.SyncTopAristsAsync(0);

                //Act
                var result = actCtx.Artists.ToList();

                //Assert
                Assert.IsTrue(result[0].Name != "Eminem");
                Assert.IsTrue(result[0].PictureURL != "testUrl");
            };
        }
    }
}


