﻿using MapsterMapper;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;
using RidePal.Data.Context;
using RidePal.Data.Models;
using RidePal.Services;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace RidePal.Tests.SyncServices.Tests
{
    [TestClass]
    public class SyncAlbumsFromArtistsAsunc_Should
    {
        [TestMethod]
        public async Task InsertAlbumInDb()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(InsertAlbumInDb));

            var loggerMock = new Mock<ILogger<SyncServicesAsync>>();

            var mockFactory = new Mock<IHttpClientFactory>();
            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            handlerMock
               .Protected()
               .Setup<Task<HttpResponseMessage>>(
                  "SendAsync",
                  ItExpr.IsAny<HttpRequestMessage>(),
                  ItExpr.IsAny<CancellationToken>()
               )
               .ReturnsAsync(new HttpResponseMessage()
               {
                   StatusCode = HttpStatusCode.NoContent,
                   Content = new StringContent(
                      "{ \"data\":[{\"id\":44706951,\"title\":\"Pavarotti Sings Rare Verdi Arias (Remastered)\",\"link\":\"https:\\/\\/www.deezer.com\\/album\\/44706951\",\"cover\":\"https:\\/\\/api.deezer.com\\/album\\/44706951\\/image\",\"cover_small\":\"https:\\/\\/e-cdns-images.dzcdn.net\\/images\\/cover\\/9677c2b2940bedb55ffce10ae771afcf\\/56x56-000000-80-0-0.jpg\",\"cover_medium\":\"https:\\/\\/e-cdns-images.dzcdn.net\\/image\\/cover\\/9677c2b2940bedb55ffce10ae771afcf\\/250x250-000000-80-0-0.jpg\",\"cover_big\":\"https:\\/\\/e-cdns-images.dzcdn.net\\/images\\/cover\\/9677c2b2940bedb55ffce10ae771afcf\\/500x500-000000-80-0-0.jpg\",\"cover_xl\":\"https:\\/\\/e-cdns-images.dzcdn.net\\/images\\/cover\\/9677c2b2940bedb55ffce10ae771afcf\\/1000x1000-000000-80-0-0.jpg\",\"md5_image\":\"9677c2b2940bedb55ffce10ae771afcf\",\"genre_id\":98,\"fans\":493,\"release_date\":\"2017-08-04\",\"record_type\":\"album\",\"tracklist\":\"https:\\/\\/api.deezer.com\\/album\\/44706951\\/tracks\",\"explicit_lyrics\":false,\"type\":\"album\"}],\"total\":112,\"next\":\"https:\\/\\/api.deezer.com\\/artist\\/13896\\/albums?limit=1&index=1\"}"

                   )
               })
               .Verifiable();

            var httpClient = new HttpClient(handlerMock.Object);
            mockFactory.Setup(method => method.CreateClient(It.IsAny<string>())).Returns(httpClient);

            using (var arrangeCtx = new RidePalContext(options))
            {
                var genreToInsert = new Genre { ID = 98, Name = "Classical", PictureURL = "" };
                var artistToInsert = new Artist { ID = 13896, Name = "Luciano Pavarotti" };

                await arrangeCtx.AddAsync(genreToInsert);
                await arrangeCtx.AddAsync(artistToInsert);

                await arrangeCtx.SaveChangesAsync();
            }
            using (var actCtx = new RidePalContext(options))
            {
                var sut = new SyncServicesAsync(actCtx, new Mapper(), mockFactory.Object, loggerMock.Object);

                await sut.SyncAlbumsFromArtistsAsync(1);

                //Act
                var result = actCtx.Albums.ToList();

                //Assert
                Assert.IsTrue(result.Count() != 0);
                Assert.IsTrue(result[0].Title == "Pavarotti Sings Rare Verdi Arias (Remastered)");
                Assert.IsTrue(result[0].ID == 44706951);
            }

        }

        [TestMethod]
        public async Task UpdateAlbumInDb_IfAlbumExists()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(UpdateAlbumInDb_IfAlbumExists));

            var loggerMock = new Mock<ILogger<SyncServicesAsync>>();

            var mockFactory = new Mock<IHttpClientFactory>();
            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            handlerMock
               .Protected()
               .Setup<Task<HttpResponseMessage>>(
                  "SendAsync",
                  ItExpr.IsAny<HttpRequestMessage>(),
                  ItExpr.IsAny<CancellationToken>()
               )
               .ReturnsAsync(new HttpResponseMessage()
               {
                   StatusCode = HttpStatusCode.NoContent,
                   Content = new StringContent(
                      "{ \"data\":[{\"id\":44706951,\"title\":\"Pavarotti Sings Rare Verdi Arias (Remastered)\",\"link\":\"https:\\/\\/www.deezer.com\\/album\\/44706951\",\"cover\":\"https:\\/\\/api.deezer.com\\/album\\/44706951\\/image\",\"cover_small\":\"https:\\/\\/e-cdns-images.dzcdn.net\\/images\\/cover\\/9677c2b2940bedb55ffce10ae771afcf\\/56x56-000000-80-0-0.jpg\",\"cover_medium\":\"https:\\/\\/e-cdns-images.dzcdn.net\\/image\\/cover\\/9677c2b2940bedb55ffce10ae771afcf\\/250x250-000000-80-0-0.jpg\",\"cover_big\":\"https:\\/\\/e-cdns-images.dzcdn.net\\/images\\/cover\\/9677c2b2940bedb55ffce10ae771afcf\\/500x500-000000-80-0-0.jpg\",\"cover_xl\":\"https:\\/\\/e-cdns-images.dzcdn.net\\/images\\/cover\\/9677c2b2940bedb55ffce10ae771afcf\\/1000x1000-000000-80-0-0.jpg\",\"md5_image\":\"9677c2b2940bedb55ffce10ae771afcf\",\"genre_id\":98,\"fans\":493,\"release_date\":\"2017-08-04\",\"record_type\":\"album\",\"tracklist\":\"https:\\/\\/api.deezer.com\\/album\\/44706951\\/tracks\",\"explicit_lyrics\":false,\"type\":\"album\"}],\"total\":112,\"next\":\"https:\\/\\/api.deezer.com\\/artist\\/13896\\/albums?limit=1&index=1\"}"

                   )
               })
               .Verifiable();

            var httpClient = new HttpClient(handlerMock.Object);
            mockFactory.Setup(method => method.CreateClient(It.IsAny<string>())).Returns(httpClient);

            using (var arrangeCtx = new RidePalContext(options))
            {
                var genreToInsert = new Genre { ID = 98, Name = "Classical", PictureURL = "" };
                var artistToInsert = new Artist { ID = 13896, Name = "Luciano Pavarotti" };
                var albumToInserForUpdate = new Album { ID = 44706951, Title = "Album TO Update, this title should be update by the method", TrackListUrl = "Should Update from call" };

                await arrangeCtx.AddAsync(genreToInsert);
                await arrangeCtx.AddAsync(artistToInsert);
                await arrangeCtx.AddAsync(albumToInserForUpdate);

                await arrangeCtx.SaveChangesAsync();
            }
            using (var actCtx = new RidePalContext(options))
            {
                var sut = new SyncServicesAsync(actCtx, new Mapper(), mockFactory.Object, loggerMock.Object);

                await sut.SyncAlbumsFromArtistsAsync(1);

                //Act
                var result = actCtx.Albums.ToList();

                //Assert
                Assert.IsTrue(result[0].Title != "Album TO Update, this title should be update by the method");
                Assert.IsTrue(result[0].TrackListUrl != "Should Update from call");
            }
        }
    }
}
