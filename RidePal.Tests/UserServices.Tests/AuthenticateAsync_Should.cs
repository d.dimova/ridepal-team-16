﻿using FakeItEasy;
using MapsterMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RidePal.Data.Context;
using RidePal.Data.Models;
using RidePal.Services;
using RidePal.Services.DTOs;
using RidePal.Services.Helpers;
using System.Threading.Tasks;

namespace RidePal.Tests.UserServices.Tests
{
    [TestClass]
    public class AuthenticateAsync_Should
    {
        [TestMethod]
        public async Task ReturnNull_WhenLoginInfoIsEmpty()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnNull_WhenLoginInfoIsEmpty));

            var userManagerFake = A.Fake<UserManager<User>>();
            var roleManagerFake = A.Fake<RoleManager<IdentityRole<int>>>();
            var signIngManagerFake = A.Fake<SignInManager<User>>();
            var appSettingsFake = A.Fake<IOptions<AppSettings>>();
            var loggerFake = A.Fake<ILogger<UserServicesAsync>>();

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new UserServicesAsync(arrangeCtx
                , new Mapper()
                , userManagerFake
                , appSettingsFake
                , signIngManagerFake
                , roleManagerFake
                , loggerFake);

                //Act
                var result = await sut.AuthenticateAsync(new LoginDTO());

                //Assert
                Assert.IsNull(result);
            };
        }

        [TestMethod]
        public async Task ReturnNull_WhenLoginAttemptsIsUnsucseful()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnNull_WhenLoginAttemptsIsUnsucseful));
            var users = TestSetup.GetUsers();
            var loginInfo = new LoginDTO { Username = "georgi.g", Password = "123123" };

            var userManagerFake = A.Fake<UserManager<User>>();
            var roleManagerFake = A.Fake<RoleManager<IdentityRole<int>>>();
            var signIngManagerFake = A.Fake<SignInManager<User>>();
            A.CallTo(() => signIngManagerFake.CheckPasswordSignInAsync(new User(), "pass", false)).Returns(SignInResult.Failed);

            var appSettingsFake = A.Fake<IOptions<AppSettings>>();
            var loggerFake = A.Fake<ILogger<UserServicesAsync>>();

            using (var arrangeCtx = new RidePalContext(options))
            {
                await arrangeCtx.Users.AddRangeAsync(users);

                await arrangeCtx.SaveChangesAsync();
            }

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new UserServicesAsync(arrangeCtx
                    , new Mapper()
                    , userManagerFake
                    , appSettingsFake
                    , signIngManagerFake
                    , roleManagerFake
                    , loggerFake);

                //Act
                var result = await sut.AuthenticateAsync(loginInfo);

                //Assert
                Assert.IsNull(result);
            }
        }
    }
}
