﻿using FakeItEasy;
using MapsterMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RidePal.Data.Context;
using RidePal.Data.Models;
using RidePal.Services;
using RidePal.Services.Helpers;
using System.Threading.Tasks;

namespace RidePal.Tests.UserServices.Tests
{
    [TestClass]
    public class CreateRolesAsync_Should
    {

        [TestMethod]
        public async Task CheckIfMethodWasCalledCorrectly()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(CheckIfMethodWasCalledCorrectly));
            var users = TestSetup.GetUsers();
            var user = new User();
            user = null;

            var userManagerFake = A.Fake<UserManager<User>>();
            A.CallTo(() => userManagerFake.FindByEmailAsync(A<string>.Ignored)).Returns(user);
            A.CallTo(() => userManagerFake.CreateAsync(A<User>.Ignored, A<string>.Ignored)).Returns(IdentityResult.Success);

            var roleManagerFake = A.Fake<RoleManager<IdentityRole<int>>>();
            A.CallTo(() => roleManagerFake.RoleExistsAsync(A<string>.Ignored)).Returns(Task.FromResult(false));
            A.CallTo(() => roleManagerFake.CreateAsync(A<IdentityRole<int>>.Ignored)).Returns(Task.FromResult(IdentityResult.Success));

            var signIngManagerFake = A.Fake<SignInManager<User>>();
            var appSettingsFake = A.Fake<IOptions<AppSettings>>();
            var loggerFake = A.Fake<ILogger<UserServicesAsync>>();

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new UserServicesAsync(arrangeCtx
                    , new Mapper()
                    , userManagerFake
                    , appSettingsFake
                    , signIngManagerFake
                    , roleManagerFake
                    , loggerFake);

                //Act
                await sut.CreateRolesAsync();

                //Assert
                A.CallTo(() => roleManagerFake.RoleExistsAsync(A<string>.Ignored)).MustHaveHappened();
                A.CallTo(() => roleManagerFake.CreateAsync(A<IdentityRole<int>>.Ignored)).MustHaveHappenedOnceOrMore();
                A.CallTo(() => userManagerFake.FindByEmailAsync(A<string>.Ignored)).MustHaveHappened();
                A.CallTo(() => userManagerFake.CreateAsync(A<User>.Ignored, A<string>.Ignored)).MustHaveHappened();
            };
        }
    }
}
