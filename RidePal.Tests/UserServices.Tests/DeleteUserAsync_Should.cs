﻿using FakeItEasy;
using MapsterMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RidePal.Data.Context;
using RidePal.Data.Models;
using RidePal.Services;
using RidePal.Services.Helpers;
using System.Threading.Tasks;

namespace RidePal.Tests.UserServices.Tests
{
    [TestClass]
    public class DeleteUserAsync_Should
    {
        [TestMethod]
        public async Task ReturnFalse_WhenUserNotFound()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnFalse_WhenUserNotFound));

            var userManagerFake = A.Fake<UserManager<User>>();
            var roleManagerFake = A.Fake<RoleManager<IdentityRole<int>>>();
            var signIngManagerFake = A.Fake<SignInManager<User>>();
            var appSettingsFake = A.Fake<IOptions<AppSettings>>();
            var loggerFake = A.Fake<ILogger<UserServicesAsync>>();

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new UserServicesAsync(arrangeCtx
                    , new Mapper()
                    , userManagerFake
                    , appSettingsFake
                    , signIngManagerFake
                    , roleManagerFake
                    , loggerFake);

                //Act
                var result = await sut.DeleteUserAsync(1);

                //Assert
                Assert.IsFalse(result);
            }
        }

        [TestMethod]
        public async Task ReturnTrue_WhenUserIsDeleted()
        { 
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnTrue_WhenUserIsDeleted));
            var users = TestSetup.GetUsers();

            var userManagerFake = A.Fake<UserManager<User>>();
            var roleManagerFake = A.Fake<RoleManager<IdentityRole<int>>>();
            var signIngManagerFake = A.Fake<SignInManager<User>>();
            var appSettingsFake = A.Fake<IOptions<AppSettings>>();
            var loggerFake = A.Fake<ILogger<UserServicesAsync>>();

            using (var arrangeCtx = new RidePalContext(options))
            {
                await arrangeCtx.Users.AddRangeAsync(users);

                await arrangeCtx.SaveChangesAsync();
            }

            using (var actCtx = new RidePalContext(options))
            {
                var sut = new UserServicesAsync(actCtx
                    , new Mapper()
                    , userManagerFake
                    , appSettingsFake
                    , signIngManagerFake
                    , roleManagerFake
                    , loggerFake);

                //Act
                var result = await sut.DeleteUserAsync(1);
                var deletedUser =await actCtx.Users.FindAsync(1);

                //Assert
                Assert.IsTrue(result);
                Assert.IsTrue(deletedUser.IsDeleted = true);
            }
        }
    }
}
