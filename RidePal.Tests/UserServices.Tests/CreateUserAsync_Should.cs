﻿using FakeItEasy;
using MapsterMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RidePal.Data.Context;
using RidePal.Data.Models;
using RidePal.Services;
using RidePal.Services.DTOs;
using RidePal.Services.Helpers;
using System.Threading.Tasks;

namespace RidePal.Tests.UserServices.Tests
{
    [TestClass]
    public class CreateUserAsync_Should
    {
        [TestMethod]
        public async Task ReturnFalse_WhenWrongOrNullRegisterInfoIsPassed()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnFalse_WhenWrongOrNullRegisterInfoIsPassed));

            var userManagerFake = A.Fake<UserManager<User>>();
            var roleManagerFake = A.Fake<RoleManager<IdentityRole<int>>>();
            var signIngManagerFake = A.Fake<SignInManager<User>>();
            var appSettingsFake = A.Fake<IOptions<AppSettings>>();
            var loggerFake = A.Fake<ILogger<UserServicesAsync>>();

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new UserServicesAsync(arrangeCtx
                    , new Mapper()
                    , userManagerFake
                    , appSettingsFake
                    , signIngManagerFake
                    , roleManagerFake
                    , loggerFake);

                //Act 
                var result = await sut.CreateUserAsync(new RegisterModelDTO());

                //Assert
                Assert.IsFalse(result);
            }
        }

        [TestMethod]
        public async Task ReturnTrue_WhenCorrectParamsArePassed()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnTrue_WhenCorrectParamsArePassed));

            var userManagerFake = A.Fake<UserManager<User>>();
            A.CallTo(() => userManagerFake.CreateAsync(A<User>.Ignored, A<string>.Ignored)).Returns(IdentityResult.Success);

            var roleManagerFake = A.Fake<RoleManager<IdentityRole<int>>>();
            var signIngManagerFake = A.Fake<SignInManager<User>>();
            var appSettingsFake = A.Fake<IOptions<AppSettings>>();
            var loggerFake = A.Fake<ILogger<UserServicesAsync>>();

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new UserServicesAsync(arrangeCtx
                    , new Mapper()
                    , userManagerFake
                    , appSettingsFake
                    , signIngManagerFake
                    , roleManagerFake
                    , loggerFake);

                //Act 
                var result = await sut.CreateUserAsync(new RegisterModelDTO());

                //Assert
                Assert.IsTrue(result);
            }
        }
    }
}
