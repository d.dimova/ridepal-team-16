﻿using FakeItEasy;
using Mapster;
using MapsterMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RidePal.Data.Context;
using RidePal.Data.Models;
using RidePal.Services;
using RidePal.Services.DTOs;
using RidePal.Services.Helpers;
using System.Threading.Tasks;

namespace RidePal.Tests.UserServices.Tests
{
    [TestClass]
    public class GetUserAsync_Should
    {
        [TestMethod]
        public async Task ReturnNull_WhenUserNotFound()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnNull_WhenUserNotFound));

            var userManagerFake = A.Fake<UserManager<User>>();
            var roleManagerFake = A.Fake<RoleManager<IdentityRole<int>>>();
            var signIngManagerFake = A.Fake<SignInManager<User>>();
            var appSettingsFake = A.Fake<IOptions<AppSettings>>();
            var loggerFake = A.Fake<ILogger<UserServicesAsync>>();

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new UserServicesAsync(arrangeCtx
                    , new Mapper()
                    , userManagerFake
                    , appSettingsFake
                    , signIngManagerFake
                    , roleManagerFake
                    , loggerFake);

                //Act
                var result = await sut.GetUserAsync(1);

                //Assert
                Assert.IsNull(result);
            }
        }

        [TestMethod]
        public async Task ReturnCorectUser_WhencValidIdIsPassed()
        {
            var options = TestSetup.GetOptions(nameof(ReturnCorectUser_WhencValidIdIsPassed));
            var users = TestSetup.GetUsers();

            var userManagerFake = A.Fake<UserManager<User>>();
            var roleManagerFake = A.Fake<RoleManager<IdentityRole<int>>>();
            var signIngManagerFake = A.Fake<SignInManager<User>>();
            var appSettingsFake = A.Fake<IOptions<AppSettings>>();
            var loggerFake = A.Fake<ILogger<UserServicesAsync>>();

            using (var arrangeCtx = new RidePalContext(options))
            {
                await arrangeCtx.Users.AddRangeAsync(users);

                await arrangeCtx.SaveChangesAsync();
            }

            using (var actCtx = new RidePalContext(options))
            {
                var sut = new UserServicesAsync(actCtx
                    , new Mapper()
                    , userManagerFake
                    , appSettingsFake
                    , signIngManagerFake
                    , roleManagerFake
                    , loggerFake);

                //Act
                var expected = users[0].Adapt<UserDTO>();
                var result = await sut.GetUserAsync(1);

                //Assert
                Assert.AreEqual(expected.Id, result.Id);
                Assert.AreEqual(expected.Name, result.Name);
                Assert.AreEqual(expected.UserName, result.UserName);
            }
        }
    }
}
