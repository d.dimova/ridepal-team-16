﻿using FakeItEasy;
using Mapster;
using MapsterMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RidePal.Data.Context;
using RidePal.Data.Models;
using RidePal.Services;
using RidePal.Services.DTOs;
using RidePal.Services.Helpers;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RidePal.Tests.UserServices.Tests
{
    [TestClass]
    public class GetAllAsync_Should
    {
        [TestMethod]
        public async Task ReturnEmptyCollection_When_NoRecordsInDatabase()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnEmptyCollection_When_NoRecordsInDatabase));

            var userManagerFake = A.Fake<UserManager<User>>();
            var roleManagerFake = A.Fake<RoleManager<IdentityRole<int>>>();
            var signIngManagerFake = A.Fake<SignInManager<User>>();
            var appSettingsFake = A.Fake<IOptions<AppSettings>>();
            var loggerFake = A.Fake<ILogger<UserServicesAsync>>();

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new UserServicesAsync(arrangeCtx
                    , new Mapper()
                    , userManagerFake
                    , appSettingsFake
                    , signIngManagerFake
                    , roleManagerFake
                    , loggerFake);

                //Act
                var result = await sut.GetAllAsync();

                //Assert
                Assert.IsInstanceOfType(result, typeof(IEnumerable<UserDTO>));
                Assert.IsTrue(result.ToList().Count() == 0);
            }
        }

        [TestMethod]
        public async Task ReturnValidUsersCollection_When_PlaylistsExist()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnValidUsersCollection_When_PlaylistsExist));
            var users = TestSetup.GetUsers();

            var userManagerFake = A.Fake<UserManager<User>>();
            var roleManagerFake = A.Fake<RoleManager<IdentityRole<int>>>();
            var signIngManagerFake = A.Fake<SignInManager<User>>();
            var appSettingsFake = A.Fake<IOptions<AppSettings>>();
            var loggerFake = A.Fake<ILogger<UserServicesAsync>>();

            using (var arrangeCtx = new RidePalContext(options))
            {
                await arrangeCtx.Users.AddRangeAsync(users);

                await arrangeCtx.SaveChangesAsync();
            }

            using (var actCtx = new RidePalContext(options))
            {
                var sut = new UserServicesAsync(actCtx
                    , new Mapper()
                    , userManagerFake
                    , appSettingsFake
                    , signIngManagerFake
                    , roleManagerFake
                    , loggerFake);

                //Act
                var result = await sut.GetAllAsync();
                var expected = users.Adapt<IList<UserDTO>>();

                //Assert
                Assert.IsInstanceOfType(result, typeof(IEnumerable<UserDTO>));
                Assert.AreEqual(expected[0].Id, result.Select(x => x.Id).ToArray()[0]);
                Assert.AreEqual(expected[0].Name, result.Select(x => x.Name).ToArray()[0]);
                Assert.AreEqual(expected[0].UserName, result.Select(x => x.UserName).ToArray()[0]);
                Assert.AreEqual(expected[1].Id, result.Select(x => x.Id).ToArray()[1]);
                Assert.AreEqual(expected[1].Name, result.Select(x => x.Name).ToArray()[1]);
                Assert.AreEqual(expected[1].UserName, result.Select(x => x.UserName).ToArray()[1]);
            }
        }
    }
}
