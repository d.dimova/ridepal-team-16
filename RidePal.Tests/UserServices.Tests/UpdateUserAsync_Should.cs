﻿using FakeItEasy;
using MapsterMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RidePal.Data.Context;
using RidePal.Data.Models;
using RidePal.Services;
using RidePal.Services.DTOs;
using RidePal.Services.Helpers;
using System.Threading.Tasks;

namespace RidePal.Tests.UserServices.Tests
{
    [TestClass]
    public class UpdateUserAsync_Should
    {
        [TestMethod]
        public async Task ReturnFalse_WhenUserIsNotFound()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnFalse_WhenUserIsNotFound));

            var userManagerFake = A.Fake<UserManager<User>>();
            var roleManagerFake = A.Fake<RoleManager<IdentityRole<int>>>();
            var signIngManagerFake = A.Fake<SignInManager<User>>();
            var appSettingsFake = A.Fake<IOptions<AppSettings>>();
            var loggerFake = A.Fake<ILogger<UserServicesAsync>>();

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new UserServicesAsync(arrangeCtx
                    , new Mapper()
                    , userManagerFake
                    , appSettingsFake
                    , signIngManagerFake
                    , roleManagerFake
                    , loggerFake);

                //Act
                var result = await sut.UpdateUserAsync(1, new UpdateUserDTO());

                //Assert
                Assert.IsFalse(result);
            }
        }

        [TestMethod]
        public async Task ReturnTrue_WhenUserIsUpdated()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnTrue_WhenUserIsUpdated));
            var users = TestSetup.GetUsers();

            var updateDTO = new UpdateUserDTO { Age = 45, Name = "Petur" };

            var userManagerFake = A.Fake<UserManager<User>>();
            var roleManagerFake = A.Fake<RoleManager<IdentityRole<int>>>();
            var signIngManagerFake = A.Fake<SignInManager<User>>();
            var appSettingsFake = A.Fake<IOptions<AppSettings>>();
            var loggerFake = A.Fake<ILogger<UserServicesAsync>>();

            using (var arrangeCtx = new RidePalContext(options))
            {
                await arrangeCtx.Users.AddRangeAsync(users);

                await arrangeCtx.SaveChangesAsync();
            }

            using (var actCtx = new RidePalContext(options))
            {
                var sut = new UserServicesAsync(actCtx
                    , new Mapper()
                    , userManagerFake
                    , appSettingsFake
                    , signIngManagerFake
                    , roleManagerFake
                    , loggerFake);

                //Act
                var result = await sut.UpdateUserAsync(1, updateDTO);
                var updatedResult = await actCtx.Users.FindAsync(1);
                var expected = updateDTO;

                //Assert
                Assert.IsTrue(result);
                Assert.AreEqual(expected.Age, updatedResult.Age);
                Assert.AreEqual(expected.Name, updatedResult.Name);
            }
        }
    }
}
