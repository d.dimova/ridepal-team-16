﻿using FakeItEasy;
using Mapster;
using MapsterMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RidePal.Data.Context;
using RidePal.Data.Models;
using RidePal.Services;
using RidePal.Services.DTOs;
using RidePal.Services.Helpers;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RidePal.Tests.UserServices.Tests
{
    [TestClass]
    public class GetLogsAsync_Should
    {
        [TestMethod]
        public async Task ReturnEmptyCollection_When_NoRecordsInDatabase()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnEmptyCollection_When_NoRecordsInDatabase));

            var userManagerFake = A.Fake<UserManager<User>>();
            var roleManagerFake = A.Fake<RoleManager<IdentityRole<int>>>();
            var signIngManagerFake = A.Fake<SignInManager<User>>();
            var appSettingsFake = A.Fake<IOptions<AppSettings>>();
            var loggerFake = A.Fake<ILogger<UserServicesAsync>>();

            using (var arrangeCtx = new RidePalContext(options))
            {
                var sut = new UserServicesAsync(arrangeCtx
                    , new Mapper()
                    , userManagerFake
                    , appSettingsFake
                    , signIngManagerFake
                    , roleManagerFake
                    , loggerFake);

                //Act
                var result = await sut.GetLogsAsync();

                //Assert
                Assert.IsInstanceOfType(result, typeof(IEnumerable<LogDTO>));
                Assert.IsTrue(result.ToList().Count() == 0);
            }
        }

        [TestMethod]
        public async Task ReturnValidUsersCollection_When_PlaylistsExist()
        {
            //Arrange
            var options = TestSetup.GetOptions(nameof(ReturnValidUsersCollection_When_PlaylistsExist));

            var userManagerFake = A.Fake<UserManager<User>>();
            var roleManagerFake = A.Fake<RoleManager<IdentityRole<int>>>();
            var signIngManagerFake = A.Fake<SignInManager<User>>();
            var appSettingsFake = A.Fake<IOptions<AppSettings>>();
            var loggerFake = A.Fake<ILogger<UserServicesAsync>>();

            var log = new Log { Message = "Nice", Exception = "Null", Properties = "razni", MessageTemplate = "nice", Level = "critical" };

            using (var arrangeCtx = new RidePalContext(options))
            {
                arrangeCtx.Logs.Add(log);

                arrangeCtx.SaveChanges();
            }

            using (var actCtx = new RidePalContext(options))
            {
                var sut = new UserServicesAsync(actCtx
                    , new Mapper()
                    , userManagerFake
                    , appSettingsFake
                    , signIngManagerFake
                    , roleManagerFake
                    , loggerFake);

                //Act
                var result = await sut.GetLogsAsync();
                var expected = log.Adapt<LogDTO>();

                //Assert
                Assert.IsInstanceOfType(result, typeof(IEnumerable<LogDTO>));
                Assert.AreEqual(expected.Id, result.Select(x => x.Id).ToArray()[0]);
                Assert.AreEqual(expected.Message, result.Select(x => x.Message).ToArray()[0]);
                Assert.AreEqual(expected.Level, result.Select(x => x.Level).ToArray()[0]);
            }
        }
    }
}
