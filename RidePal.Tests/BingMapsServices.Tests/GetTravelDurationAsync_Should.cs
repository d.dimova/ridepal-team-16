using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;
using RidePal.Services;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;


namespace RidePal.Tests.BingMapsServices.Tests
{
    [TestClass]
    public class GetTravelDurationAsync_Should
    {
        [TestMethod]
        public async Task ReturnNeg1_When_OriginAddressIsNull()
        {
            //Arrange
            var destinationAddress = "Pernik, BG";

            var mockFactory = new Mock<IHttpClientFactory>();
            var loggerMock = new Mock<ILogger<BingMapsServicesAsync>>();

            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            handlerMock
               .Protected()
               .Setup<Task<HttpResponseMessage>>(
                  "SendAsync",
                  ItExpr.IsAny<HttpRequestMessage>(),
                  ItExpr.IsAny<CancellationToken>()
               )
               .ReturnsAsync(new HttpResponseMessage()
               {
                   StatusCode = HttpStatusCode.OK,
                   Content = new StringContent("[{resourceSets: [{ resources: [{ travelDuration: 19032}]}]}]"),
               })
               .Verifiable();

            var httpClient = new HttpClient(handlerMock.Object);
            mockFactory.Setup(method => method.CreateClient(It.IsAny<string>())).Returns(httpClient);

            //Act
            var sut = new BingMapsServicesAsync(mockFactory.Object, loggerMock.Object);
            var result = await sut.GetTravelDurationAsync(null, destinationAddress);

            //Assert
            Assert.IsTrue(result == -1);
        }

        [TestMethod]
        public async Task ReturnNeg1_When_DestinationAddressIsNull()
        {
            //Arrange
            var originAddress = "Sofia, BG";
            
            var loggerMock = new Mock<ILogger<BingMapsServicesAsync>>();

            var mockFactory = new Mock<IHttpClientFactory>();
            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            handlerMock
               .Protected()
               .Setup<Task<HttpResponseMessage>>(
                  "SendAsync",
                  ItExpr.IsAny<HttpRequestMessage>(),
                  ItExpr.IsAny<CancellationToken>()
               )
               .ReturnsAsync(new HttpResponseMessage()
               {
                   StatusCode = HttpStatusCode.OK,
                   Content = new StringContent("[{resourceSets: [{ resources: [{ travelDuration: 19032}]}]}]"),
               })
               .Verifiable();

            var httpClient = new HttpClient(handlerMock.Object);
            mockFactory.Setup(method => method.CreateClient(It.IsAny<string>())).Returns(httpClient);

            //Act
            var sut = new BingMapsServicesAsync(mockFactory.Object, loggerMock.Object);
            var result = await sut.GetTravelDurationAsync(originAddress, null);

            //Assert
            Assert.IsTrue(result == -1);
        }

        [TestMethod]
        public async Task ReturnNeg1_When_ResponseStatusCodeIsNot200()
        {
            //Arrange
            var originAddress = "BG";
            var destinationAddress = "WA";

            var loggerMock = new Mock<ILogger<BingMapsServicesAsync>>();

            var mockFactory = new Mock<IHttpClientFactory>();

            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            handlerMock
               .Protected()
               .Setup<Task<HttpResponseMessage>>(
                  "SendAsync",
                  ItExpr.IsAny<HttpRequestMessage>(),
                  ItExpr.IsAny<CancellationToken>()
               )
               .ReturnsAsync(new HttpResponseMessage()
               {
                   StatusCode = HttpStatusCode.NoContent,
                   Content = new StringContent("{resourceSets: [{ resources: [{ travelDuration: 19032}]}], statusCode: 204}"),
               })
               .Verifiable();

            var httpClient = new HttpClient(handlerMock.Object);
            mockFactory.Setup(method => method.CreateClient(It.IsAny<string>())).Returns(httpClient);

            //Act
            var sut = new BingMapsServicesAsync(mockFactory.Object, loggerMock.Object);
            var result = await sut.GetTravelDurationAsync(originAddress, destinationAddress);

            //Assert
            Assert.IsTrue(result == -1);
        }
        
        [TestMethod]
        public async Task ReturnDuration_When_ResponseStatusCodeIs200()
        {
            //Arrange
            var originAddress = "Sofia, BG";
            var destinationAddress = "Varna, BG";

            var loggerMock = new Mock<ILogger<BingMapsServicesAsync>>();

            var mockFactory = new Mock<IHttpClientFactory>();

            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            handlerMock
               .Protected()
               .Setup<Task<HttpResponseMessage>>(
                  "SendAsync",
                  ItExpr.IsAny<HttpRequestMessage>(),
                  ItExpr.IsAny<CancellationToken>()
               )
               .ReturnsAsync(new HttpResponseMessage()
               {
                   StatusCode = HttpStatusCode.NoContent,
                   Content = new StringContent("{resourceSets: [{ resources: [{ travelDuration: 19032}]}], statusCode: 200}"),
               })
               .Verifiable();

            var httpClient = new HttpClient(handlerMock.Object);
            mockFactory.Setup(method => method.CreateClient(It.IsAny<string>())).Returns(httpClient);

            //Act
            var sut = new BingMapsServicesAsync(mockFactory.Object, loggerMock.Object);
            var result = await sut.GetTravelDurationAsync(originAddress, destinationAddress);

            //Assert
            Assert.AreEqual(19032, result);
        }
    }
}
