﻿using Mapster;
using Microsoft.EntityFrameworkCore;
using RidePal.Data.Context;
using RidePal.Data.Models;
using RidePal.Services.Helpers;
using System;
using System.Collections.Generic;

namespace RidePal.Tests
{
    public class TestSetup
    {
        public static void TestInitialize()
        {
            TypeAdapterConfig.GlobalSettings.Apply(new MapsterConfigs());
        }

        public static DbContextOptions<RidePalContext> GetOptions(string databaseName)
        {
            return new DbContextOptionsBuilder<RidePalContext>()
                .UseInMemoryDatabase(databaseName)
                .Options;
        }

        public static IList<Track> GetPlaylists()
        {
            return new Track[]
            {
                new Track
                {
                    ID = 1,
                    Title = "imagine",
                    Duration = 212,
                    Rank = 352481,
                    ReleaseDate = new DateTime(2018, 12, 14),
                    Preview = "http://track.preview.com/1",
                    TrackLink = "http://track.link.com/1",
                    AlbumID = 1,
                    ArtistID = 1,
                    GenreID = 1
                },
                new Track
                {
                    ID = 2,
                    Title = "bloodline",
                    Duration = 216,
                    Rank = 976542,
                    ReleaseDate = new DateTime(2019, 2, 8),
                    Preview = "http://track.preview.com/2",
                    TrackLink = "http://track.link.com/2",
                    AlbumID = 1,
                    ArtistID = 1,
                    GenreID = 1
                },
                new Track
                {
                    ID = 3,
                    Title = "bad idea",
                    Duration = 267,
                    Rank = 923645,
                    ReleaseDate = new DateTime(2019, 2, 8),
                    Preview = "http://track.preview.com/3",
                    TrackLink = "http://track.link.com/3",
                    AlbumID = 1,
                    ArtistID = 1,
                    GenreID = 1
                },
                new Track
                {
                    ID = 4,
                    Title = "Heartless",
                    Duration = 198,
                    Rank = 214704,
                    ReleaseDate = new DateTime(2019, 11, 27),
                    Preview = "http://track.preview.com/4",
                    TrackLink = "http://track.link.com/4",
                    AlbumID = 2,
                    ArtistID = 2,
                    GenreID = 2
                },
                new Track
                {
                    ID = 5,
                    Title = "Blinding Lights",
                    Duration = 200,
                    Rank = 624752,
                    ReleaseDate = new DateTime(2019, 11, 29),
                    Preview = "http://track.preview.com/5",
                    TrackLink = "http://track.link.com/5",
                    AlbumID = 2,
                    ArtistID = 2,
                    GenreID = 2
                },
                new Track
                {
                    ID = 6,
                    Title = "In Your Eyes",
                    Duration = 237,
                    Rank = 129463,
                    ReleaseDate = new DateTime(2020, 3, 24),
                    Preview = "http://track.preview.com/6",
                    TrackLink = "http://track.link.com/6",
                    AlbumID = 2,
                    ArtistID = 2,
                    GenreID = 2
                },
                new Track
                {
                    ID = 7,
                    Title = "Lust",
                    Duration = 166,
                    Rank = 379429,
                    ReleaseDate = new DateTime(2018, 9, 18),
                    Preview = "http://track.preview.com/7",
                    TrackLink = "http://track.link.com/7",
                    AlbumID = 3,
                    ArtistID = 3,
                    GenreID = 3
                },
                new Track
                {
                    ID = 8,
                    Title = "Roses",
                    Duration = 179,
                    Rank = 243579,
                    ReleaseDate = new DateTime(2016, 7, 22),
                    Preview = "http://track.preview.com/8",
                    TrackLink = "http://track.link.com/8",
                    AlbumID = 3,
                    ArtistID = 3,
                    GenreID = 3
                },
                new Track
                {
                    ID = 9,
                    Title = "Reflex",
                    Duration = 241,
                    Rank = 352892,
                    ReleaseDate = new DateTime(2016, 9, 23),
                    Preview = "http://track.preview.com/9",
                    TrackLink = "http://track.link.com/9",
                    AlbumID = 3,
                    ArtistID = 3,
                    GenreID = 3
                },
                 new Track
                {
                    ID = 10,
                    Title = "thank u, next(live)",
                    Duration = 389,
                    Rank = 675397,
                    ReleaseDate = new DateTime(2018, 12, 14),
                    Preview = "http://track.preview.com/10",
                    TrackLink = "http://track.link.com/10",
                    AlbumID = 1,
                    ArtistID = 1,
                    GenreID = 1
                }
            };
        }

        public static IList<Track> GetTracks()
        {
            return new Track[]
            {
                new Track
                {
                    ID = 1,
                    Title = "imagine",
                    Duration = 212,
                    Rank = 352481,
                    ReleaseDate = new DateTime(2018, 12, 14),
                    Preview = "http://track.preview.com/1",
                    TrackLink = "http://track.link.com/1",
                    AlbumID = 1,
                    ArtistID = 1,
                    GenreID = 1
                },
                new Track
                {
                    ID = 2,
                    Title = "bloodline",
                    Duration = 216,
                    Rank = 976542,
                    ReleaseDate = new DateTime(2019, 2, 8),
                    Preview = "http://track.preview.com/2",
                    TrackLink = "http://track.link.com/2",
                    AlbumID = 1,
                    ArtistID = 1,
                    GenreID = 1
                },
                new Track
                {
                    ID = 3,
                    Title = "bad idea",
                    Duration = 267,
                    Rank = 923645,
                    ReleaseDate = new DateTime(2019, 2, 8),
                    Preview = "http://track.preview.com/3",
                    TrackLink = "http://track.link.com/3",
                    AlbumID = 1,
                    ArtistID = 1,
                    GenreID = 1
                },
                new Track
                {
                    ID = 4,
                    Title = "Heartless",
                    Duration = 198,
                    Rank = 214704,
                    ReleaseDate = new DateTime(2019, 11, 27),
                    Preview = "http://track.preview.com/4",
                    TrackLink = "http://track.link.com/4",
                    AlbumID = 2,
                    ArtistID = 2,
                    GenreID = 2
                },
                new Track
                {
                    ID = 5,
                    Title = "Blinding Lights",
                    Duration = 200,
                    Rank = 624752,
                    ReleaseDate = new DateTime(2019, 11, 29),
                    Preview = "http://track.preview.com/5",
                    TrackLink = "http://track.link.com/5",
                    AlbumID = 2,
                    ArtistID = 2,
                    GenreID = 2
                },
                new Track
                {
                    ID = 6,
                    Title = "In Your Eyes",
                    Duration = 237,
                    Rank = 129463,
                    ReleaseDate = new DateTime(2020, 3, 24),
                    Preview = "http://track.preview.com/6",
                    TrackLink = "http://track.link.com/6",
                    AlbumID = 2,
                    ArtistID = 2,
                    GenreID = 2
                },
                new Track
                {
                    ID = 7,
                    Title = "Lust",
                    Duration = 166,
                    Rank = 379429,
                    ReleaseDate = new DateTime(2018, 9, 18),
                    Preview = "http://track.preview.com/7",
                    TrackLink = "http://track.link.com/7",
                    AlbumID = 3,
                    ArtistID = 3,
                    GenreID = 3
                },
                new Track
                {
                    ID = 8,
                    Title = "Roses",
                    Duration = 179,
                    Rank = 243579,
                    ReleaseDate = new DateTime(2016, 7, 22),
                    Preview = "http://track.preview.com/8",
                    TrackLink = "http://track.link.com/8",
                    AlbumID = 3,
                    ArtistID = 3,
                    GenreID = 3
                },
                new Track
                {
                    ID = 9,
                    Title = "Reflex",
                    Duration = 241,
                    Rank = 352892,
                    ReleaseDate = new DateTime(2016, 9, 23),
                    Preview = "http://track.preview.com/9",
                    TrackLink = "http://track.link.com/9",
                    AlbumID = 3,
                    ArtistID = 3,
                    GenreID = 3
                },
                 new Track
                {
                    ID = 10,
                    Title = "thank u, next(live)",
                    Duration = 389,
                    Rank = 675397,
                    ReleaseDate = new DateTime(2018, 12, 14),
                    Preview = "http://track.preview.com/10",
                    TrackLink = "http://track.link.com/10",
                    AlbumID = 1,
                    ArtistID = 1,
                    GenreID = 1
                }
            };
        }
        
        public static IList<Genre> GetGenres()
        {
            return new Genre[]
            {
                new Genre
                {
                    ID = 1,
                    Name = "Pop",
                    PictureURL = "http://genre.pic.com/1",
                },
                new Genre
                {
                    ID = 2,
                    Name = "R&B",
                    PictureURL = "http://genre.pic.com/2",
                },
                new Genre
                {
                    ID = 3,
                    Name = "Hip-Hop",
                    PictureURL = "http://genre.pic.com/3",
                },
                new Genre
                {
                    ID = 4,
                    Name = "Jazz",
                    PictureURL = "http://genre.pic.com/4",
                },
                new Genre
                {
                    ID = 5,
                    Name = "Metal",
                    PictureURL = "http://genre.pic.com/5",
                },
                new Genre
                {
                    ID = 6,
                    Name = "Rock",
                    PictureURL = "http://genre.pic.com/6",
                },
                new Genre
                {
                    ID = 7,
                    Name = "Techno",
                    PictureURL = "http://genre.pic.com/7",
                },
                new Genre
                {
                    ID = 8,
                    Name = "Dance",
                    PictureURL = "http://genre.pic.com/8",
                },
                new Genre
                {
                    ID = 9,
                    Name = "Classical",
                    PictureURL = "http://genre.pic.com/9",
                }
            };
        }

        public static IList<Album> GetAlbums()
        {
            return new Album[]
            {
                new Album
                {
                    ID = 1,
                    Title = "Thank u, next",
                    ArtistID = 1,
                    GenreID = 1,
                    ReleaseDate = new DateTime(),
                    TrackListUrl = "http://tracks.list/1",
                    PictureMediumUrl = "http://album.pic.com/1"
                },
                new Album
                {
                    ID = 2,
                    Title = "After Hours",
                    ArtistID = 2,
                    GenreID = 2,
                    ReleaseDate = new DateTime(),
                    TrackListUrl = "http://tracks.list/2",
                    PictureMediumUrl = "http://album.pic.com/2"
                },
                new Album
                {
                    ID = 3,
                    Title = "Collection One",
                    ArtistID = 3,
                    GenreID = 3,
                    ReleaseDate = new DateTime(),
                    TrackListUrl = "http://tracks.list/3",
                    PictureMediumUrl = "http://album.pic.com/3"
                }
            };
        }

        public static IList<Artist> GetArtists()
        {
            return new Artist[]
            {
                new Artist
                {
                    ID = 1,
                    Name = "Ariana Grande",
                    PictureURL = "http://artist.pic.com/1",
                    TrackListUrl = "http://tracks.list.com/1"
                },
                new Artist
                {
                    ID = 2,
                    Name = "The Weeknd",
                    PictureURL = "http://artist.pic.com/2",
                    TrackListUrl = "http://tracks.list.com/2"
                },
                new Artist
                {
                    ID = 3,
                    Name = "SAINt JHN",
                    PictureURL = "http://artist.pic.com/3",
                    TrackListUrl = "http://tracks.list.com/3"
                }
            };
        }

        public static IList<User> GetUsers()
        {
            return new User[]
            {
                new User
                {
                    Id = 1,
                    Name = "Georgi",
                    UserName = "georgi.g",
                    Email = "georgi.g@gmail.com",
                    Age = 44
                },
                new User
                {
                    Id = 2,
                    Name = "Stamat",
                    UserName = "s.aleksiev",
                    Email = "s.aleksiev@gmail.com",
                    Age = 25
                },
                new User
                {
                    Id = 3,
                    Name = "Penka",
                    UserName = "penka.yordanova",
                    Email = "penka.yordanova@gmail.com",
                    Age = 33
                },
            };
        }
    }
}
