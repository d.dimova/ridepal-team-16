![](images/RP16Logo.png)

# RidePal

RidePal is a web application for generating playlist for a given travel duration. The user provides his starting and ending points and the app calcualtes the travel duration by calling Bing Maps API service. Users can pick up to 4 genres with different percentages to be mixed in their generated playlist. By default the app generates playlist with single track per artist, which can be changed to allow multiple tracks from the same artist. Also there is an option for using only top tracks.


The project relies on 3 external API to function:
- [Deezer API](https://developers.deezer.com/api) - Main source for Albums , Artists and Tracks.
- [Bing Maps API](https://www.bingmapsportal.com) - Calcuation for route duration.
- [Unsplash API](https://unsplash.com/developers) - Image generation.
## AngularApp Address:

http://ridepal16.azurewebsites.net/home

## API Address:

http://ridepal16api.azurewebsites.net/swagger/index.html


## Installation

### Prerequisites
The following list of software should be installed:
- [SQL Server 2019](https://www.microsoft.com/en-us/sql-server/sql-server-downloads)
- [.NET 5](https://dotnet.microsoft.com/download/dotnet/5.0)
- [Node.js](https://nodejs.org/en/download/)

### Clone
- Clone or download the project on your local machine using `https://gitlab.com/dimoangelov/ridepal-team-16`
- All the `code` required to get started

### Setup

#### Setup REST API
- Navigate to the RidePal folder (e.g. cd /RidePal)
> restore the project and install its dependencies
```Dos
dotnet restore
```
- Database
> updates the database to the last migration
```Dos
dotnet tool install --global dotnet-ef
dotnet ef database update --project RidePal.Data
```

> open **new** terminal and run the project
```Dos
dotnet run --project RidePal.Web
```
> To seed the database from the Deezer API and create Admin User - Open your browser and navigate to http://localhost:5000/swagger/index.html . Execute InitalSeed Post Method:

![](images/InitialSeed.jpg)

#### Setup Angular Front-End
- Navigate to the ClientApp folder (e.g. cd RidePal/RidePal.Web/ClientApp)
```Dos
npm install
npm run start
```
- In your browser navigate to http://localhost:4200 

## Technologies

- ASP.NET 5
- ASP.NET Identity
- Entity Framework Core 5
- MS SQL Server
- Angular 11
- Angular Material
- HTML
- CSS
- Bootstrap
- Azure Cloud Services

## Database Schema
![](images/DBSchema.jpg)

## API Documentation

- [RidePal API Documentation](https://app.swaggerhub.com/apis-docs/MyOrg753/ride-pal_api/v1)
# Team 16

- Dayana Dimova - [GitLab](https://gitlab.com/d.dimova)

- Dimo Angelov - [LinkedIn](https://www.linkedin.com/in/dimo-angelov-6a6a28a/), [GitLab](https://gitlab.com/dimoangelov)