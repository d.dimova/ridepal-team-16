import { Component, OnInit } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import {  } from '@app/animations/route-animations';
import { User } from '@app/models';
import { AuthenticationService, NavbarService } from '@app/services';
import { fadeAnimation } from '../../animations/route-animations';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
  animations:[
    fadeAnimation
  ]
})
export class SidenavComponent implements OnInit {
  user: User;
  sidenavWidth = 4;

  public showSearch = true;
  constructor(public navbarService: NavbarService,
    public authenticationService: AuthenticationService) { 
      this.authenticationService.user.subscribe(x => this.user = x);
    }

  ngOnInit(): void {
    this.navbarService.showNavbar = true;
  }

  increase() {
    this.navbarService.toggleLogoText();
    this.navbarService.contentTextColor = 'white';
    this.sidenavWidth = 15;
	}
	decrease() {
    this.navbarService.toggleLogoText();
    this.navbarService.contentTextColor = '#26254a';
    this.sidenavWidth = 4;
  }

  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData.animation;
  }
}
