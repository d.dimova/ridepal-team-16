import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { User } from '@app/models';
import { AuthenticationService, NavbarService } from '@app/services';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  user: User;
  
  @Output() toggleSidenav = new EventEmitter<void>();

  constructor(
    public authenticationService: AuthenticationService, 
    public dialog: MatDialog, 
    public navbarService: NavbarService) {
    this.authenticationService.user.subscribe(x => this.user = x);
  }

  ngOnInit(): void {}

  logout() {
      this.authenticationService.logout();
  }

  createImgPath = (serverPath: string) => {
    return `http://localhost:5000/${serverPath}`;
  }
}
