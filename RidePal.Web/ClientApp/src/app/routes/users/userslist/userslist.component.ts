import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { first } from 'rxjs/operators';
import { UsersService } from '@app/services';
import { Playlist, Role, User } from '@app/models';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialog } from '@angular/material/dialog';
import { UserDeleteDialogComponent } from '../user-delete-dialog/user-delete-dialog.component';

@Component({
  selector: 'app-userslist',
  templateUrl: './userslist.component.html',
  styleUrls: ['./userslist.component.scss']
})
export class UserslistComponent implements OnInit, AfterViewInit {
  users = null;
 
  //User Table properties 
  dataSource = new MatTableDataSource<User>();
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  columnsToDisplay = ['name' , 'email', 'pic', 'username', 'role','age', 'update' , 'delete'];


  //Playlis table Properties
  dataSourcePLaylist = new MatTableDataSource<Playlist>();
  @ViewChild(MatSort) sortPlaylist: MatSort;
  @ViewChild(MatPaginator) paginatorPlaylist: MatPaginator;
  columnsToDisplayPlaylist = ['name']


  constructor(private usersService: UsersService,
     public dialog: MatDialog,) {} 

  ngOnInit() {
    this.usersService.getAll()
    .pipe(first())
    .subscribe(users => this.users = users);
   
    this.getUsers();
  }
  
  //create the datasource for the table
  public getUsers = ()=>{
    this.usersService.getAll().pipe().subscribe(result => { this.dataSource.data = result  as User[] });
  }
  //filter for the table
  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }
  //paging and sorting
  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }
  
  openUserDeleteDialog(id: number) {
    const dialogRef = this.dialog.open(UserDeleteDialogComponent);

    dialogRef.afterClosed().subscribe(response => {
      if(response){
        this.deleteUser(id);
        
        console.log('Dialog: User deleted.')
      }

      console.log('Dialog: Closed.')
    });
  }

  deleteUser(id: number) {
    const user = this.users.find(x => x.id === id);
    user.isDeleting = true;
    this.usersService.delete(id)
        .pipe(first())
        .subscribe(() => this.dataSource.data = this.dataSource.data.filter(x => x.id !== id));
  }

  createImgPath = (serverPath: string) => {
    return `http://localhost:5000/${serverPath}`;
  }

  syncDb(){
    this.usersService.sync().subscribe();
  }
}
