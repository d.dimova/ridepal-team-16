import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserslistComponent } from './userslist/userslist.component';
import { AddEditComponent } from './add-edit/add-edit.component';
import { UsersLayoutComponent } from './userslayout/userslayout.component';
import { AuthGuard } from '@app/helpers';
import { Role } from '@app/models';
import { PlaylistAdminComponent } from './playlist-admin/playlist-admin.component';

const routes: Routes = [
    {
        path: '', component: UsersLayoutComponent,
        children: [
            { path: '', component: UserslistComponent, canActivate: [AuthGuard],  data: { roles: [Role.Admin] } },
            { path: 'add', component: AddEditComponent, canActivate: [AuthGuard],  data: { roles: [Role.Admin] } },
            { path: 'edit/:id', component: AddEditComponent },
            { path: 'playlistadmin', component: PlaylistAdminComponent, canActivate: [AuthGuard],  data: { roles: [Role.Admin] }},
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UsersRoutingModule { }