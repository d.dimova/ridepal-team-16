import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, CanActivate } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AlertService , UsersService } from '@app/services';
import { AuthenticationService } from '../../../services/authentication.service';
import { User } from '@app/models';

@Component({
  selector: 'app-add-edit',
  templateUrl: './add-edit.component.html',
  styleUrls: ['./add-edit.component.scss']
})
export class AddEditComponent implements OnInit {
  form: FormGroup;
  id: number;
  isAddMode: boolean;
  loading = false;
  submitted = false;
  response: {
    dbPath: ''
  };
  uploadDone: boolean = false;
  user: User;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private alertService: AlertService,
    private userService: UsersService,
    private authenticationService: AuthenticationService
  ) {}

  ngOnInit() {
    this.authenticationService.user.subscribe(u => this.user = u);
    this.id = this.route.snapshot.params['id'];
    this.isAddMode = !this.id;

    // password not required in edit mode
    const passwordValidators = [Validators.minLength(6)];
    if (this.isAddMode) {
      passwordValidators.push(Validators.required);
    }

    this.form = this.formBuilder.group({
      email: ['', Validators.email],
      age: ['', [Validators.required, Validators.min(14), Validators.max(120)]],
      userName: ['', [Validators.required, Validators.min(3)]],
      name: ['', [Validators.required, Validators.min(3)]],
      password: ['', passwordValidators],
      imageURL: ['']
    });

    if (!this.isAddMode) {
      this.userService.getById(this.id)
        .pipe(first())
        .subscribe(x => this.form.patchValue(x));
    }
  }

  public uploadFinished = (event) => {
    this.response = event;
    this.uploadDone = true;
  }
  
  get f() {
    return this.form.controls;
  }

  onSubmit() {
    this.submitted = true;

    if (this.form.invalid) {
      return;
    }

    this.loading = true;
    if (this.isAddMode) {
      this.createUser();
    } else {
      if (this.uploadDone) {
        this.form.value.imageURL = this.response.dbPath;
      }
      this.updateUser();
    }
  }

  private async createUser() {
    (await this.userService.register(this.form.value))
    .pipe(first())
      .subscribe({
        next: () => {
          this.alertService.success('User added successfully', {
            keepAfterRouteChange: true
          });
          this.router.navigate(['../'], {
            relativeTo: this.route
          });
        },
        error: error => {
          this.alertService.error(error);
          this.loading = false;
        }
      });
  }

  private async updateUser() {
    this.userService.update(this.id, this.form.value)
      .pipe(first())
      .subscribe({
        next: () => {
          if (this.user.role === "Admin") {
            if(this.id === this.user.id){
              this.authenticationService.refreshUser(this.id).pipe(first()).subscribe();
            }

            this.router.navigate(['../../'], {
              relativeTo: this.route
            });

           
          } 
          else {
            this.router.navigate(['../../../profile'], {
              relativeTo: this.route
            });
            this.authenticationService.refreshUser(this.id).pipe(first()).subscribe();
          }
        },
        error: error => {
          this.alertService.error(error);
          this.loading = false;
        }
      });
  }

  cancel() {
    if (this.user.role === "Admin") {
      this.router.navigate(['../../'], {
        relativeTo: this.route
      });
    } else {
      this.router.navigate(['../../../profile'], {
        relativeTo: this.route
      });
    }
  }
}
