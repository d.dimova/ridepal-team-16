import { UploadComponent } from './../../helpers/upload/upload.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { MatDividerModule } from '@angular/material/divider';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatInputModule } from '@angular/material/input';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSelectModule } from '@angular/material/select';
import { MatSortModule } from '@angular/material/sort';
import { UserslistComponent } from './userslist/userslist.component';
import { AddEditComponent } from './add-edit/add-edit.component';
import { UsersRoutingModule } from './users-routing.module';
import { UsersLayoutComponent } from './userslayout/userslayout.component';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatTabsModule } from '@angular/material/tabs';
import { PlaylistAdminComponent } from './playlist-admin/playlist-admin.component';
import { UserDeleteDialogComponent } from './user-delete-dialog/user-delete-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';

@NgModule({
  declarations: [
    UserslistComponent,
    AddEditComponent,
    UserslistComponent,
    UsersLayoutComponent,
    UploadComponent,
    PlaylistAdminComponent,
    UserDeleteDialogComponent,
  ],
  imports: [
    CommonModule,
    MatTableModule,
    ReactiveFormsModule,
    MatDividerModule,
    MatProgressSpinnerModule,
    MatInputModule,
    MatSlideToggleModule,
    MatSelectModule,
    ReactiveFormsModule,
    MatSortModule,
    UsersRoutingModule,
    MatCardModule,
    MatButtonModule,
    MatListModule,
    MatPaginatorModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatTooltipModule,
    MatTabsModule,
    MatDialogModule,
  ], 
  exports: [
    UploadComponent,
    AddEditComponent,
    PlaylistAdminComponent
  ],
})
export class UsersModule { }
