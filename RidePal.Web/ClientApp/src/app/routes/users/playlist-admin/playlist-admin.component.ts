import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Playlist } from '@app/models';
import { first } from 'rxjs/operators';
import { PlaylistService } from '../../../services/playlist.service';

@Component({
  selector: 'app-playlist-admin',
  templateUrl: './playlist-admin.component.html',
  styleUrls: ['./playlist-admin.component.scss']
})
export class PlaylistAdminComponent implements OnInit {

  //Table properties
  dataSource = new MatTableDataSource<Playlist>();
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  columnsToDisplay = [ 'id', 'name' , 'rank' , 'totalPlaytime' , 'userName', 'pictureUrl', 'tracksCount', 'genres', 'update','delete'];


  constructor(private playlistService: PlaylistService) { }

  ngOnInit(): void {
    this.getPlaylistsAll();
  }

  public getPlaylistsAll(){
     this.playlistService.getPlaylists().pipe(first()).subscribe(result=>{this.dataSource.data = result as Playlist[];})
  }
  
  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  // paging and sorting
  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  deletePlaylist(id: number){
    this.playlistService.delete(id).pipe().subscribe(()=>this.dataSource.data = this.dataSource.data.filter(x=>x.id!==id));
  }

}
