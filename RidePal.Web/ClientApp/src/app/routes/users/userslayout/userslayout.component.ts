import { Component, OnInit } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { fadeAnimation } from '@app/animations/route-animations';

@Component({
  selector: 'app-userslayout',
  templateUrl: './userslayout.component.html',
  styleUrls: ['./userslayout.component.scss'],
  animations:[
    fadeAnimation
  ]
})
export class UsersLayoutComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData.animation;
  }
}
