import { Component, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatMenuTrigger } from '@angular/material/menu';
import { Playlist, User } from '@app/models';
import { AuthenticationService, PlaylistService } from '@app/services';
import { first } from 'rxjs/operators';
import { UsersService } from '../../services/users.service';
import { PlaylistDeleteDialogComponent } from '../playlist/playlist-delete-dialog/playlist-delete-dialog.component';
import { UserDeleteDialogComponent } from '../users/user-delete-dialog/user-delete-dialog.component';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  @ViewChild('menuTrigger') menuTrigger: MatMenuTrigger;
  
  user: User;
  userPlaylists: Playlist[];
  
  constructor(public authenticationService: AuthenticationService,
     public usersService: UsersService,
     public dialog: MatDialog,
     public playlistService: PlaylistService) { 
      this.authenticationService.user.subscribe(x => this.user = x);
     
  }

  ngOnInit(): void {
    this.usersService.getById(this.user.id)
    .pipe(first())
    .subscribe(response => this.userPlaylists = response.playlists);
  }

 
  openPlaylistDeleteDialog(id: number) {
    const dialogRef = this.dialog.open(PlaylistDeleteDialogComponent, {restoreFocus: false});

    dialogRef.afterClosed().subscribe(response => {
      if(response){
        this.deletePlaylist(id);

        console.log('Dialog: Playlist deleted.')
      }

      this.menuTrigger.focus();
      console.log('Dialog: Closed.')
    });
  }

  openUserDeleteDialog() {
    const dialogRef = this.dialog.open(UserDeleteDialogComponent);

    dialogRef.afterClosed().subscribe(response => {
      if(response){
        this.deleteUser(this.user.id);
        this.authenticationService.logout();
        
        console.log('Dialog: User deleted.')
      }

      console.log('Dialog: Closed.')
    });
  }

  deleteUser(id: number) {
    this.usersService.delete(id)
        .pipe(first())
        .subscribe();
  }

  deletePlaylist(id: number) {
    this.playlistService.delete(id)
        .pipe(first())
        .subscribe(()=> this.userPlaylists = this.userPlaylists.filter(pl => pl.id !== id));
  }

  createImgPath = (serverPath: string) => {
    return `http://localhost:5000/${serverPath}`;
  }
}
