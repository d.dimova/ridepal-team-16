import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlaylistListComponent } from './playlist-list/playlist-list.component';
import { AppRoutingModule } from '../../app-routing.module';
import { PlaylistCreateComponent } from './playlist-create/playlist-create.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatOptionModule } from '@angular/material/core';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatStepperModule } from '@angular/material/stepper';
import { PlyrModule } from 'ngx-plyr';
import { MatGridListModule } from '@angular/material/grid-list';
import { PlaylistCollectionComponent } from './playlist-collection/playlist-collection.component';
import { MinutesPipe } from '../../helpers/minutes.pipe';
import { MatTabsModule } from '@angular/material/tabs';
import { MatPaginatorModule } from '@angular/material/paginator';
import { PlaylistDeleteDialogComponent } from './playlist-delete-dialog/playlist-delete-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { PlaylistEditComponent } from './playlist-edit/playlist-edit.component';
import { PlaylistDetailsComponent } from './playlist-details/playlist-details.component';
import { MatListModule } from '@angular/material/list';
import { NgxAudioPlayerModule } from 'ngx-audio-player';
import { AudioPlayerComponent } from './audio-player/audio-player.component';
@NgModule({
  declarations: [
    PlaylistListComponent,
    PlaylistCreateComponent,
    PlaylistCollectionComponent,
    MinutesPipe,
    PlaylistDeleteDialogComponent,
    PlaylistEditComponent,
    PlaylistDetailsComponent,
    AudioPlayerComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatCardModule,
    MatFormFieldModule,
    MatIconModule,
    MatCheckboxModule,
    MatButtonModule,
    MatToolbarModule,
    MatInputModule,
    MatSelectModule,
    MatOptionModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatStepperModule,
    PlyrModule,
    MatGridListModule,
    MatTabsModule,
    MatPaginatorModule,
    MatDialogModule,
    MatListModule,
    NgxAudioPlayerModule,
  ],
  exports: [
    PlaylistListComponent,
    MinutesPipe
  ]
})
export class PlaylistModule { }
