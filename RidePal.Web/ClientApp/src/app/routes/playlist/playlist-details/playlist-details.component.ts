import { CustomTrack } from './../../../models/customTrack';
import { Component, OnInit } from '@angular/core';
import { Playlist } from '@app/models';
import { ActivatedRoute } from '@angular/router';
import { PlaylistService } from '../../../services/playlist.service';
import { first } from 'rxjs/operators';
import { Track } from 'ngx-audio-player';

@Component({
  selector: 'app-playlist-details',
  templateUrl: './playlist-details.component.html',
  styleUrls: ['./playlist-details.component.scss']
})
export class PlaylistDetailsComponent implements OnInit {
  id: number;
  playlist: Playlist;

  currentTrack: CustomTrack;
  currentTrackElemenet: HTMLElement;

  constructor(private route: ActivatedRoute,
    private playlistService: PlaylistService) { }

    ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];

    this.playlistService.getById(this.id)
    .pipe(first())
    .subscribe(pl => this.playlist = pl);

  }

  renderTrack(track: CustomTrack, id: number) {
    var trackElement = document.getElementById(`track${id}`);
    
    if (this.currentTrack?.id === track.id) {
      return;
    }
    else if (this.currentTrackElemenet){
      this.currentTrackElemenet.removeAttribute('hidden');

      this.currentTrack = track;
      trackElement.setAttribute('hidden', 'true');
    }
    else {
      this.currentTrack = track;
      trackElement.setAttribute('hidden', 'true');
    }
    
    this.currentTrackElemenet = trackElement;
  }

  shuffleTracks(){
    this.playlist.tracks = this.shuffle(this.playlist.tracks)
  }


  shuffle(track : CustomTrack[]) {
    var currentIndex = track.length, temporaryValue, randomIndex;
    while (0 !== currentIndex) {
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
      temporaryValue = track[currentIndex];
      track[currentIndex] = track[randomIndex];
      track[randomIndex] = temporaryValue;
    }
    return track;
  }

}
