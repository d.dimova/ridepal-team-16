import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Playlist, User } from '@app/models';
import { AlertService, UsersService } from '@app/services';
import { first } from 'rxjs/operators';
import { PlaylistService } from '../../../services/playlist.service';
import { AuthenticationService } from '../../../services/authentication.service';

@Component({
  selector: 'app-playlist-edit',
  templateUrl: './playlist-edit.component.html',
  styleUrls: ['./playlist-edit.component.scss']
})
export class PlaylistEditComponent implements OnInit {
  id: number;
  form: FormGroup;
  loading = false;
  submitted = false;
  user: User;

  constructor(
      private formBuilder: FormBuilder,
      private router: Router,
      private route: ActivatedRoute,
      private alertService: AlertService,
      private playlistService: PlaylistService,
      private authenticateService: AuthenticationService
  ) {
    this.authenticateService.user.subscribe(user => this.user = user);
  }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];

    this.form = this.formBuilder.group({
        name: ['', [ Validators.required, Validators.minLength(0), Validators.maxLength(150)]],
        pictureUrl: ['']
    });

    this.playlistService.getById(this.id)
    .pipe(first())
    .subscribe(x => this.form.patchValue(x));
  }

  onSubmit() {
      this.submitted = true;

      // stop here if form is invalid
      if (this.form.invalid) {
          return;
      }
     
      this.loading = true;
      this.updatePlaylist(this.id);
  }

  private async updatePlaylist(id: number) {
        this.playlistService.update(id, this.form.value)
          .pipe(first())
          .subscribe({
              next: () => {
                this.playlistService.getPlaylists()
                .pipe(first())
                .subscribe(response => this.user.playlists = response.filter(pl => pl.userId === this.user.id));

                this.alertService.success('Update successful', { keepAfterRouteChange: true });
                this.router.navigate([`/details/${id}`]);
              },
              error: error => {
                this.alertService.error(error);
                this.loading = false;
              }
          });
  }

  cancel() {
    if (this.user.role === "Admin") {
      this.router.navigate(['/users'], {
        relativeTo: this.route
      });
    } else {
      this.router.navigate(['/profile'], {
        relativeTo: this.route
      });
    }
  }
}
