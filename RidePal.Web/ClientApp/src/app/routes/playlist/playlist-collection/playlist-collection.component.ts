import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Playlist } from '@app/models';
import { PlaylistService } from '@app/services';
import { GenreService } from '@app/services/genre.service';
import { Genre } from '../../../models/genre';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-playlist-collection',
  templateUrl: './playlist-collection.component.html',
  styleUrls: ['./playlist-collection.component.scss']
})
export class PlaylistCollectionComponent implements OnInit {
  playlistList: Playlist[];
  searchForm: FormGroup;
  loading = false;
  submitted = false;
  floatLabelControl = new FormControl('auto');
  genres: Genre[];


  constructor(private playlistService: PlaylistService,
    private formBuilder: FormBuilder,
    private genreService: GenreService) {
    this.playlistService.getPlaylists()
    .pipe(first())
    .subscribe(response => this.playlistList = response);

    this.genreService.getGenres()
    .pipe(first())
    .subscribe(response => this.genres = response);
  }

  ngOnInit(): void {
    this.searchForm = this.formBuilder.group({
      playlistName: [''],
      genreId: [''],
      maxDuration: ['']
    });
  }

  onSubmit() {
    this.submitted = true;
    this.loading = true;
    
    this.playlistService.getFilteredPlaylists(this.searchForm.value)
    .pipe(first())
    .subscribe(response => this.playlistList = response);
    this.loading = false;
  }

  clearFilter(){
    this.playlistService.getPlaylists()
    .pipe(first())
    .subscribe(response => this.playlistList = response);
  }
}
