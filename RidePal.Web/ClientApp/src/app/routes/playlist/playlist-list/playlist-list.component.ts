import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Playlist } from '@app/models';
import { PlaylistService } from '@app/services';

@Component({
  selector: 'app-playlist-list',
  templateUrl: './playlist-list.component.html',
  styleUrls: ['./playlist-list.component.scss']
})
export class PlaylistListComponent implements OnInit {
  @Input() playlists: Playlist[];
  visualizerInstanciated: boolean;

  constructor(
    private playlistServices: PlaylistService
    , private route: ActivatedRoute
    , private router: Router) {
      this.visualizerInstanciated = false;
     }

  ngOnInit(): void {
  }

  makeInstance() {
    if(!this.visualizerInstanciated)
    {
      this.visualizerInstanciated = !this.visualizerInstanciated;
    }
  }
}
