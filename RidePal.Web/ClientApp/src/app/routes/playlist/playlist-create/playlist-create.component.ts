import { Genre } from '@app/models';
import { Component, OnInit } from '@angular/core';
import { PlaylistService } from '../../../services/playlist.service';

import { AbstractControl, Form, FormArray, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { AlertService } from '../../../services/alert.service';
import { User } from '@app/models';
import { AuthenticationService } from '@app/services';
import { GenreService } from '../../../services/genre.service';
import { BehaviorSubject } from 'rxjs';
import { AllowedGenres } from '../../../models/allowedGenres';
import { InputPlaylist } from '../../../models/input-playlist';

@Component({
  selector: 'app-playlist-create',
  templateUrl: './playlist-create.component.html',
  styleUrls: ['./playlist-create.component.scss']
})
export class PlaylistCreateComponent implements OnInit {
  user: User;
  form: FormGroup;
  locationFormGroup: FormGroup;
  nameFormGroup: FormGroup;
  genresFormGroup: FormGroup;
  customizationFormGroup: FormGroup;

  useTopTracks = new FormControl(false);
  repeatSameArtist = new FormControl(false);

  genres: Genre[];
  
  allowedGenres$ = new BehaviorSubject([]);
  selectedGenres: number[] = [];

  maxPercentValues: number[] = [4];
  formControlGenreNames: Map<string, AbstractControl>; 
  formControlPercentageNames: Map<string, AbstractControl>;

  loading: boolean = false;
  formError: string;

  constructor(private playlistService: PlaylistService
    , private authenticationService: AuthenticationService
    , private formBuilder: FormBuilder
    , private router: Router
    , private alertService: AlertService
    , private genreService: GenreService) {
      
      this.authenticationService.user.subscribe(currentUser => this.user = currentUser);
      this.genreService.getGenres().subscribe(response => this.genres = response);
    }

  get formArray() { 
    return this.form.get('formArray') as FormArray; 
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      formArray: this.formBuilder.array([
        this.locationFormGroup = this.formBuilder.group({
          originAddress: ['', [Validators.required, Validators.minLength(5)]],
          destinationAddress: ['', [Validators.required, Validators.minLength(5)]]
        }),
        this.nameFormGroup = this.formBuilder.group({
          name: ['', [Validators.required, Validators.minLength(3)]]
        }),
        this.genresFormGroup = this.formBuilder.group({
          genreId1: ['', [Validators.required, Validators.min(1)]],
          genrePercentage1: ['', [Validators.required, Validators.min(1), Validators.max(100)]],
        }),
        this.customizationFormGroup = this.formBuilder.group({
          useTopTracks: this.useTopTracks,
          repeatSameArtist: this.repeatSameArtist
        })
      ])
    });
    
    this.formControlGenreNames = new Map ([
      ['genreId1', this.genresFormGroup.get('genreId1')],
      ['genreId2', this.genresFormGroup.get('genreId2')],
      ['genreId3', this.genresFormGroup.get('genreId3')],
      ['genreId4', this.genresFormGroup.get('genreId4')]
    ]); 

    this.formControlPercentageNames =  new Map ([
      ['genrePercentage1', this.genresFormGroup.get('genrePercentage1')],
      ['genrePercentage2', this.genresFormGroup.get('genrePercentage2')],
      ['genrePercentage3', this.genresFormGroup.get('genrePercentage3')],
      ['genrePercentage4', this.genresFormGroup.get('genrePercentage4')]
    ]);

    this.maxPercentValues[0] = 100;
  }

  onSubmit(){
    if(this.percentageSum() != 100)
    {
      this.genresFormGroup.setValidators(this.sumCustomValidator);
      this.genresFormGroup.setErrors({ notValid : true });

      this.form.updateValueAndValidity();
    }

    if(this.form.invalid)
    {
      return;
    }

    this.loading = true;
    
    var inputModel = new InputPlaylist();
    this.formArray.controls.forEach(control => Object.assign(inputModel, control.value));

    this.playlistService.createPlaylist(this.user.id, inputModel)
            .pipe(first())
            .subscribe({
              next: response => {
                this.loading = false;

                this.router.navigate([`/details/${response.id}`]);
              },
              error: error => {
                this.form.setValidators(() => error? {'internalError': true}: null);
                this.form.setErrors({internalError: true});

                this.form.updateValueAndValidity();

                this.formError = error;
                this.loading = false;
                return;
              }
          });
  }

  check(control: string): void {
    let sum = this.percentageSum();
    let currentControl = this.formControlPercentageNames.get(control);

    let genreKeys = Array.from(this.formControlGenreNames.keys());
    let index = parseInt(control[control.length - 1]);

    if(currentControl.valid && sum < 100) {

      let percentageKeys = Array.from(this.formControlPercentageNames.keys());

      this.addControl(genreKeys[index], percentageKeys[index]);
    }
    else if(currentControl.valid && sum === 100) {
      index = 0;
      let currentSum = 0;

      for (const [key, value] of this.formControlPercentageNames.entries()) { 
        
        if(value !== undefined && value !== null) {
          if(currentSum == 100)
          {
            this.removeControl(genreKeys[index], key);
            this.maxPercentValues[index] = undefined;
          }
          else{
            currentSum += value.value;
          }

          if(currentSum == 100 && this.maxPercentValues[index] != value.value) {
            value.setValidators(Validators.max(value.value));
            value.reset(value.value);
            
            this.form.updateValueAndValidity();

            this.maxPercentValues[index] = value.value;
          }
        }

        
        index++;
      }
    }
    else {
      var currentSum = 0;

      for(let [key, value] of this.formControlPercentageNames.entries()) {
        if(value !== undefined && value !== null) {
          currentSum += value.value;

          if(currentSum > 100) {
            value.setValidators(this.percentageCustomValidator(key));
            this.form.updateValueAndValidity();

            value.setErrors({ max: true});
          }
        }
        
        index++;
      }
  }
  }

  changed(data: number, index: number){
    this.selectedGenres[index] = data;

    this.createTypesList();
  }

  private createTypesList() {
    let currentGenres = [];

    for(let genre of this.genres) {
      let selected = this.selectedGenres.includes(genre.id);

      currentGenres.push(new AllowedGenres(genre, !selected));
    };

    this.allowedGenres$.next(currentGenres);
  } 

  private percentageCustomValidator(currentPercentageControl: string) {
    let valueSum = 0;
    let index = 0;

    for(let [key, value] of this.formControlPercentageNames.entries()) {

      if(value !== undefined && value !== null && value.value !== "") {
        valueSum += value.value;
      }

      if(key === currentPercentageControl && valueSum <= 100) {
        this.maxPercentValues[index] = 100 - valueSum;
        break;
      }
      else if(key === currentPercentageControl && valueSum > 100) {
        this.maxPercentValues[index] = 100 - (valueSum - value.value);
        break;
      }
      
      index++;
    }

    return Validators.max(this.maxPercentValues[index]);
  }

  private sumCustomValidator() {
    return this.percentageSum() != 100 ? { "notValid": true } : null;
  }

  private percentageSum(): number{
    var sum: number = 0;

    for (const value of this.formControlPercentageNames.values()) { 

      if(value !== undefined && value !== null && value.value !== "")
      {
        sum += value.value;
      }
    }

    return sum;
  }

  private addControl(controlGenreName: string, controlPerName: string) {
    this.genresFormGroup.addControl(controlGenreName, new FormControl('', [Validators.required, Validators.min(1)]));

    const validator = this.percentageCustomValidator(controlPerName);
    this.genresFormGroup.addControl(controlPerName, new FormControl('', [Validators.required, Validators.min(1), validator]));
     
    this.form.updateValueAndValidity();

    this.formControlGenreNames.set(controlGenreName, this.genresFormGroup.get(controlGenreName));
    this.formControlPercentageNames.set(controlPerName, this.genresFormGroup.get(controlPerName));
  }

  private removeControl(controlGenreName: string, controlPerName: string) {
    this.genresFormGroup.removeControl(controlGenreName);
    this.genresFormGroup.removeControl(controlPerName);

    this.form.updateValueAndValidity();
    this.formControlGenreNames.set(controlGenreName, null);
    this.formControlPercentageNames.set(controlPerName, null);
  }
} 
