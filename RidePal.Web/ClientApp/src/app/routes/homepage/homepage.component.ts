import { AfterViewChecked, Component, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AuthenticationService } from '../../services/authentication.service';
import { Playlist, User } from '@app/models';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Track } from 'ngx-audio-player';  
import { PlaylistService } from '../../services/playlist.service';
import { first, take, filter, toArray } from 'rxjs/operators';
import { sortDescendingPriority } from '@angular/flex-layout';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: {displayDefaultIndicatorType: false}
  }]

})
export class HomepageComponent implements OnInit, AfterViewChecked {
  playlistOrderedList: Playlist[];

  constructor(private playlistService:  PlaylistService) {
    this.playlistService.getOrderedPlaylists('desc')
    .pipe(first())
    .subscribe(response => this.playlistOrderedList = response.slice(0,5));
  }
 
  ngOnInit() {
    
  }

  ngAfterViewChecked() {

  }

}

