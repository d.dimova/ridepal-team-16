import { Component, OnInit } from '@angular/core';
import { NavbarService } from '../../services/navbar.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-pagenotfound',
  templateUrl: './pagenotfound.component.html',
  styleUrls: ['./pagenotfound.component.scss']
})
export class PagenotfoundComponent implements OnInit {

  constructor(private navbarService: NavbarService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.navbarService.showNavbar = false;
    this.navbarService.showSidenav = false;
  }

  ngOnDestroy(): void {
    this.navbarService.showNavbar = true;
    this.navbarService.showSidenav = true;
  }
  
}
