import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MustMatch } from '@app/helpers';
import { User } from '@app/models';
import { AlertService, AuthenticationService, UsersService } from '@app/services';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {
  loginForm: FormGroup;
  public loginInvalid: boolean;

  registerForm: FormGroup;
  user: User;
  

  loginLoading = false;
  loginSubmitted = false;
  
  registerLoading = false;
  registerSubmitted = false;

  isLoginOpen = true;
  isRegisterOpen = false;

  loginError: string;
  registerError: string;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticatonService: AuthenticationService,
    private alertService: AlertService,
    private usersService: UsersService
  ) { }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

    this.registerForm = this.formBuilder.group({
      email: ['', Validators.email],
      age: ['', [Validators.required, Validators.min(14), Validators.max(120)]],
      username: ['', [Validators.required, Validators.min(3)]],
      name: ['', [Validators.required, Validators.min(3)]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', Validators.required] ,
      imageURL: ['']
    },
    { validator: MustMatch('password', 'confirmPassword') }
    );
  }

  get loginFormControls() { return this.loginForm.controls; }

  get registerFormControls() { return this.registerForm.controls; }

  async onLoginSubmit() {
    this.loginSubmitted = true;

    // reset alerts on submit
    this.alertService.clear();

    // stop here if form is invalid
    if (this.loginForm.invalid) {
        return;
    }

    this.loginLoading = true;
    this.authenticatonService.login(this.loginFormControls.username.value, this.loginFormControls.password.value)
        .pipe(first())
        .subscribe({
            next: () => {
                const returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/home';
                this.router.navigateByUrl(returnUrl);
                this.alertService.success('Log In successful', { keepAfterRouteChange: true });

                this.loginLoading = false;
            },
            error: error => {
                // this.alertService.error(error);
                this.loginForm.setValidators(() => error? {'internalError': true}: null);
                this.loginForm.setErrors({internalError: true});

                this.loginForm.updateValueAndValidity();

                this.loginError = error;
                this.loginLoading = false;
                return;
            }
        });
  }

  async onRegisterSubmit() {
    this.registerSubmitted = true;

    // reset alerts on submit
    this.alertService.clear();

    // stop here if form is invalid
    if (this.registerForm.invalid) {
        return;
    }
   
    this.registerLoading = true;
    (await this.usersService.register(this.registerForm.value))
        .pipe(first())
        .subscribe({
            next: () => {
              this.authenticatonService.login(this.registerFormControls.username.value, this.registerFormControls.password.value)
              .pipe(first())
              .subscribe({
                  next: () => {
                      const returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/profile';
                      this.router.navigateByUrl(returnUrl);
                  }});
            },
            error: error => {
              this.registerForm.setValidators(() => error? {'internalError': true}: null);
              this.registerForm.setErrors({internalError: true});

              this.registerForm.updateValueAndValidity();

              this.registerError = error;
              this.registerLoading = false;
              return;
            }
        });
  }

  onLoginButtonClick(): boolean {
    if (this.isLoginOpen){
      return this.isLoginOpen;
    }

    this.isRegisterOpen = !this.isRegisterOpen;
    return this.isLoginOpen = !this.isLoginOpen;
  }

  onRegisterButtonClick(): boolean {
    if (this.isRegisterOpen){
      return this.isRegisterOpen;
    }

    this.isLoginOpen = !this.isLoginOpen;
    return this.isRegisterOpen = !this.isRegisterOpen;
  }

}
