import { Component, OnInit, ElementRef, Inject } from '@angular/core';
import { NavbarService } from '../../services/navbar.service';
import { DOCUMENT } from '@angular/common';
import Typewriter from 't-writer.js';

@Component({
  selector: 'app-intropage',
  templateUrl: './intropage.component.html',
  styleUrls: ['./intropage.component.scss']
})
export class IntropageComponent implements OnInit {

  constructor( 
    @Inject(DOCUMENT) private document, 
    private elementRef: ElementRef,
    public navbarService: NavbarService) {
   }

  ngOnInit(): void {
    this.navbarService.showSidenav = false;

    this.loadDynamicText();
  }

  ngOnDestroy(): void {
    this.navbarService.showSidenav = true;
  }

  loadDynamicText() {
    const target = document.querySelector('.tw');

    const writer = new Typewriter(target, {
      loop: true,
      typeColor: 'white',
      animateCursor: 'none',
      typeSpeed: 200,
      deleteSpeed: 100,
    })

    writer
      .removeCursor()
      .type("Adventure")
      .rest(2000)
      .clear()
      .type("Made")
      .rest(2000)
      .clear()
      .type("Better")
      .rest(2000)
      .clear()
      .start();
  }
}
