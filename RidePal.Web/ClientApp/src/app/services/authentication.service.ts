import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { User } from '../models/user';


@Injectable({ 
    providedIn: 'root'
 })
export class AuthenticationService  {
    private userSubject: BehaviorSubject<User>;
    public user: Observable<User>;

    constructor(
        private router: Router,
        private http: HttpClient,
        
    ) {
        this.userSubject = new BehaviorSubject<User>(JSON.parse(sessionStorage.getItem('user')));
        this.user = this.userSubject.asObservable();
    }

    public get userValue(): User {
        return this.userSubject.value;
    }

    login(username: any, password: any): Observable <User> {
        return this.http.post<User>(`${environment.apiUrl}/user/authenticate`, { username, password })
            .pipe(map(user => {
                // store user details and jwt token in session storage to keep user logged in between page refreshes
                sessionStorage.setItem('user', JSON.stringify(user));
                
                this.userSubject.next(user);
                return user;
            }));
    }

    logout() {
        // remove user from session storage and set current user to null
        sessionStorage.removeItem('user');
        this.userSubject.next(null);
        this.router.navigate(['/home']);
    }

    refreshUser(id: number){
        return this.http.get < User > (`${environment.apiUrl}/user/${id}`)
        .pipe(map(user => {
            // store user details and jwt token in session storage to keep user logged in between page refreshes
            sessionStorage.setItem('user', JSON.stringify(user));
            
            this.userSubject.next(user);
            return user;
        }));
    }
}