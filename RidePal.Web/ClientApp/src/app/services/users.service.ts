import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '@app/models';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  private userSubject: BehaviorSubject < User > ;
  public user: Observable < User > ;
 
  constructor(
    private http: HttpClient,
  ) { }


  public get userValue(): User {
    return this.userSubject.value;
  }

  public async register(user: User) {
    return this.http.post(`${environment.apiUrl}/user/register`, user);
  }

  getAll(): Observable < User[] > {
    return this.http.get < User[] > (`${environment.apiUrl}/user/all`);
  }

  getById(id: number): Observable < User > {
    return this.http.get < User > (`${environment.apiUrl}/user/${id}`);
  }

  update(id: number, params: any) {
    return this.http.put(`${environment.apiUrl}/user/${id}`, params);
  }

  delete(id: number){
    return this.http.delete(`${environment.apiUrl}/user/${id}`)
  }

  sync(){
    return this.http.post(`${environment.apiUrl}/sync/initialseed`, '')
  }
}