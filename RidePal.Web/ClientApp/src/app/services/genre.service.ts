import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';
import { Genre } from '@app/models/genre';
import { SingleGenre } from '@app/models';

@Injectable({
  providedIn: 'root'
})
export class GenreService {

  constructor(private http: HttpClient) { 
  }

  getGenres(): Observable< Genre[] >{
    return this.http.get< Genre[] >(`${environment.apiUrl}/Genre`);
  }

  getSingleGenreById(id: number): Observable< SingleGenre >{
    return this.http.get< SingleGenre >(`${environment.apiUrl}/Genre/${id}`);
  }
}
