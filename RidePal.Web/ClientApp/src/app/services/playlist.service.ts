import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';
import { FilterForm, Playlist } from '@app/models';
import { InputPlaylist } from './../models/input-playlist';

@Injectable({
  providedIn: 'root'
})
export class PlaylistService {
  playlist: Playlist;

  constructor(private http: HttpClient) { }

  createPlaylist(userId: number, model: InputPlaylist){
    return this.http.post<Playlist>(`${environment.apiUrl}/Playlist/Create/${userId}`, model);
  }

  getPlaylists(): Observable<Playlist[]>{
    return this.http.get<Playlist[]>(`${environment.apiUrl}/Playlist`);
  }

  getOrderedPlaylists(order: string): Observable<Playlist[]>{
    return this.http.get<Playlist[]>(`${environment.apiUrl}/Playlist/sort?order=${order}`);
  }

  getFilteredPlaylists(form: FilterForm): Observable<Playlist[]>{
    return this.http.get<Playlist[]>(`${environment.apiUrl}/Playlist/filter?genreID=${form.genreId}&name=${form.playlistName}&duration=${form.maxDuration}`);
  }

  getById(id: number): Observable<Playlist>{
    return this.http.get<Playlist>(`${environment.apiUrl}/Playlist/${id}`);
  }

  update(id: number, params: Playlist): Observable<string>{
    return this.http.put<string>(`${environment.apiUrl}/Playlist/Update/${id}`, params);
  }
  
  delete(id: number){
    return this.http.delete(`${environment.apiUrl}/Playlist/Delete/${id}`);
  }
}
