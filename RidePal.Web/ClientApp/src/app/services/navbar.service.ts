import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NavbarService {

  showNavbar: boolean ;
  showSidenav: boolean;
  showLogoText: boolean ;
  contentTextColor: string;

  constructor() {
    this.showNavbar = true;
    this.showSidenav = true;
    this.showLogoText = false;
    this.contentTextColor = '#26254a';
   }

   public toggleLogoText() {
     this.showLogoText = !this.showLogoText;
   }
}
