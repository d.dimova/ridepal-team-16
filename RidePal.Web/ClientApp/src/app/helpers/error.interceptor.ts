import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthenticationService } from '@app/services';
import { UsersService } from '../services/users.service';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor
    (
        private authenticationService: AuthenticationService,
        private usersService : UsersService
        ) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
             
            if ([401, 403].includes(err.status) && this.usersService.userValue) {
                // auto logout if 401 or 403 response returned from api
                this.authenticationService.logout();
            }
   
            const error = err.error?.message || err.statusText;
            console.error(err);
            return throwError(error);
        }))
    }
}