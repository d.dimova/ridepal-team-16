import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NavbarComponent } from './layout/navbar/navbar.component';
import { MatIconModule } from '@angular/material/icon';
import { LayoutModule } from '@angular/cdk/layout';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list'
import { JwtInterceptor, ErrorInterceptor} from './helpers';
import { MatCardModule } from '@angular/material/card';
import { MatMenuModule } from '@angular/material/menu';
import { MatTableModule } from '@angular/material/table';
import { MatDividerModule } from '@angular/material/divider';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatInputModule } from '@angular/material/input';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PagenotfoundComponent } from './routes/pagenotfound/pagenotfound.component';
import { AlertComponent } from './layout/alert/alert.component';
import { HomepageComponent } from './routes/homepage/homepage.component';
import { UsersModule } from './routes/users/users.module';
import { MatSortModule } from '@angular/material/sort';
import { matDialogAnimations, MatDialogModule, MatDialog, MatDialogActions } from '@angular/material/dialog';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { MatRippleModule } from '@angular/material/core';
import { PortalModule } from '@angular/cdk/portal';
import { PlaylistModule } from './routes/playlist/playlist.module';
import { IntropageComponent } from './routes/intropage/intropage.component';
import { MatRadioModule } from '@angular/material/radio';
import { SidenavComponent } from './layout/sidenav/sidenav.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { MatStepperModule } from '@angular/material/stepper';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { FlexLayoutModule } from '@angular/flex-layout';
import { matTooltipAnimations, MatTooltipModule } from '@angular/material/tooltip';
import { AccountComponent } from './routes/account/account.component';
import { ProfileComponent } from './routes/profile/profile.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { NgxAudioPlayerModule } from 'ngx-audio-player';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    PagenotfoundComponent,
    AlertComponent,
    HomepageComponent,
    IntropageComponent,
    SidenavComponent,
    AccountComponent,
    ProfileComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FlexLayoutModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatToolbarModule,
    MatIconModule,
    LayoutModule,
    MatButtonModule,
    MatSidenavModule,
    MatListModule,
    MatCardModule,
    MatMenuModule,
    MatTableModule,
    MatDividerModule,
    MatProgressSpinnerModule,
    MatInputModule,
    MatSlideToggleModule,
    MatSelectModule,
    ReactiveFormsModule,
    MatSortModule,
    MatDialogModule,
    ScrollingModule,
    FormsModule,
    UsersModule,
    MatRippleModule,
    MatSelectModule,
    MatSlideToggleModule,
    PortalModule,
    PlaylistModule,
    MatSlideToggleModule,
    MatRadioModule,
    MatStepperModule,
    MatFormFieldModule,
    MatTooltipModule,
    MatGridListModule,
    NgxAudioPlayerModule,
  ],
  exports: [
    AlertComponent,
   
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    { provide: STEPPER_GLOBAL_OPTIONS, useValue: { displayDefaultIndicatorType: false } },
    { provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: { appearance: 'fill' } },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
