import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagenotfoundComponent } from './routes/pagenotfound/pagenotfound.component';
import { AuthGuard } from './helpers/auth.guard';
import { HomepageComponent } from './routes/homepage/homepage.component';
import { Role } from './models';
import { PlaylistCreateComponent } from './routes/playlist/playlist-create/playlist-create.component';
import { IntropageComponent } from './routes/intropage/intropage.component';
import { AccountComponent } from './routes/account/account.component';
import { ProfileComponent } from './routes/profile/profile.component';
import { PlaylistCollectionComponent } from './routes/playlist/playlist-collection/playlist-collection.component';
import { PlaylistEditComponent } from './routes/playlist/playlist-edit/playlist-edit.component';
import { PlaylistDetailsComponent } from './routes/playlist/playlist-details/playlist-details.component';

const usersModule = () => import('./routes/users/users.module').then(x => x.UsersModule);

const routes: Routes = [
  { path: '', redirectTo: 'intro', pathMatch: 'full'},
  { path: 'home', component: HomepageComponent } ,
  { path: 'intro', component: IntropageComponent } ,
  { path: 'users', loadChildren: usersModule },
  { path: 'details/:id', component: PlaylistDetailsComponent },
  { path: 'update/:id', component: PlaylistEditComponent, canActivate: [AuthGuard] },
  { path: 'collection', component: PlaylistCollectionComponent },
  { path: 'create', component: PlaylistCreateComponent, canActivate: [AuthGuard] },
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
  { path: 'account', component: AccountComponent },
  { path: '', component: HomepageComponent } ,
  { path: '**', component: PagenotfoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
