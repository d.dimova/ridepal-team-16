export class InputPlaylist{
    originAddress: string;
    destinationAddress: string;
    name: string;
    genreId1: number;
    genrePercentage1: number;
    genreId2: number;
    genrePercentage2: number;
    genreId3: number;
    genrePercentage3: number;
    genreId4: number;
    genrePercentage4: number;
    useTopTracks: boolean;
    repeatSameArtist: boolean;
}