import { Genre } from './genre';

export class AllowedGenres {
    genre: Genre;
    allowed: boolean;
    
    constructor (genre: Genre, allowed: boolean) {
      this.genre = genre;
      this.allowed = allowed;
    }
  }