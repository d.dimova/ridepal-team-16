import { Playlist } from './playlist';
import { Role } from './role';
 
export class User {
    id: number;
    name: string;
    userName: string;
    email: string;
    role: Role;
    age: number;
    
    playlists: Playlist[];
    token: string;
    imageURL: string;
}