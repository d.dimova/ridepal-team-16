import { CustomTrack } from '.';

export class SingleGenre {
    id: number;
    name: string;
    pictureURL: number;

    albums: string[];
    tracks: CustomTrack[];
}