import { CustomTrack } from './customTrack';

export class Album{
    id: number;
    title: string;
    srtistId: number;
    genreId: number;
    releaseDate: Date;
    trackListUrl: string;

    tracks: CustomTrack[];
}