import { CustomTrack } from './customTrack';

export class Playlist{
    id: number;
    name: string;
    totalPlaytime: number;
    rank: number;
    userId: number;
    userName: string;
    tracksCount: number;
    genres: string[];
    pictureUrl: string;

    tracks: CustomTrack[];
}