export class CustomTrack{
    id: number;
    title: string;
    duration: number;
    rank: number;
    releaseDate: Date;
    preview: string;
    trackLink: string;
    albumId: number;
    artistId: number;
    genreId: number;
    album: string;
    artist: string;
    genre: string;
    albumPictureUrl: string;
    playlists: string[];
}