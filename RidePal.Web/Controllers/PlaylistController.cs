﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RidePal.Services.Contracts;
using RidePal.Services.DTOs;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RidePal.Web.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    public class PlaylistController : ControllerBase
    {
        private readonly ILogger<PlaylistController> logger;
        private readonly IPlaylistServicesAsync playlistServices;

        public PlaylistController(ILogger<PlaylistController> logger
            , IPlaylistServicesAsync playlistServices)
        {
            this.logger = logger;
            this.playlistServices = playlistServices;
        }

        /// <summary>
        ///  Gets all playlists from the database.
        /// </summary>
        /// <response code="200">Succesfull operation</response>
        /// <response code="204">No content</response>
        /// <response code="500">Internal server error</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetAllAsync()
        {
            var playlistDTOs = await this.playlistServices.GetAllPlaylistsAsync();

            if (playlistDTOs != null || playlistDTOs.Count() > 0)
            {
                return Ok(playlistDTOs);
            }

            return NoContent();
        }

        /// <summary>
        ///  Oreders playlists by different criteria.
        /// </summary>
        /// <response code="200">Succesfull operation</response>
        /// <response code="404">No records available</response>
        /// <response code="500">Internal server error</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [AllowAnonymous]
        [HttpGet()]
        [Route("sort")]
        public async Task<IActionResult> GetOrderedAsync([FromQuery] string order)
        {
            var playlistDTOs = await this.playlistServices.GetOrderedPlaylistsAsync(order);

            if (playlistDTOs != null)
                return Ok(playlistDTOs);

            return NotFound(new { message = "No records available." });
        }

        /// <summary>
        ///  Filters playlists by different criteria.
        /// </summary>
        /// <response code="200">Succesfull operation</response>
        /// <response code="404">No records available</response>
        /// <response code="500">Internal server error</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [AllowAnonymous]
        [HttpGet()]
        [Route("filter")]
        public async Task<IActionResult> GetFilteredAsync([FromQuery] string name = null, string genreID = null, string duration = null)
        {
            var playlistDTOs = await this.playlistServices.GetFilteredPlaylistsAsync(name, genreID, duration);

            if (playlistDTOs != null)
                return Ok(playlistDTOs);

            return NotFound(new { message = "No records available." });
        }

        /// <summary>
        ///  Gets a single playlist by ID.
        /// </summary>
        /// <response code="200">Succesfull operation.</response>
        /// <response code="404">Playlist with such ID does not exist!</response>
        /// <response code="500">Internal server error.</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetSinglePlaylistAsync(int id)
        {
            var playlistDTO = await this.playlistServices.GetPlaylistAsync(id);

            if (playlistDTO != null)
            {
                return Ok(playlistDTO);
            }

            //TODO:Logger
            return NotFound(new { message = "Playlist with such ID does not exist!" });
        }


        /// <summary>
        /// Generates playlist with the given parameters.
        /// </summary>
        /// <response code="200">Successful operation</response>
        /// <response code="400">The percentage of genres must be exactly 100%</response>
        /// <response code="400">The maximum count of genres allowed is 4</response>
        /// <response code="400">TSomething went wrong. Unable to generate playlist with the given genres</response>
        /// <response code="401">Unathorized</response>
        /// <response code="500">Internal server error</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPost("Create/{userID}")]
        public async Task<IActionResult> GeneratePlaylistAsync(int userID, [FromBody] InputPlaylistDTO inputDTO)
        {
            var playlistDTO = new CreatePlaylistDTO
            {
                OriginAddress = inputDTO.OriginAddress,
                DestinationAddress = inputDTO.DestinationAddress,
                Name = inputDTO.Name,
                UseTopTracks = inputDTO.UseTopTracks,
                RepeatSameArtist = inputDTO.RepeatSameArtist
            };

            playlistDTO.GenrePercentage = new Dictionary<int, int>() { { inputDTO.GenreId1, inputDTO.GenrePercentage1 } };

            if (inputDTO.GenreId2 != null && inputDTO.GenrePercentage2 != null)
                playlistDTO.GenrePercentage.Add((int)inputDTO.GenreId2, (int)inputDTO.GenrePercentage2);
            if (inputDTO.GenreId3 != null && inputDTO.GenrePercentage3 != null)
                playlistDTO.GenrePercentage.Add((int)inputDTO.GenreId3, (int)inputDTO.GenrePercentage3);
            if (inputDTO.GenreId4 != null && inputDTO.GenrePercentage4 != null)
                playlistDTO.GenrePercentage.Add((int)inputDTO.GenreId4, (int)inputDTO.GenrePercentage4);


            if (playlistDTO.GenrePercentage.Values.Sum() != 100)
            {
                //TODO:Logger
                return BadRequest(new { message = "The percentage of genres must be exactly 100%." });
            }
            if (playlistDTO.GenrePercentage.Count() > 4)
            {
                //TODO:Logger
                return BadRequest(new { message = "The maximum count of genres allowed is 4." });
            }

            var generatedPlaylistDTO = await this.playlistServices.CreatePlaylistAsync(playlistDTO, userID);

            if (generatedPlaylistDTO != null)
            {
                this.logger.LogInformation($"Playlist with name {playlistDTO.Name} created.");
                return Ok(generatedPlaylistDTO);
            }

            //TODO:Logger
            return BadRequest(new { message = "Unable to generate playlist. The duration of the journey was too long." });
        }

        /// <summary>
        /// Updates given playlist.
        /// </summary>
        /// <response code="200">Playlist with ID {id} was successfully updated</response>
        /// <response code="400">Failed to update playlist due to invalid ID or playlist model</response>
        /// <response code="401">Unathorized</response>
        /// <response code="500">Internal server error</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPut("Update/{id}")]
        public async Task<IActionResult> UpdatePlaylist(int id, [FromBody] UpdatePlaylistDTO playlistDTO)
        {
            if (id >= 1 && playlistDTO != null)
            {
                var result = await this.playlistServices.UpdateAsync(id, playlistDTO);

                if (result)
                {
                    return Ok(new { message = $"Playlist with ID {id} was successfully updated!" });
                }
            }

            return BadRequest(new { message = "Failed to update playlist due to invalid ID or playlist model." });
        }

        /// <summary>
        /// Deletes playlist by ID.
        /// </summary>
        /// <response code="204">Successful operation</response>
        /// <response code="400">Failed to delete playlist due to invalid ID</response>
        /// <response code="401">Unathorized</response>
        /// <response code="500">Internal server error</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpDelete("Delete/{id}")]
        public async Task<IActionResult> DeletePlaylist(int id)
        {
            if (id >= 1)
            {
                var result = await this.playlistServices.DeleteAsync(id);

                if (result)
                {
                    return NoContent();
                }
            }

            return BadRequest(new { message = "Failed to delete playlist due to invalid ID." });
        }
    }
}
