﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RidePal.Services.Contracts;
using RidePal.Services.DTOs;
using System;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace RidePal.Web.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IUserServicesAsync userServicesAsync;
        private readonly ILogger<UserController> logger;

        public UserController(IUserServicesAsync userServicesAsync
            , ILogger<UserController> logger)
        {
            this.userServicesAsync = userServicesAsync;
            this.logger = logger;
        }

        /// <summary>
        ///  Gets all users in the application
        /// </summary>
        /// <response code="200">Succesfull operation</response>
        /// <response code="401">Unathorized</response>
        /// <response code="500">Internal server error</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Authorize(Roles = "Admin")]
        [HttpGet("all")]
        public async Task<IActionResult> GetAllAsync()
        {
            var userDTOs = await this.userServicesAsync.GetAllAsync();

            if (userDTOs != null || userDTOs.Count() > 0)
            {
                return Ok(userDTOs);
            }

            return NoContent();
        }

        /// <summary>
        ///  Gets a specific user by ID
        /// </summary>
        /// <response code="200">Succesfull operation</response>
        /// <response code="400">Bad request</response>
        /// <response code="401">Unathorized</response>
        /// <response code="404">User with such ID does not exist!</response>
        /// <response code="500">Internal server error</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetSingleUserAsync(int id)
        {
            var user = await this.userServicesAsync.GetUserAsync(id);

            if (user != null)
                return Ok(user);

            return NotFound("User with such ID does not exist!");
        }

        /// <summary>
        /// Logs in the user, creates and returns JWT token
        /// </summary>
        /// <response code="200">Succesfull operation</response>
        /// <response code="400">Username or password is incorrect</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="500">Internal server error</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [AllowAnonymous]
        [HttpPost("authenticate")]
        public async Task<IActionResult> Authenticate([FromBody] LoginDTO model)
        {
            var loginDTO = model;

            var user = await this.userServicesAsync.AuthenticateAsync(loginDTO);

            if (user != null)
            {
                this.logger.LogInformation($"{model.Username} authenticated.", DateTime.Now);
                return Ok(user);
            }

            return BadRequest(new { message = "Username or password is incorrect" });
        }

        /// <summary>
        /// Registers a user and saves it in the database
        /// </summary>
        /// <response code="200">User created!</response>
        /// <response code="400">Unable to create user.</response>
        /// <response code="500">Internal server error</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [AllowAnonymous]
        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] RegisterModelDTO model)
        {
            var operationResult = await this.userServicesAsync.CreateUserAsync(model);

            if (operationResult)
            {
                this.logger.LogInformation($"User with id: {model.UserName}  created", DateTime.Now);
                return Ok(new { message = "User created!"});
            }

            return BadRequest(new { message = "Unable to create user." });
        }

        /// <summary>
        /// Updates given user.
        /// </summary>
        /// <response code="200">User updated</response>
        /// <response code="400">Failed to update user due to invalid ID or user model</response>
        /// <response code="500">Internal server error</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateUserAsync(int id, [FromBody] UpdateUserDTO updateModel)
        {
            if (id >= 1)
            {
                var operationResult = await this.userServicesAsync.UpdateUserAsync(id, updateModel);

                if (operationResult)
                    return Ok(new { message = "User updated." });
            }

            return BadRequest(new { message = "Failed to update user due to invalid ID or user model." });
        }

        /// <summary>
        /// Deletes the user by ID.
        /// </summary>
        /// <response code="200">User deleted</response>
        /// <response code="400">Failed to delete user due to invalid ID</response>
        /// <response code="500">Internal server error</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUserAsync(int id)
        {
            if (id >= 1)
            {
                var operationResult = await this.userServicesAsync.DeleteUserAsync(id);

                if (operationResult)
                    return Ok(new { message = "User deleted." });
            }
            
            return BadRequest(new { message = "Failed to delete user due to invalid ID." });
        }

        /// <summary>
        /// Get the logs from the Database.
        /// </summary>
        /// <response code="200">Succesfull operation</response>
        /// <response code="500">Internal server error</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Authorize(Roles = "Admin")]
        [HttpGet("logs")]
        public async Task<IActionResult> GetLogs()
        {
            var logsDTOs = await this.userServicesAsync.GetLogsAsync();

            return Ok(logsDTOs);
        }


        /// <summary>
        /// Uploads profile pic
        /// </summary>
        /// <response code="200">Succesfull operation</response>
        /// <response code="400">Failed to upload</response>
        /// <response code="500">Internal server error</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPost("upload")]
        public IActionResult UploadProfilePicture()
        {
            var file = Request.Form.Files[0];
            var folderName = Path.Combine("Resources", "Images");
            var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);

            if (file.Length > 0)
            {
                var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                var fullPath = Path.Combine(pathToSave, fileName);
                var dbPath = Path.Combine(folderName, fileName);
                using (var stream = new FileStream(fullPath, FileMode.Create))
                {
                    file.CopyTo(stream);
                }
                return Ok(new { dbPath });
            }

            return BadRequest();
        }
    }
}
