﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RidePal.Services.Contracts;
using System.Linq;
using System.Threading.Tasks;

namespace RidePal.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GenreController : ControllerBase
    {
        private readonly ILogger<GenreController> logger;
        private readonly IGenreServicesAsync genreServices;

        public GenreController(ILogger<GenreController> logger
            , IGenreServicesAsync genreServices)
        {
            this.logger = logger;
            this.genreServices = genreServices;
        }

        /// <summary>
        ///  Gets all genres from the database.
        /// </summary>
        /// <response code="200">Succesfull operation</response>
        /// <response code="204">No content</response>
        /// <response code="401">Unathorized</response>
        /// <response code="500">Internal server error</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet]
        public async Task<IActionResult> GetAllAsync()
        {
            var genreDTOs = await this.genreServices.GetAllGenresAsync();

            if (genreDTOs != null || genreDTOs.Count() > 0)
            {
                return Ok(genreDTOs);
            }

            return NoContent();
        }

        /// <summary>
        ///  Gets single genre by ID.
        /// </summary>
        /// <response code="200">Succesfull operation</response>
        /// <response code="404">Genre with such ID does not exist</response>
        /// <response code="500">Internal server error</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetSingleGenreAsync(int id)
        {
            if (id >= 1)
            {
                var genreDTO = await this.genreServices.GetGenreAsync(id);

                if (genreDTO != null)
                {
                    return Ok(genreDTO);
                }
            }

            return NotFound(new { message = "Genre with such ID does not exist!" });
        }

    }
}
