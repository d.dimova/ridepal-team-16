﻿using Hangfire;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RidePal.Services.Contracts;

namespace RidePal.Web.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class SyncController : Controller
    {
        private readonly ISyncServicesAsync syncServicesAsync;
        private readonly ILogger<SyncController> logger;
        private readonly IUserServicesAsync userServicesAsync;

        public SyncController(ISyncServicesAsync syncServicesAsync
            ,ILogger<SyncController> logger 
            ,IUserServicesAsync userServicesAsync)
        {
            this.syncServicesAsync = syncServicesAsync;
            this.logger = logger;
            this.userServicesAsync = userServicesAsync;
        }

        /// <summary>
        ///  Seeds the database, creates roles and creates Admin user.
        /// </summary>
        /// <response code="200">Seed jobs Completed. Seeded the DB</response>
        /// <response code="401">Unathorized</response>
        /// <response code="500">Internal server error</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [AllowAnonymous]
        [HttpPost]
        [Route("InitialSeed")]
        public IActionResult IntialSeedToDB()
        {
            var deezerInitialSeed = BackgroundJob.Enqueue(() => this.syncServicesAsync.SyncDataFromDeezer());
            var roleSeed = BackgroundJob.Enqueue(() => this.userServicesAsync.CreateRolesAsync());
            this.logger.LogInformation($"Seed jobs executed.");
            return Ok($"Seed jobs Completed. Seeded the DB");
        }

        /// <summary>
        /// Creates synchronization task in hangfire that runs daily.
        /// </summary>
        /// <response code="200">Sync Job Completed</response>
        /// <response code="401">Unathorized</response>
        /// <response code="500">Internal server error</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPost]
        [Route("Sync")]
        public IActionResult Sync()
        {
            RecurringJob.AddOrUpdate(() => this.syncServicesAsync.SyncDataFromDeezer(), Cron.Daily);
            return Ok($"Sync Job Completed");
        }
    }
}
