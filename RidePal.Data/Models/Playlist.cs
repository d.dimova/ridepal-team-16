﻿using Mapster;
using RidePal.Data.Models.Abstract;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RidePal.Data.Models
{
    public class Playlist : Entity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [AdaptIgnore(MemberSide.Destination)]
        public override int ID { get => base.ID; set => base.ID = value; }

        [Required]
        [StringLength(150)]
        public string Name { get; set; }

        [Required]
        public int TotalPlaytime { get; set; }

        [Required]
        public double Rank { get; set; }

        [Required]
        public int UserID { get; set; }

        [AdaptIgnore(MemberSide.Destination)]
        public int TracksCount { get; set; }

        public string PictureUrl { get; set; }

        [AdaptIgnore(MemberSide.Destination)]
        public User User { get; set; }

        [AdaptIgnore(MemberSide.Destination)]
        public ICollection<Track> Tracks { get; set; }

        [AdaptIgnore(MemberSide.Destination)]
        public ICollection<Genre> Genres { get; set; }

    }
}
