﻿using Mapster;
using Newtonsoft.Json;
using RidePal.Data.Models.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RidePal.Data.Models
{
    public class Track : Entity
    {
        [Required]
        public string Title { get; set; }

        [Required]
        public int Duration { get; set; }

        [Required]
        public int Rank { get; set; }

        public DateTime? ReleaseDate { get; set; }

        [Required]
        public string Preview { get; set; }

        public string TrackLink { get; set; }

        public int? AlbumID { get; set; }

        public int ArtistID { get; set; }

        public int GenreID { get; set; }

        [AdaptIgnore(MemberSide.Destination)]
        public Artist Artist { get; set; }

        [AdaptIgnore(MemberSide.Destination)]
        public Album Album { get; set; }

        [AdaptIgnore(MemberSide.Destination)]
        public Genre Genre { get; set; }

        [AdaptIgnore(MemberSide.Destination)]
        public ICollection<Playlist> Playlists { get; set; }
    }
}
