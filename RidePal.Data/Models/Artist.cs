﻿using Mapster;
using RidePal.Data.Models.Abstract;
using System.Collections.Generic;

namespace RidePal.Data.Models
{
    public class Artist : Entity
    {
        public string Name { get; set; }
        public string PictureURL { get; set; }
        public string TrackListUrl { get; set; }

        [AdaptIgnore(MemberSide.Destination)]
        public ICollection<Album> Albums { get; set; }

        [AdaptIgnore(MemberSide.Destination)]
        public ICollection<Track> Tracks { get; set; }
    }
}
