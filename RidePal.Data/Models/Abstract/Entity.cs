﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RidePal.Data.Models.Abstract
{
    public class Entity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public virtual int ID { get; set; }

        [Column(TypeName = "date")]
        [Required]
        public DateTime CreatedOn { get; set; } = DateTime.Now;

        [Column(TypeName = "date")]
        public DateTime? ModifiedOn { get; set; }

        [Column(TypeName = "date")]
        public DateTime? DeletedOn { get; set; }

        [Column(TypeName = "bit")]
        [Required]
        public bool IsDeleted { get; set; }
    }
}
