﻿using Mapster;
using Newtonsoft.Json;
using RidePal.Data.Models.Abstract;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RidePal.Data.Models
{
    public class Genre : Entity
    {
        [Required]
        public string Name { get; set; }
        public string PictureURL { get; set; }

        [AdaptIgnore(MemberSide.Destination)]
        public ICollection<Album> Albums { get; set; }

        [AdaptIgnore(MemberSide.Destination)]
        public ICollection<Track> Tracks { get; set; }

        public ICollection<Playlist> Playlists { get; set; }
    }
}
