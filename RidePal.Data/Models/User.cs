﻿using Mapster;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RidePal.Data.Models
{
    public class User : IdentityUser<int>
    {
        [Required]
        [MinLength(3), MaxLength(100)]
        public string Name { get; set; }

        [Required]
        [MinLength(3), MaxLength(100)]
        public override string UserName { get; set; }

        [Required]
        [Range(10, 120)]
        public int Age { get; set; }

        [Column(TypeName = "date")]
        [Required]
        public DateTime CreatedOn { get; set; } = DateTime.Now;

        [Column(TypeName = "date")]
        public DateTime? ModifiedOn { get; set; }

        [Column(TypeName = "date")]
        public DateTime? DeletedOn { get; set; }

        [Column(TypeName = "bit")]
        [Required]
        public bool IsDeleted { get; set; } = false;

        [NotMapped]
        public string Role { get; set; }

        [AdaptIgnore(MemberSide.Destination)]
        public ICollection<Playlist> Playlists { get; set; }

        public string Token { get; set; }

        [DataType(DataType.ImageUrl)]
        public string ImageURL { get; set; } = "Resources\\Images\\defaultProfPic.png";
    }
}
