﻿using Mapster;
using RidePal.Data.Models.Abstract;
using System;
using System.Collections.Generic;

namespace RidePal.Data.Models
{
    public class Album : Entity
    {
        public string Title { get; set; }
        public int? ArtistID { get; set; }
        public int? GenreID { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public string TrackListUrl { get; set; }
        public string PictureMediumUrl { get; set; }

        [AdaptIgnore(MemberSide.Destination)]
        public virtual Artist Artist { get; set; }

        [AdaptIgnore(MemberSide.Destination)]
        public virtual Genre Genre { get; set; }

        [AdaptIgnore(MemberSide.Destination)]
        public ICollection<Track> Tracks { get; set; }

    }
}
