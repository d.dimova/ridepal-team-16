﻿using MapsterMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RidePal.Data.Context;
using RidePal.Data.Models;
using RidePal.Services.Contracts;
using RidePal.Services.JSON_Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace RidePal.Services
{
    public class SyncServicesAsync : ISyncServicesAsync
    {
        private readonly RidePalContext rpContext;
        private readonly IMapper mapper;
        private readonly IHttpClientFactory clientFactory;
        private readonly ILogger<SyncServicesAsync> logger;

        public SyncServicesAsync(RidePalContext rpContext
            , IMapper mapper
            , IHttpClientFactory clientFactory
            , ILogger<SyncServicesAsync> logger)
        {
            this.rpContext = rpContext;
            this.mapper = mapper;
            this.clientFactory = clientFactory;
            this.logger = logger;
        }

        /// <summary>
        /// Seeds the Database with results extracted from Deezer public API.
        /// </summary>
        /// <returns></returns>
        public async Task SyncDataFromDeezer()
        {
            await SyncGenresAsync();
            await SyncTopAristsAsync(7);
            //Limit the albums to take
            await SyncAlbumsFromArtistsAsync(2);
            //First arg is albums to take from genre, second is how many track to take from each album
            await SyncTracksFromAlbumsAsync(30, 10);
            await SyncPlaylistsRanksAsync();
        }

        /// <summary>
        /// Syncs the genres in the Database from the results extracted from Deezer public API.
        /// </summary>
        /// <returns></returns>
        public async Task SyncGenresAsync()
        {
            var client = this.clientFactory.CreateClient();

            var response = await client.GetAsync("https://api.deezer.com/genre");

            var responseAsString = await response.Content.ReadAsStringAsync();

            var resultGenresJson = JsonConvert.DeserializeObject<GenreJsonList>(responseAsString);

            var genresFromAPI = mapper.Map<IEnumerable<Genre>>(resultGenresJson.GenreJsons);

            foreach (var genre in genresFromAPI)
            {
                var genreToUpdate = await this.rpContext.Genres.SingleOrDefaultAsync(x => x.ID.Equals(genre.ID));

                if (genreToUpdate != null)
                {
                    genreToUpdate.ID = genre.ID;
                    genreToUpdate.Name = genre.Name;
                    genreToUpdate.ModifiedOn = DateTime.Now;
                }
                else
                {
                    await this.rpContext.AddAsync(genre);
                }
            }
            this.logger.LogInformation($"Genres synced.", DateTime.Now);
            await this.rpContext.SaveChangesAsync();
        }

        /// <summary>
        /// Syncs the artists in the Database from the results extracted from Deezer public API.
        /// </summary>
        /// <param name="numberOfGenresToSkip">The number to skip from genres</param>
        /// <returns></returns>
        public async Task SyncTopAristsAsync(int numberOfGenresToSkip)
        {
            var client = this.clientFactory.CreateClient();

            var setToPreventDoubleInsert = new HashSet<ArtistJson>();

            var genres = this.rpContext.Genres
                .OrderBy(g => g.ID)
                .Skip(numberOfGenresToSkip)
                .Take(14)
                .AsNoTracking()
                .ToList();

            foreach (var item in genres)
            {
                var response = await client.GetAsync($"https://api.deezer.com/genre/{item.ID}/artists");

                var responseAsString = await response.Content.ReadAsStringAsync();

                var result = JsonConvert.DeserializeObject<ArtistJsonList>(responseAsString);

                foreach (var artistJ in result.ArtistJsons)
                {
                    var artistToSync = await this.rpContext.Artists.SingleOrDefaultAsync(a => a.ID.Equals(artistJ.ID));

                    if (artistToSync != null)
                    {
                        artistToSync.ID = artistJ.ID;
                        artistToSync.Name = artistJ.Name;
                        artistToSync.PictureURL = artistJ.PictureURL;
                        artistToSync.TrackListUrl = artistJ.TrackListUrl;
                    }
                    else
                    {
                        await this.rpContext.Artists.AddAsync(mapper.Map<Artist>(artistJ));
                    }
                    await this.rpContext.SaveChangesAsync();
                    Thread.Sleep(300);
                }
            }
            this.logger.LogInformation($"Artists synced.", DateTime.Now);
        }

        /// <summary>
        /// Syncs the albums in the Database from the results extracted from Deezer public API.
        /// </summary>
        /// <param name="limitAlbumsToTake">The total of albums to be taken per artist.</param>
        /// <returns></returns>
        public async Task SyncAlbumsFromArtistsAsync(int limitAlbumsToTake)
        {
            var client = this.clientFactory.CreateClient();

            var genres = this.rpContext.Genres.ToList();

            var artistList = this.rpContext.Artists.Take(30).ToList();

            //Limit the albums to take 
            //int limitAlbumsToTake = 2;

            foreach (var artist in artistList)
            {
                var response = await client.GetAsync($"https://api.deezer.com/artist/{artist.ID}/albums?limit={limitAlbumsToTake}");

                var responseAsString = await response.Content.ReadAsStringAsync();

                var resultAlbums = JsonConvert.DeserializeObject<AlbumJsonList>(responseAsString);
                resultAlbums.AlbumJsons
                    .Select(al => al.ArtistID = artist.ID)
                    .ToList();

                foreach (var albumJ in resultAlbums.AlbumJsons)
                {
                    var albumToSync = await this.rpContext.Albums.SingleOrDefaultAsync(al => al.ID.Equals(albumJ.ID));

                    if (albumToSync != null)
                    {
                        albumToSync.ID = albumJ.ID;
                        albumToSync.Title = albumJ.Title;
                        albumToSync.GenreID = albumJ.GenreID;
                        albumToSync.TrackListUrl = albumJ.TrackListUrl;
                        albumToSync.PictureMediumUrl = albumJ.PictureMediumUrl;
                    }
                    else if (genres.Any(g => g.ID.Equals(albumJ.GenreID)))
                    {
                        await this.rpContext.Albums.AddAsync(mapper.Map<Album>(albumJ));
                    }
                    await this.rpContext.SaveChangesAsync();
                }
                Thread.Sleep(300);
            }
            this.logger.LogInformation($"Albums synced.", DateTime.Now);
        }

        /// <summary>
        /// Syncs the tracks in the Database from the results extracted from Deezer public API.
        /// </summary>
        /// <param name="albumsToTake">Total albums to be taken per genre.</param>
        /// <param name="limitSongsToTake">Total tracks to be taken per album.</param>
        /// <returns></returns>
        public async Task SyncTracksFromAlbumsAsync(int albumsToTake, int limitSongsToTake)
        {
            var client = this.clientFactory.CreateClient();

            var albumsList = this.rpContext.Albums
                .AsNoTracking()
                .ToList();

            //Gouping the albums in order to take even number of tracks by genre ID
            var groupedByGenreAlbums = albumsList.GroupBy(a => a.GenreID).ToList();
            var albumsToUse = new List<Album>();
            foreach (var group in groupedByGenreAlbums)
            {
                albumsToUse.AddRange(group.Take(albumsToTake));
            }

            //Limit the songs to take per album
            //int limitSongsToTake = 10;

            foreach (var album in albumsToUse)
            {
                var response = await client.GetAsync($"https://api.deezer.com/album/{album.ID}/tracks?limit={limitSongsToTake}");
                if (!response.IsSuccessStatusCode)
                {
                    continue;
                }
                var responseAsString = await response.Content.ReadAsStringAsync();

                var resultTracks = JsonConvert.DeserializeObject<TrackJsonList>(responseAsString);

                foreach (var track in resultTracks.TrackJson)
                {
                    var trackToSync = await this.rpContext.Tracks.SingleOrDefaultAsync(tr => tr.ID.Equals(track.ID));

                    if (trackToSync != null)
                    {
                        trackToSync.ID = track.ID;
                        trackToSync.Preview = track.Preview;
                        trackToSync.Title = track.Title;
                        trackToSync.Rank = track.Rank;
                        trackToSync.TrackLink = track.TrackLink;
                        trackToSync.ArtistID = (int)album.ArtistID;
                        trackToSync.GenreID = (int)album.GenreID;
                        trackToSync.AlbumID = album.ID;
                    }
                    else
                    {
                        var trackToInsert = new Track
                        {
                            ID = track.ID,
                            Duration = track.Duration,
                            Preview = track.Preview,
                            Title = track.Title,
                            Rank = track.Rank,
                            TrackLink = track.TrackLink,
                            ArtistID = (int)album.ArtistID,
                            GenreID = (int)album.GenreID,
                            AlbumID = album.ID,
                            ReleaseDate = album.ReleaseDate
                        };

                        await this.rpContext.AddAsync(mapper.Map<Track>(trackToInsert));
                    }
                   
                }
                await this.rpContext.SaveChangesAsync();
                Thread.Sleep(200);
            }
            this.logger.LogInformation($"Tracks synced.", DateTime.Now);
        }

        /// <summary>
        /// Syncs the playlist in the Database according to the changes in tracks' ranking.
        /// </summary>
        /// <returns></returns>
        public async Task SyncPlaylistsRanksAsync()
        {
            await this.rpContext.Playlists
                .Include(pl => pl.Tracks)
                .ForEachAsync(pl => pl.Rank = pl.Tracks.Average(track => track.Rank));

            await this.rpContext.SaveChangesAsync();
            this.logger.LogInformation($"Playlist rank synce.", DateTime.Now);
        }
    }
}
