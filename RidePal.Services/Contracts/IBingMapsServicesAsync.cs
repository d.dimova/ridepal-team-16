﻿using System.Threading.Tasks;

namespace RidePal.Services.Contracts
{
    public interface IBingMapsServicesAsync
    {
        Task<int> GetTravelDurationAsync(string OriginAddress, string DestinationAddress);
    }
}
