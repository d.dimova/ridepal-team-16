﻿using System.Threading.Tasks;

namespace RidePal.Services.Contracts
{
    public interface IPictureServicesAsync
    {
        Task<string> GetRandomPicAsync();
    }
}
