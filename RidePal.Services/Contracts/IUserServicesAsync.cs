﻿using RidePal.Services.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RidePal.Services.Contracts
{
    public interface IUserServicesAsync
    {

        Task<IEnumerable<UserDTO>> GetAllAsync();

        Task<UserDTO> GetUserAsync(int id);

        Task<UserDTO> AuthenticateAsync(LoginDTO loginDTO);

        Task<bool> CreateUserAsync(RegisterModelDTO modelDTO);

        Task<bool> UpdateUserAsync(int id, UpdateUserDTO updateUserDTO);

        Task<bool> DeleteUserAsync(int id);

        Task CreateRolesAsync();

        Task<IEnumerable<LogDTO>> GetLogsAsync();
    }
}