﻿using RidePal.Data.Models;
using RidePal.Services.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RidePal.Services.Contracts
{
    public interface IPlaylistServicesAsync
    {
        Task<SinglePlaylistDTO> CreatePlaylistAsync(CreatePlaylistDTO playlistDTO, int userID);

        Task<IEnumerable<PlaylistDTO>> GetAllPlaylistsAsync();

        Task<IEnumerable<PlaylistDTO>> GetOrderedPlaylistsAsync(string order);

        Task<IEnumerable<PlaylistDTO>> GetFilteredPlaylistsAsync(string name, string genreID, string duration);

        Task<SinglePlaylistDTO> GetPlaylistAsync(int id);

        Task<bool> UpdateAsync(int id, UpdatePlaylistDTO playlistDTO);

        Task<bool> DeleteAsync(int id);
    }
}
