﻿using RidePal.Services.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RidePal.Services.Contracts
{
    public interface IGenreServicesAsync
    {
        Task<IEnumerable<GenreDTO>> GetAllGenresAsync();

        Task<SingleGenreDTO> GetGenreAsync(int id);
    }
}
