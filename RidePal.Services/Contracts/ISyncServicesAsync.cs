﻿using System.Threading.Tasks;

namespace RidePal.Services.Contracts
{
    public interface ISyncServicesAsync
    {
        Task SyncDataFromDeezer();
    }
}