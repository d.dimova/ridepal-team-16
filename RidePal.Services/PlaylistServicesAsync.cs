﻿using Mapster;
using MapsterMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using RidePal.Data.Context;
using RidePal.Data.Models;
using RidePal.Services.Contracts;
using RidePal.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RidePal.Services
{
    public class PlaylistServicesAsync : IPlaylistServicesAsync
    {
        private readonly RidePalContext rpContext;
        private readonly IBingMapsServicesAsync bingMapsServices;
        private readonly IPictureServicesAsync pictureServices;
        private readonly IMapper mapper;
        private readonly Random randomNumberGenerator;
        private readonly ILogger<PlaylistServicesAsync> logger;

        public PlaylistServicesAsync(RidePalContext rpContext
            , IBingMapsServicesAsync bingMapsServices
            , IPictureServicesAsync pictureServices
            , IMapper mapper
            , Random randomNumberGenerator
            , ILogger<PlaylistServicesAsync> _logger)
        {
            this.rpContext = rpContext;
            this.bingMapsServices = bingMapsServices;
            this.pictureServices = pictureServices;
            this.mapper = mapper;
            this.randomNumberGenerator = randomNumberGenerator;
            this.logger = _logger;
        }

        public IQueryable<Playlist> AllPlaylists
        {
            get => this.rpContext.Playlists
                .Where(pl => !pl.IsDeleted)
                .Include(pl => pl.User)
                .Include(pl => pl.Genres)
                .AsNoTracking();
        }

        public IQueryable<Genre> AllGenres
        {
            get => this.rpContext.Genres
                .Where(genre => !genre.IsDeleted)
                .Include(genre => genre.Playlists.Where(pl => !pl.IsDeleted))
                    .ThenInclude(pl => pl.User)
                .AsNoTracking();
        }

        public IEnumerable<Track> AllTracks
        {
            get => this.rpContext.Tracks
                .Where(pl => !pl.IsDeleted)
                .ToList();
        }
        /// <summary>
        /// Creates a playlist by the given model for the current user.
        /// </summary>
        /// <param name="generatePlaylistDTO">The model, from which the data is extracted.</param>
        /// <param name="userID">The current user ID.</param>
        /// <returns>Plylist DTO if the playlist was successfully created, otherwise the result is null.</returns>
        public async Task<SinglePlaylistDTO> CreatePlaylistAsync(CreatePlaylistDTO generatePlaylistDTO, int userID)
        {
            if (this.rpContext.Playlists.AsNoTracking().Where(pl => !pl.IsDeleted).Any(pl => pl.Name.Equals(generatePlaylistDTO.Name)))
            {
                this.logger.LogError("PlaylistServicesAsync: Playlist with the given name already exists.");
                return null;
            }

            var duration = await this.bingMapsServices.GetTravelDurationAsync(generatePlaylistDTO.OriginAddress, generatePlaylistDTO.DestinationAddress);

            if (duration != -1 && userID >= 1)
            {
                var playlist = await this.GeneratePlaylist(generatePlaylistDTO, userID, duration);

                if (playlist != null)
                {
                    var result = await this.rpContext.Playlists.AddAsync(playlist);

                    await this.rpContext.SaveChangesAsync();

                    return await this.GetPlaylistAsync(result.Entity.ID);
                }
                else
                {
                    return null;
                }
            }

            this.logger.LogError("PlaylistServicesAsync: Failed to create playlist. Either userID or duration is invalid.");
            return null;
        }

        /// <summary>
        /// Gets all playlists from the Database.
        /// </summary>
        /// <returns>Collection of Playlist DTOs.</returns>
        public async Task<IEnumerable<PlaylistDTO>> GetAllPlaylistsAsync()
        {
            var playlistDTOs = mapper.Map<IEnumerable<PlaylistDTO>>(this.AllPlaylists.ToList().Where(x => !x.IsDeleted));

            return await Task.FromResult(playlistDTOs);
        }

        /// <summary>
        /// Gets all playlists from the Database ordered by Rank.
        /// </summary>
        /// <param name="order">The sorting order.(asc/desc)</param>
        /// <returns>Collection of Playlist DTOs.</returns>
        public async Task<IEnumerable<PlaylistDTO>> GetOrderedPlaylistsAsync(string order)
        {
            var playlistDTOs = this.AllPlaylists;

            switch (order)
            {
                case "asc":
                    playlistDTOs = await playlistDTOs.FilterListAsync_Playlists(null, list => list.OrderBy(pl => pl.Rank));
                    break;
                case "desc":
                    playlistDTOs = await playlistDTOs.FilterListAsync_Playlists(null, list => list.OrderByDescending(pl => pl.Rank));
                    break;
            };

            return await Task.FromResult(playlistDTOs
                .Adapt<IEnumerable<PlaylistDTO>>()
                        .ToList());
        }

        /// <summary>
        /// Gets all playlists from the Database filtered by the given parameters.
        /// </summary>
        /// <param name="name">The name of the playlist.</param>
        /// <param name="genreID">The genre ID of the tracks.</param>
        /// <param name="duration">The total playtime of the playlist.</param>
        /// <returns>Collection of Playlist DTOs if parameters are valid, otherwise null.</returns>
        public async Task<IEnumerable<PlaylistDTO>> GetFilteredPlaylistsAsync(string name, string genreID, string duration)
        {
            return (!String.IsNullOrWhiteSpace(name), !String.IsNullOrWhiteSpace(genreID), !String.IsNullOrWhiteSpace(duration)) switch
            {
                (true, false, false) => await Task.FromResult(this.AllPlaylists.FilterListAsync_Playlists(pl => pl.Name.ToLower().Contains(name.ToLower()), null)
                .Result
                .Adapt<IEnumerable<PlaylistDTO>>()),

                (true, true, false) => await Task.FromResult(this.AllGenres.FirstOrDefault(genre => genre.ID.Equals(int.Parse(genreID)))?
                .Playlists
                .Where(pl => pl.Name.ToLower().Contains(name.ToLower()))
                .Adapt<IEnumerable<PlaylistDTO>>()) ?? null,

                (true, true, true) => await Task.FromResult(this.AllGenres.FirstOrDefault(genre => genre.ID.Equals(int.Parse(genreID)))?
                .Playlists
                .Where(pl => pl.Name.ToLower().Contains(name.ToLower()) && pl.TotalPlaytime <= int.Parse(duration) * 60)
                .Adapt<IEnumerable<PlaylistDTO>>()) ?? null,

                (false, true, false) => await Task.FromResult(this.AllGenres.FirstOrDefault(genre => genre.ID.Equals(int.Parse(genreID)))?
                .Playlists
                .Adapt<IEnumerable<PlaylistDTO>>()) ?? null,

                (false, true, true) => await Task.FromResult(this.AllGenres.FirstOrDefault(genre => genre.ID.Equals(int.Parse(genreID)))?
                .Playlists
                .Where(pl => pl.TotalPlaytime <= int.Parse(duration) * 60)
                .Adapt<IEnumerable<PlaylistDTO>>()) ?? null,

                (false, false, true) => await Task.FromResult(this.AllPlaylists.FilterListAsync_Playlists(pl => pl.TotalPlaytime <= int.Parse(duration) * 60, null)
                .Result
                .Adapt<IEnumerable<PlaylistDTO>>()),

                (true, false, true) => await Task.FromResult(this.AllPlaylists.FilterListAsync_Playlists(pl => pl.Name.ToLower().Contains(name.ToLower()) && pl.TotalPlaytime <= int.Parse(duration) * 60, null)
                .Result
               .Adapt<IEnumerable<PlaylistDTO>>()),

                _ => null
            };
        }

        /// <summary>
        /// Gets single playlist from the Database.
        /// </summary>
        /// <param name="id">The ID of the playlist.</param>
        /// <returns>Playlist DTO if ID is valid, otherwise null.</returns>
        public async Task<SinglePlaylistDTO> GetPlaylistAsync(int id)
        {
            var singlePlaylist = await this.rpContext.Playlists
                .Where(pl => !pl.IsDeleted)
                .Include(pl => pl.User)
                .Include(pl => pl.Genres)
                .Include(pl => pl.Tracks)
                .ThenInclude(tr => tr.Artist)
                .Include(pl => pl.Tracks)
                .ThenInclude(tr => tr.Genre)
                .Include(pl => pl.Tracks)
                .ThenInclude(tr => tr.Album)
                .AsNoTracking()
                .SingleOrDefaultAsync(pl => pl.ID.Equals(id));

            if (singlePlaylist != null)
            {
                var playlistDTO = singlePlaylist.Adapt<SinglePlaylistDTO>();

                return playlistDTO;
            }

            this.logger.LogError("PlaylistServicesAsync: Failed to find playlist with the given id.");
            return null;
        }

        /// <summary>
        /// Updates the values of a playlist based on the given model.
        /// </summary>
        /// <param name="id">The playlist ID.</param>
        /// <param name="playlistDTO">The model, from which the data is extracted.</param>
        /// <returns>True if the model is updated, otherwise - false.</returns>
        public async Task<bool> UpdateAsync(int id, UpdatePlaylistDTO playlistDTO)
        {
            var playlist = this.rpContext.Playlists
                .Where(pl => !pl.IsDeleted)
                .FirstOrDefault(pl => pl.ID.Equals(id));

            if (playlist != null)
            {
                playlist.Name = playlistDTO.Name;

                if (!String.IsNullOrWhiteSpace(playlistDTO.PictureUrl))
                {
                    playlist.PictureUrl = playlistDTO.PictureUrl;
                }

                playlist.ModifiedOn = DateTime.Now;

                this.rpContext.Entry(playlist).State = EntityState.Modified;

                await this.rpContext.SaveChangesAsync();

                return true;
            }

            return false;
        }

        /// <summary>
        /// Deletes a playlist.
        /// </summary>
        /// <param name="id">The playlist ID.</param>
        /// <returns>True if the model is deleted, otherwise - false.</returns>
        public async Task<bool> DeleteAsync(int id)
        {
            var playlist = this.rpContext.Playlists
                .FirstOrDefault(pl => pl.ID.Equals(id));

            if (playlist != null)
            {
                playlist.IsDeleted = true;
                playlist.DeletedOn = DateTime.Now;

                this.rpContext.Entry(playlist).State = EntityState.Modified;

                await this.rpContext.SaveChangesAsync();

                return true;
            }
            return false;
        }

        /// <summary>
        /// Removes or adds tracks from/to the Track list according to the left duration so that the total playtime of the playlist is in the range of +5/-5 min of the travel duration.
        /// </summary>
        /// <param name="unshuffledTracks">The list of current tracks.</param>
        /// <param name="artistIdHash">Hash set with existing artist IDs.</param>
        /// <param name="trackIdHash">Hash set with existing track IDs.</param>
        /// <param name="action">The action to be executed over the list.(increase/decrease)</param>
        /// <param name="genreID">The ID of the genre, which tracks to use. </param>
        /// <param name="totalTime">The maximum allowed tracks duration to be added/removed.</param>
        /// <param name="useTopTracks">Determins whether to use top tracks.</param>
        /// <param name="repeatArtist">Determins whether to repeat tracks from the same artist.</param>
        /// <returns>Returns the left duration in seconds of the Playlist in seconds after performing the desired action or -1 if no action is possible.</returns>
        private int AdjustDuration(ref List<Track> unshuffledTracks, ref HashSet<int> artistIdHash, ref HashSet<int> trackIdHash, string action, int genreID, int totalTime, bool useTopTracks, bool repeatArtist)
        {
            switch (action)
            {
                case "increase":
                    var genreTracks = new List<Track>();

                    foreach (var track in AllTracks
                        .Where(track => track.GenreID.Equals(genreID)))
                    {
                        if (!trackIdHash.Contains(track.ID))
                        {
                            genreTracks.Add(track);
                        }
                    }

                    if (genreTracks != null && genreTracks.Count() != 0)
                    {
                        if (useTopTracks)
                        {
                            genreTracks.OrderBy(track => track.Rank);
                        }

                        var trackToAdd = this.FindSuitableTrack(ref genreTracks, ref artistIdHash, totalTime, repeatArtist);

                        if (trackToAdd == null)
                        {
                            var totalAllowed = 600 - totalTime; //If the current time is more than 300sec, we can add at least 600sec of tracks more and be in the aquired range

                            trackToAdd = this.FindSuitableTrack(ref genreTracks, ref artistIdHash, totalAllowed, repeatArtist);
                        }

                        if (trackToAdd != null)
                        {
                            artistIdHash.Add(trackToAdd.ArtistID);
                            trackIdHash.Add(trackToAdd.ID);
                            totalTime -= trackToAdd.Duration;
                            unshuffledTracks.Add(trackToAdd);
                        }
                        else
                        {
                            this.logger.LogError("PlaylistServicesAsync: Failed to increase the duration. No more tracks are available from the given genre.");
                            return -1;
                        }
                    }
                    else
                    {
                        this.logger.LogError("PlaylistServicesAsync: Failed to adjust the duration. Either no such genre exists or no more tracks are available from the given genre.");
                        return -1;
                    }
                    break;
                case "decrease":
                    var trackToDel = unshuffledTracks.FirstOrDefault(track => track.Duration <= totalTime);

                    if (trackToDel == null)
                    {
                        var totalAllowed = 300 + totalTime; //If the current time is less than 300sec, we can remove at least 300sec of tracks to be in the aquired range

                        trackToDel = unshuffledTracks.FirstOrDefault(track => track.Duration <= totalAllowed);
                    }

                    if (trackToDel != null)
                    {
                        totalTime = (totalTime * -1) + trackToDel.Duration;
                        unshuffledTracks.Remove(trackToDel);
                        trackIdHash.Remove(trackToDel.ID);
                    }
                    else
                    {
                        this.logger.LogError("PlaylistServicesAsync: Failed to decrease the duration. No more tracks are available from the given genre.");
                        return -1;
                    }
                    break;
            }

            return totalTime;
        }

        /// <summary>
        /// Calculates the total allowed duration of tracks from the given genre for the generated playlist.
        /// </summary>
        /// <param name="genrePercentage">The percent of desired genre out of the whole playlist.</param>
        /// <param name="totalPlaylistTime">The travel duration.</param>
        /// <returns>The duration of tracks from the given genre.</returns>
        private int CalculateTotalTimeForGenre(int genrePercentage, int totalPlaylistTime)
        {
            return (int)(totalPlaylistTime * (genrePercentage / 100.0));
        }

        /// <summary>
        /// Fetches random picture from Unsplash external API.
        /// </summary>
        /// <returns>The URL of the picture.</returns>
        private async Task<string> FetchPictureUrl()
        {
            try
            {
                var picURL = await this.pictureServices.GetRandomPicAsync();

                return picURL;
            }
            catch (ArgumentException)
            {
                return null;
            }
        }

        /// <summary>
        /// Finds track that passes the defined conditions.
        /// </summary>
        /// <param name="genreTracks">The list of tracks from the current genre.</param>
        /// <param name="artistIdHash">Hash set with existing artist IDs.</param>
        /// <param name="totalTime">The maximum allowed tracks duration to be added.</param>
        /// <param name="repeatArtist">Determins whether to repeat tracks from the same artist.</param>
        /// <returns>Track if one is found, otherwise null.</returns>
        private Track FindSuitableTrack(ref List<Track> genreTracks, ref HashSet<int> artistIdHash, int totalTime, bool repeatArtist)
        {
            var suitableTracksToAdd = genreTracks.Where(track => track.Duration <= totalTime).ToList();

            Track trackToAdd = null;

            if (suitableTracksToAdd.Count() != 0)
            {
                if (!repeatArtist)
                {
                    foreach (var track in suitableTracksToAdd)
                    {
                        if (!artistIdHash.Contains(track.ArtistID))
                        {
                            genreTracks.Add(track);
                        }
                    }
                }
                else
                {
                    trackToAdd = suitableTracksToAdd[randomNumberGenerator.Next(suitableTracksToAdd.Count())];
                }
            }

            if (trackToAdd == null)
            {
                this.logger.LogError("PlaylistServicesAsync: No suitable tracks found.");
            }
            return trackToAdd;
        }

        /// <summary>
        /// Generates Playlist by the given model for the current user with defined duration.
        /// </summary>
        /// <param name="generatePlaylistDTO">The model, from which the data is extracted.</param>
        /// <param name="userID">The current user ID.</param>
        /// <param name="duration">The total playtime of the playlist.</param>
        /// <returns>Playlist model to add to the Database.</returns>
        private async Task<Playlist> GeneratePlaylist(CreatePlaylistDTO generatePlaylistDTO, int userID, int duration)
        {
            var genreID = 0;
            var leftDuration = duration;

            var unshuffledTracks = new List<Track>();
            var artistIdHash = new HashSet<int>();
            var trackIdHash = new HashSet<int>();
            var genreHash = new HashSet<Genre>();

            for (int i = 0; i < generatePlaylistDTO.GenrePercentage.Count; i++)
            {
                //Get the play time for the current genre
                var totalGenreTime = this.CalculateTotalTimeForGenre(generatePlaylistDTO.GenrePercentage.ElementAt(i).Value, duration);
                genreID = generatePlaylistDTO.GenrePercentage.ElementAt(i).Key;

                //Get tracks from current genre
                var currentDuration = this.InsertTracksFromGenre(ref unshuffledTracks, ref artistIdHash, ref trackIdHash, ref genreHash, genreID, totalGenreTime, generatePlaylistDTO.UseTopTracks, generatePlaylistDTO.RepeatSameArtist);

                if (currentDuration > 0)
                {
                    leftDuration -= currentDuration;
                    continue;
                }

                this.logger.LogError("PlaylistServicesAsync: Failed to generate playlist with the given genres.");
                return null;
            }

            while (leftDuration > 300 || leftDuration < -300)
            {
                var randNum = this.randomNumberGenerator.Next(generatePlaylistDTO.GenrePercentage.Count);
                genreID = generatePlaylistDTO.GenrePercentage.ElementAt(randNum).Key;

                leftDuration = leftDuration > 300
                    ? this.AdjustDuration(ref unshuffledTracks, ref artistIdHash, ref trackIdHash, "increase", genreID, leftDuration, generatePlaylistDTO.UseTopTracks, generatePlaylistDTO.RepeatSameArtist) //Playlist is too small -> more than -5 min
                    : this.AdjustDuration(ref unshuffledTracks, ref artistIdHash, ref trackIdHash, "decrease", genreID, (leftDuration * -1), false, false); //Playlist is too long -> more than +5 min

                if (leftDuration == -1)
                {
                    this.logger.LogError("PlaylistServicesAsync: Failed to adjust duration.");
                    return null;
                }
            }

            //Randomize and return tracks
            var shuffledTracks = this.RandomizeTracks(ref unshuffledTracks);

            var picURL = await this.FetchPictureUrl();

            while (picURL == null)
            {
                picURL = await this.FetchPictureUrl();
            }

            var playlist = new Playlist
            {
                Name = generatePlaylistDTO.Name,
                UserID = userID,
                Tracks = shuffledTracks.ToList(),
                Genres = genreHash.ToList(),
                TracksCount = shuffledTracks.Count(),
                PictureUrl = picURL,
                TotalPlaytime = shuffledTracks.Sum(track => track.Duration),
                Rank = shuffledTracks.Average(track => track.Rank)
            };

            return await Task.FromResult(playlist);
        }

        /// <summary>
        /// Adds tracks from the given genre to the Track list according to the desired genre duration.
        /// </summary>
        /// <param name="unshuffledTracks">The list of current tracks.</param>
        /// <param name="artistIdHash">Hash set with existing artist IDs.</param>
        /// <param name="trackIdHash">Hash set with existing track IDs.</param>
        /// <param name="genreHash">Hash set with existing genres.</param>
        /// <param name="genreID">The ID of the genre, which tracks to use. </param>
        /// <param name="totalTime">The maximum allowed tracks duration to be added.</param>
        /// <param name="useTopTracks">Determins whether to use top tracks.</param>
        /// <param name="repeatArtist">Determins whether to repeat tracks from the same artist.</param>
        /// <returns>Returns the total added tracks duration in seconds from the current genre or -1 if no genre or tracks are found.</returns>
        private int InsertTracksFromGenre(ref List<Track> unshuffledTracks, ref HashSet<int> artistIdHash, ref HashSet<int> trackIdHash, ref HashSet<Genre> genreHash, int genreID, int totalTime, bool useTopTracks, bool repeatArtist)
        {
            var currentGenreTracks = new List<Track>(this.AllTracks
                .Where(track => track.GenreID.Equals(genreID)).AsEnumerable());

            if (currentGenreTracks != null && currentGenreTracks.Count() != 0)
            {
                var genre = this.rpContext.Genres.FirstOrDefault(genre => genre.ID.Equals(genreID));
                genreHash.Add(genre);

                if (useTopTracks)
                {
                    currentGenreTracks.OrderBy(track => track.Rank);
                }

                var currentTime = 0;

                while (currentTime <= totalTime && currentGenreTracks.Count() != 0)
                {
                    var randIndex = randomNumberGenerator.Next(currentGenreTracks.Count());
                    var currentTrack = currentGenreTracks[randIndex];

                    if (!repeatArtist && artistIdHash.Contains(currentTrack.ArtistID))
                    {
                        currentGenreTracks.RemoveAt(randIndex);
                        continue;
                    }

                    artistIdHash.Add(currentTrack.ArtistID);
                    trackIdHash.Add(currentTrack.ID);
                    currentGenreTracks.RemoveAt(randIndex);
                    currentTime += currentTrack.Duration;

                    unshuffledTracks.Add(currentTrack);
                }

                return currentTime;
            }

            this.logger.LogError("PlaylistServicesAsync: Failed to insert tracks from genre. Either the genre is invalid or no tracks are found.");
            return -1;
        }
        /// <summary>
        /// Performs an algorithm for randomizing the order of elements in a collection of Tracks.
        /// </summary>
        /// <param name="unshuffledTracks">The list of current tracks.</param>
        /// <returns>New collection of shuffled Tracks.</returns>
        private IEnumerable<Track> RandomizeTracks(ref List<Track> unshuffledTracks)
        {
            var shuffledTracks = new HashSet<Track>();

            while (unshuffledTracks.Count() > 0)
            {
                var randIndex = this.randomNumberGenerator.Next(unshuffledTracks.Count());

                var currentTrack = unshuffledTracks[randIndex];

                shuffledTracks.Add(currentTrack);

                unshuffledTracks.RemoveAt(randIndex);
            }

            return shuffledTracks;
        }

    }
}
