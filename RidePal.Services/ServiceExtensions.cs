﻿using Microsoft.EntityFrameworkCore;
using RidePal.Data.Models;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace RidePal.Services
{
    public static class ServiceExtensions
    {
        /// <summary>
        /// Performs filtering and/or ordering actions over a collection of Playlists.
        /// </summary>
        /// <param name="playlists">The playlist on which the actions are performed.</param>
        /// <param name="filter">The filtering function to be performed.</param>
        /// <param name="orderBy">The ordering function to be performed.</param>
        /// <returns>Collection of Playlists with the applied changes.</returns>
        public static async Task<IQueryable<Playlist>> FilterListAsync_Playlists(this IQueryable<Playlist> playlists,
            Expression<Func<Playlist, bool>> filter = null,
            Func<IQueryable<Playlist>, IOrderedQueryable<Playlist>> orderBy = null)
        {
            if (filter != null)
                playlists = playlists.Where(filter);

            if (orderBy != null)
                return await Task.FromResult(orderBy(playlists).AsNoTracking());
            else
                return await Task.FromResult(playlists.AsNoTracking());
        }
    }
}
