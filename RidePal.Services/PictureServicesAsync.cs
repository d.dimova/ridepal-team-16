﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using RidePal.Services.Contracts;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace RidePal.Services
{
    public class PictureServicesAsync : IPictureServicesAsync
    {
        private readonly IHttpClientFactory httpClientFactory;
        private readonly ILogger logger;
        private const string UnspalshAccessKey = "1txhFrCqnsg8pY_SYZPMsY39nJxJ7lf-PFyDxP-9EPE";

        public PictureServicesAsync(IHttpClientFactory httpClientFactory
            ,ILogger<PictureServicesAsync> logger)
        {
            this.httpClientFactory = httpClientFactory;
            this.logger = logger;
        }

        /// <summary>
        /// Makes Request to Unsplash API and get a random picture.
        /// </summary>
        /// <returns>URL as string</returns>
        public async Task<string> GetRandomPicAsync()
        {
            var client = this.httpClientFactory.CreateClient();

            var response = await client.GetAsync($"https://api.unsplash.com/photos/random/?client_id={UnspalshAccessKey}&orientation=squarish&query=music");

            if (!response.IsSuccessStatusCode)
            {
                this.logger.LogError($"Error with request Unsplash Api Request. - {response.StatusCode}", DateTime.Now);
                throw new ArgumentException();
            }

            var responseAsString = await response.Content.ReadAsStringAsync();
                
            var jObj = JObject.Parse(responseAsString);

            var picUrl = jObj.SelectToken("urls.full").Value<string>();

            return picUrl;
        }
    }
}
