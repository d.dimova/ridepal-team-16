﻿using MapsterMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using RidePal.Data.Context;
using RidePal.Data.Models;
using RidePal.Services.Contracts;
using RidePal.Services.DTOs;
using RidePal.Services.Helpers;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace RidePal.Services
{
    public class UserServicesAsync : IUserServicesAsync
    {
        private readonly RidePalContext rpContext;
        private readonly IMapper mapper;
        private readonly UserManager<User> userManager;
        private readonly IOptions<AppSettings> appSettings;
        private readonly SignInManager<User> signInManager;
        private readonly RoleManager<IdentityRole<int>> roleManager;
        private readonly ILogger<UserServicesAsync> logger;

        public UserServicesAsync(RidePalContext rpContext
            , IMapper mapper
            , UserManager<User> userManager
            , IOptions<AppSettings> appSettings
            , SignInManager<User> signInManager
            , RoleManager<IdentityRole<int>> roleManager
            , ILogger<UserServicesAsync> _logger)
        {
            this.rpContext = rpContext;
            this.mapper = mapper;
            this.userManager = userManager;
            this.appSettings = appSettings;
            this.signInManager = signInManager;
            this.roleManager = roleManager;
            this.logger = _logger;
        }

        /// <summary>
        /// Gets all users from the Database.
        /// </summary>
        /// <returns>Collection of User DTOs.</returns>
        public async Task<IEnumerable<UserDTO>> GetAllAsync()
        {
            var users = this.rpContext.Users.Where(x => !x.IsDeleted).ToHashSet();

            foreach (var user in users)
            {
                user.Role = (await this.userManager.GetRolesAsync(user)).FirstOrDefault();
            }

            var userDTOs = mapper.Map<IEnumerable<UserDTO>>(users);

            return await Task.FromResult(userDTOs);
        }

        /// <summary>
        /// Gets single user from the Database.
        /// </summary>
        /// <param name="id">The ID of the user.</param>
        /// <returns>Playlist DTO if ID is valid, otherwise null.</returns>
        public async Task<UserDTO> GetUserAsync(int id)
        {
            var user = await this.rpContext.Users
                .Where(x => !x.IsDeleted)
                .Include(user => user.Playlists.Where(pl => !pl.IsDeleted))
                .ThenInclude(pl => pl.Genres)
                .SingleOrDefaultAsync(x => x.Id.Equals(id));

            if (user != null)
            {
                user.Role = (await this.userManager.GetRolesAsync(user)).FirstOrDefault();

                var userDTO = mapper.Map<UserDTO>(user);

                return await Task.FromResult(userDTO);
            }

            this.logger.LogError("UserServicesAsync: Failed to get user.");
            return null;
        }

        /// <summary>
        /// Authenticates user based on the given login model. 
        /// </summary>
        /// <param name="loginDTO">The model, from which the data is extracted.</param>
        /// <returns>User DTO with JWTSecurityToken if successfully authenticated, otherwise null.</returns>
        public async Task<UserDTO> AuthenticateAsync(LoginDTO loginDTO)
        {
            var user = await this.rpContext.Users
                .SingleOrDefaultAsync(u => u.UserName == loginDTO.Username);

            // return null if user not found
            if (user != null)
            {
                var signInAttempt = await this.signInManager.CheckPasswordSignInAsync(user, loginDTO.Password, false);

                if (signInAttempt.Succeeded)
                {
                    user.Role = (await this.userManager.GetRolesAsync(user)).FirstOrDefault();

                    // authentication successful so generate jwt token
                    var tokenHandler = new JwtSecurityTokenHandler();
                    var key = Encoding.ASCII.GetBytes(appSettings.Value.Secret);
                    var tokenDescriptor = new SecurityTokenDescriptor
                    {
                        Subject = new ClaimsIdentity(new Claim[]
                        {
                            new Claim(ClaimTypes.Name, user.Id.ToString()),
                            new Claim(ClaimTypes.Role, user.Role)
                        }),
                        Expires = DateTime.UtcNow.AddDays(7),
                        SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                    };

                    var token = tokenHandler.CreateToken(tokenDescriptor);
                    user.Token = tokenHandler.WriteToken(token);
                    this.logger.LogInformation($"User with {user.Id} was authenticated succesfully.");

                    return await Task.FromResult(mapper.Map<UserDTO>(user));
                }
            }

            this.logger.LogError("UserServicesAsync: Failed to authenticate user.");
            return null;
        }

        /// <summary>
        /// Updates user based on the given model.
        /// </summary>
        /// <param name="registerModelDTO">The model, from which the data is extracted.</param>
        /// <returns>True if the user is created, otherwise - false.</returns>
        public async Task<bool> CreateUserAsync(RegisterModelDTO registerModelDTO)
        {
            var user = mapper.Map<User>(registerModelDTO);

            var insertOperation = await this.userManager.CreateAsync(user, registerModelDTO.Password);


            if (insertOperation.Succeeded)
            {
                var createdUser = await this.userManager.FindByNameAsync(user.UserName);
                await this.userManager.AddToRoleAsync(createdUser, "User");
                return true;
            }

            return false;
        }

        /// <summary>
        /// Updates the values of a user based on the given model.
        /// </summary>
        /// <param name="id">The user ID.</param>
        /// <param name="updateUserDTO">The model, from which the data is extracted.</param>
        public async Task<bool> UpdateUserAsync(int id, UpdateUserDTO updateUserDTO)
        {
            var userToUpdate = await this.rpContext.Users.FindAsync(id);

            if (userToUpdate == null)
                return false;

            var updateModel = mapper.Map<User>(updateUserDTO);

            userToUpdate.UserName = updateModel.UserName;
            userToUpdate.Name = updateModel.Name;
            userToUpdate.Age = updateModel.Age;
            userToUpdate.Email = updateModel.Email;
            userToUpdate.ImageURL = updateModel.ImageURL;

            this.rpContext.Users.Update(userToUpdate);

            await this.rpContext.SaveChangesAsync();
            this.logger.LogInformation($"User with {id} was updated.", DateTime.Now);

            return true;
        }

        /// <summary>
        /// Deletes a user.
        /// </summary>
        /// <param name="id">The user ID.</param>
        /// <returns>True if the model is deleted, otherwise - false.</returns>
        public async Task<bool> DeleteUserAsync(int id)
        {
            var userToDelete = await this.rpContext.Users.FindAsync(id);

            if (userToDelete != null)
            {
                userToDelete.IsDeleted = true;
                userToDelete.DeletedOn = DateTime.Now;
                userToDelete.ModifiedOn = DateTime.Now;

                this.rpContext.Entry(userToDelete).State = EntityState.Modified;

                await this.rpContext.SaveChangesAsync();
                this.logger.LogInformation($"User with {id} was deleted.", DateTime.Now);

                return true;
            }

            return false;
        }

        /// <summary>
        /// Creates user roles.
        /// </summary>
        /// <returns></returns>
        public async Task CreateRolesAsync()
        {
            //Creating Roles

            string[] roleNames = { "Admin", "User" };
            IdentityResult roleResult;

            foreach (var roleName in roleNames)
            {
                var roleExist = await this.roleManager.RoleExistsAsync(roleName);
                if (!roleExist)
                {
                    //create the roles and seed them to the database
                    roleResult = await this.roleManager.CreateAsync(new IdentityRole<int>(roleName));
                }
            }

            //Creating Admin User 
            var adminAccount = new User
            {
                UserName = "admin",
                Name = "administrator",
                Email = "admin@admin.com",
            };

            string adminPWD = "admin123";
            var _user = await this.userManager.FindByEmailAsync("admin@admin.com");

            if (_user == null)
            {
                var createAdmin = await this.userManager.CreateAsync(adminAccount, adminPWD);
                if (createAdmin.Succeeded)
                {
                    await this.userManager.AddToRoleAsync(adminAccount, "Admin");
                }
            }
        }

        /// <summary>
        /// Get the logs from the DB
        /// </summary>
        /// <returns>IEnumerable<LogDTO></returns>
        public async Task<IEnumerable<LogDTO>> GetLogsAsync()
        {
            var logs = this.rpContext.Logs.OrderByDescending(l => l.TimeStamp).Take(20);

            var logsDTOs = this.mapper.Map<IEnumerable<LogDTO>>(logs);

            return await Task.FromResult(logsDTOs);
        }
    }
}

