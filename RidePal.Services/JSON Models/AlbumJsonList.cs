﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace RidePal.Services.JSON_Models
{
    public class AlbumJsonList
    {
        [JsonProperty("data")]
        public List<AlbumJson> AlbumJsons { get; set; }
    }
}
