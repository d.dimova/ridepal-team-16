﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace RidePal.Services.JSON_Models
{
    public class GenreJsonList
    {
        [JsonProperty("data")]
        public List<GenreJson> GenreJsons { get; set; }
    }
}
