﻿using Newtonsoft.Json;
using RidePal.Services.JSONModels;
using System.Collections.Generic;

namespace RidePal.Services.JSON_Models
{
    public class TrackJsonList
    {
        [JsonProperty("data")]
        public List<TrackJson> TrackJson { get; set; }
    }
}
