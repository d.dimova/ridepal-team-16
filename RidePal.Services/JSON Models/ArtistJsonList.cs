﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace RidePal.Services.JSON_Models
{
    public class ArtistJsonList
    {
        [JsonProperty("data")]
        public List<ArtistJson> ArtistJsons { get; set; }
    }
}
