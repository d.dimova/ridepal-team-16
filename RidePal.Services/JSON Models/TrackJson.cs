﻿using Newtonsoft.Json;
using System;

namespace RidePal.Services.JSONModels
{
    public class TrackJson
    {
        [JsonProperty("id")]
        public int ID { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("duration")]
        public int Duration { get; set; }

        [JsonProperty("rank")]
        public int Rank { get; set; }

        [JsonProperty("preview")]
        public string Preview { get; set; }

        [JsonProperty("link")]
        public string TrackLink { get; set; }

        public DateTime? ReleaseDate { get; set; }

        public int? GenreID { get; set; }

        public int? ArtistID { get; set; }

        public int? AlbumID { get; set; }
    }
}
