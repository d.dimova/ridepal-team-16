﻿using Newtonsoft.Json;

namespace RidePal.Services.JSON_Models
{
    public class ArtistJson
    {
        [JsonProperty("id")]
        public int ID { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }


        [JsonProperty("picture_medium")]
        public string PictureURL { get; set; }

        [JsonProperty("tracklist")]
        public string TrackListUrl { get; set; }
    }
}
