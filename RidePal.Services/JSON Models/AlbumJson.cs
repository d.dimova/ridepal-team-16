﻿using Newtonsoft.Json;
using System;

namespace RidePal.Services.JSON_Models
{
    public class AlbumJson
    {
        [JsonProperty("id")]
        public int ID { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("genre_id")]
        public int? GenreID { get; set; }

        [JsonProperty("artist.id")]
        public int? ArtistID { get; set; }

        [JsonProperty("tracklist")]
        public string TrackListUrl { get; set; }

        [JsonProperty("release_date")]
        public DateTime? ReleaseDate { get; set; }

        [JsonProperty("cover_medium")]
        public string PictureMediumUrl { get; set; }

    }
}
