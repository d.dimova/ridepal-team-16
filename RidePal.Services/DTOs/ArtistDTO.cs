﻿using System.Collections.Generic;

namespace RidePal.Services.DTOs
{
    public class ArtistDTO
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string PictureURL { get; set; }

        public string TrackListUrl { get; set; }

        public ICollection<AlbumDTO> Albums { get; set; }

        public ICollection<TrackDTO> Tracks { get; set; }
    }
}
