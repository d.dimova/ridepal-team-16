﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RidePal.Services.DTOs
{
    public class SingleGenreDTO
    {
        public int ID { get; set; }

        [Required]
        public string Name { get; set; }

        public string PictureURL { get; set; }

        public ICollection<string> Albums { get; set; }

        public ICollection<TrackDTO> Tracks { get; set; }
    }
}
