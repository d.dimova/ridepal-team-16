﻿namespace RidePal.Services.DTOs
{
    public class InputPlaylistDTO
    {
        public string OriginAddress { get; set; }

        public string DestinationAddress { get; set; }

        public string Name { get; set; }

        public int GenreId1 { get; set; }

        public int GenrePercentage1 { get; set; }

        public int? GenreId2 { get; set; }

        public int? GenrePercentage2 { get; set; }

        public int? GenreId3 { get; set; }

        public int? GenrePercentage3 { get; set; }

        public int? GenreId4 { get; set; }

        public int? GenrePercentage4 { get; set; }

        public bool UseTopTracks { get; set; }

        public bool RepeatSameArtist { get; set; }
    }
}
