﻿using System.ComponentModel.DataAnnotations;

namespace RidePal.Services.DTOs
{
    public class GenreDTO
    {
        public int ID { get; set; }

        [Required]
        public string Name { get; set; }

        public string PictureURL { get; set; }
    }
}
