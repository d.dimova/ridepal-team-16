﻿using System;
using System.Collections.Generic;

namespace RidePal.Services.DTOs
{
    public class AlbumDTO
    {
        public int ID { get; set; }

        public string Title { get; set; }

        public int? ArtistID { get; set; }

        public int? GenreID { get; set; }

        public DateTime? ReleaseDate { get; set; }

        public string TrackListUrl { get; set; }

        public virtual string Artist { get; set; }

        public virtual string Genre { get; set; }

        public ICollection<TrackDTO> Tracks { get; set; }
    }
}
