﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RidePal.Services.DTOs
{
    public class UpdateUserDTO
    {
        [Required]
        [MinLength(3), MaxLength(100)]
        public string Name { get; set; }

        [Required]
        [MinLength(3), MaxLength(100)]
        public string UserName { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [Range(10, 120)]
        public int Age { get; set; }

        [DataType(DataType.ImageUrl)]
        public string ImageURL { get; set; }
    }
}
