﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RidePal.Services.DTOs
{
    public class UserDTO
    {
        public int Id { get; set; }

        [Required]
        [MinLength(3), MaxLength(100)]
        public string Name { get; set; }

        [Required]
        [MinLength(3), MaxLength(100)]
        public string UserName { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [Range(10, 120)]
        public int Age { get; set; }

        [NotMapped]
        public string Role { get; set; }

        public ICollection<PlaylistDTO> Playlists { get; set; }

        public string Token { get; set; }

        [DataType(DataType.ImageUrl)]
        public string ImageURL { get; set; }
    }
}
