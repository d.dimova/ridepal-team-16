﻿using System.ComponentModel.DataAnnotations;

namespace RidePal.Services.DTOs
{
    public class UpdatePlaylistDTO
    {
        [Required]
        [StringLength(150)]
        public string Name { get; set; }

        public string PictureUrl { get; set; }
    }
}
