﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RidePal.Services.DTOs
{
    public class TrackDTO
    {
        public int ID { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public int Duration { get; set; }

        [Required]
        public int Rank { get; set; }

        public DateTime? ReleaseDate { get; set; }

        [Required]
        public string Preview { get; set; }

        public string TrackLink { get; set; }

        public int? AlbumID { get; set; }

        public int ArtistID { get; set; }

        [Required]
        public int GenreID { get; set; }

        public string Album { get; set; }

        public string Artist { get; set; }

        public string Genre { get; set; }

        public string AlbumPictureUrl { get; set; }

        public ICollection<string> Playlists { get; set; }
    }
}
