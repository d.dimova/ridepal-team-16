﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RidePal.Services.DTOs
{
    public class PlaylistDTO
    {
        public int ID { get; set; }

        [Required]
        [StringLength(150)]
        public string Name { get; set; }

        public int TotalPlaytime { get; set; }

        public double Rank { get; set; }

        [Required]
        public int UserID { get; set; }

        public string UserName { get; set; }

        public int TracksCount { get; set; }

        public string PictureUrl { get; set; }

        public IList<string> Genres { get; set; }
    }
}
