﻿using Mapster;
using System.Collections.Generic;

namespace RidePal.Services.DTOs
{
    public class CreatePlaylistDTO
    {
        public string OriginAddress { get; set; }

        public string DestinationAddress { get; set; }

        public string Name { get; set; }

        //Key: GenreID, Value: Percentage
        [AdaptIgnore(MemberSide.Destination)]
        public IDictionary<int, int> GenrePercentage { get; set; }

        public bool UseTopTracks { get; set; }

        public bool RepeatSameArtist { get; set; }
    }
}
