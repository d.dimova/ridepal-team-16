﻿using System.ComponentModel.DataAnnotations;

namespace RidePal.Services.DTOs
{
    public class LoginDTO
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
