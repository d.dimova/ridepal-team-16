﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RidePal.Services.DTOs
{
    public class RegisterModelDTO
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [MinLength(3), MaxLength(100)]
        public string Name { get; set; }

        [Required]
        [MinLength(3), MaxLength(100)]
        public string UserName { get; set; }

        [Required]
        [Range(10, 120)]
        public int Age { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }


    }
}
