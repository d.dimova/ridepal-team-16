﻿using Mapster;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using RidePal.Data.Context;
using RidePal.Services.Contracts;
using RidePal.Services.DTOs;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RidePal.Services
{
    public class GenreServicesAsync : IGenreServicesAsync
    {
        private readonly RidePalContext rpContext;
        private readonly ILogger<GenreServicesAsync> logger;

        public GenreServicesAsync(RidePalContext rpContext
            , ILogger<GenreServicesAsync> logger)
        {
            this.rpContext = rpContext;
            this.logger = logger;
        }

        /// <summary>
        /// Gets all genres from the Database.
        /// </summary>
        /// <returns>Collection of Genre DTOs.</returns>
        public async Task<IEnumerable<GenreDTO>> GetAllGenresAsync()
        {
            var genreDTOs = this.rpContext.Genres
                .OrderBy(genre => genre.ID)
                .Skip(7)
                .Where(genre => !genre.IsDeleted).AsNoTracking().ProjectToType<GenreDTO>().ToList();

            return await Task.FromResult(genreDTOs);
        }

        /// <summary>
        /// Gets single genre from the Database.
        /// </summary>
        /// <param name="id">The ID of the genre.</param>
        /// <returns>Genre DTO if ID is valid, otherwise null.</returns>
        public async Task<SingleGenreDTO> GetGenreAsync(int id)
        {
            var genreDTO = await this.rpContext.Genres
                .Where(genre => !genre.IsDeleted)
                .AsNoTracking()
                .Include(genre => genre.Albums)
                .Include(pl => pl.Tracks)
                .ProjectToType<SingleGenreDTO>()
                .SingleOrDefaultAsync(pl => pl.ID.Equals(id));

            return genreDTO;
        }

    }
}
