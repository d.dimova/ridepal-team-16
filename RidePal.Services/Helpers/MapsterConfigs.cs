﻿using Mapster;
using RidePal.Data.Models;
using RidePal.Services.DTOs;
using System.Linq;

namespace RidePal.Services.Helpers
{
    public class MapsterConfigs : IRegister
    {
        /// <summary>
        /// Configures the MapsterMapper Global settings.
        /// </summary>
        /// <param name="config">The confugurator being used to construct the global settings.</param>
        public void Register(TypeAdapterConfig config)
        {
            config.ForType<Album, AlbumDTO>()
                .Map(dest => dest.Artist, src => src.Artist.Name)
                .Map(dest => dest.Genre, src => src.Genre.Name);

            config.ForType<Track, TrackDTO>()
                .Map(dest => dest.Album, src => src.Album.Title)
                .Map(dest => dest.AlbumPictureUrl, src => src.Album.PictureMediumUrl)
                .Map(dest => dest.Artist, src => src.Artist.Name)
                .Map(dest => dest.Genre, src => src.Genre.Name)
                .Map(dest => dest.Playlists, src => src.Playlists.Select(pl => pl.Name));

            config.ForType<Playlist, SinglePlaylistDTO>()
                .Map(dest => dest.Tracks, src => src.Tracks)
                .Map(dest => dest.Genres, src => src.Genres.Select(genre => genre.Name));

            config.ForType<Playlist, PlaylistDTO>()
                .Map(dest => dest.Genres, src => src.Genres.Select(genre => genre.Name));

            config.ForType<Genre, SingleGenreDTO>()
                .Map(dest => dest.Albums, src => src.Albums.Select(album => album.Title));
        }
    }
}
