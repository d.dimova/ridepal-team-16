﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using RidePal.Services.Contracts;
using System.Net.Http;
using System.Threading.Tasks;

namespace RidePal.Services
{
    public class BingMapsServicesAsync : IBingMapsServicesAsync
    {
        private readonly IHttpClientFactory clientFactory;
        private readonly ILogger<BingMapsServicesAsync> logger;

        public BingMapsServicesAsync(IHttpClientFactory clientFactory
            , ILogger<BingMapsServicesAsync> _logger)
        {
            this.clientFactory = clientFactory;
            this.logger = _logger;
        }

        /// <summary>
        /// Makes a request to Bing Maps public API based on the driving route and gets its duration in seconds if one exists. 
        /// </summary>
        /// <param name="originAddress">The starting point of the travel.</param>
        /// <param name="destinationAddress">The ending point of the travel.</param>
        /// <returns>The travel duration of the given route if existing, else -1.</returns>
        public async Task<int> GetTravelDurationAsync(string originAddress, string destinationAddress)
        {
            if (originAddress != null && destinationAddress != null)
            {
                var client = this.clientFactory.CreateClient();

                using (var resource = await client.GetAsync($"http://dev.virtualearth.net/REST/v1/Routes?wayPoint.1={originAddress}&wayPoint.2={destinationAddress}&key=AkkuQ8ZviULHkKfOAhEHXlVmt-cVD32ocThVKPhl0Xq-Q-gAmmqMcEk3UpfpWMay"))
                {
                    var responseAsString = await resource.Content.ReadAsStringAsync();

                    var jObj = JObject.Parse(responseAsString);

                    if (jObj["statusCode"].Value<int>() == 200)
                    {
                        var duration = jObj.SelectToken("resourceSets[0].resources[0].travelDuration").Value<int>();

                        return duration;
                    }
                }
            }

            this.logger.LogError("BingMapsServicesAsync: Failed to find route with the given points.");
            return -1;
        }
    }
}
